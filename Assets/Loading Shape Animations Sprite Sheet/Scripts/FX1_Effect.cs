﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX1_Effect : MonoBehaviour
{
    private Animator m_Anim;
    private AnimatorStateInfo animInfo;
    private void Awake()
    {
        m_Anim = this.GetComponent<Animator>();

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        animInfo = m_Anim.GetCurrentAnimatorStateInfo(0);
        if (animInfo.normalizedTime < 1.0f)
        {

        }
        else
            this.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        m_Anim.Play("LoadingFX1");
    }
}
