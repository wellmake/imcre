﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_213 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02,
        step03
    }
    public Transform container;

    [Header("Step01")]
    public GameObject step01;
    public UISprite project_Inside;

    [Header("Step02")]
    public GameObject step02;
    public UISprite sourceMonitor;
    public Transform dragImage;
    public GameObject panelAfter;
    bool dragOn;
    Vector3 newDragImagePosition;

    [Header("Step03")]
    public GameObject step03;
    public UISprite menu_Window;
    public UISprite menu_Workspaces;
    public GameObject hightlight_Menu_Workspaces;
    public GameObject guideBlue_Window;
    public GameObject guideBlue_Window_kr;
    GameObject windowClick;
    GameObject guideBlue_Workspaces;
    GameObject guideBlue_Reset;
    GameObject guideBlue_Reset_kr;

    Ray ray;
    RaycastHit hit;
    public Step curStep;
    Camera cam;
    Vector3 containerOriginPos;
    TweenPosition tp;
    List<UISprite> lmSpriteList;
    private void Start()
    {
        panelAfter.SetActive(false);
        cam = Camera.main;
        containerOriginPos = container.position;
        windowClick = step03.transform.Find("Window_Click").gameObject;
        guideBlue_Workspaces = step03.transform.Find("Guide_Blue_Workspaces").gameObject;
        guideBlue_Reset = step03.transform.Find("Guide_Blue_Reset").gameObject;
        guideBlue_Reset_kr = step03.transform.Find("Guide_Blue_Reset_kr").gameObject;

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if(LM_Manager.ins.curLanguage == LM_Manager.Language.en)
        {
           // Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            guideBlue_Window.SetActive(true);
        }
        else if(LM_Manager.ins.curLanguage == LM_Manager.Language.kr)
        {
           // Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        UIEventListener.Get(sourceMonitor.gameObject).onDragOver = PanelDragOver;
        UIEventListener.Get(sourceMonitor.gameObject).onDragOut += PanelDragOut;
    }
    void SetSprites()
    {
        UISprite dragItem = dragImage.GetComponent<UISprite>();
        UISprite window_Click = windowClick.GetComponent<UISprite>();
        UISprite workspaces_highlight = hightlight_Menu_Workspaces.GetComponent<UISprite>();
        UISprite panel_After = panelAfter.GetComponent<UISprite>();
        lmSpriteList.Clear();
        lmSpriteList.Add(dragItem);
        lmSpriteList.Add(window_Click);
        lmSpriteList.Add(menu_Window);
        lmSpriteList.Add(menu_Workspaces);
        lmSpriteList.Add(workspaces_highlight);
        lmSpriteList.Add(panel_After);
        lmSpriteList.Add(project_Inside);
        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
        dragItem.spriteName = "panel_drag";
        window_Click.spriteName = "window_(click)";
        menu_Window.spriteName = "window_menu_01";
        menu_Workspaces.spriteName = "window_menu_02";
        workspaces_highlight.spriteName = "window_menu_01(click)";
        panel_After.spriteName = "project_panel_03";
        project_Inside.spriteName = "213_project_panel_source";
    }
    public void OnStep02()
    {
        step01.SetActive(false);
        step02.SetActive(true);
        curStep++;
    }
    public void OnStep03()
    {
        step02.SetActive(false);
        panelAfter.SetActive(true);
        step03.SetActive(true);
        hightlight_Menu_Workspaces.SetActive(false);
        menu_Window.depth = -1;
        menu_Workspaces.depth = -1;
        curStep = Step.step03;
    }

    public void OnDragEnd()
    {
        Debug.Log("drag end");
    }
    public void PanelDragOver(GameObject go)
    {
        if (dragOn)
            sourceMonitor.spriteName = "panel_Source0";
    }
    public void PanelDragOut(GameObject go)
    {
        if (dragOn)
            sourceMonitor.spriteName = "panel_Source";
    }
    private void Update()
    {
        switch (curStep)
        {
            case Step.step01:
                break;
            case Step.step02:
                Step02_Update();
                break;
            case Step.step03:

                break;
            default:
                break;
        }
    }
    void Step02_Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) /*&& (hit.collider.tag == "Step02_Blue")*/)
            {
                if (hit.collider.tag == "Step02_Blue")
                {
                    //Debug.Log("dragging");
                    dragOn = true;
                    dragImage.gameObject.SetActive(true);
                    dragImage.position = hit.point;
                    newDragImagePosition = dragImage.position;
                }
            }
        }
        if (Input.GetMouseButton(0) && dragOn)
        {
            newDragImagePosition = Input.mousePosition;
            newDragImagePosition.z = 10f;
            dragImage.position = cam.ScreenToWorldPoint(newDragImagePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Step02_Red" && dragOn)
            {
                Debug.Log("red hit Move next Step");
                OnStep03();
            }
            dragOn = false;
            dragImage.gameObject.SetActive(false);
        }
    }
    public void Step03_OnWindowClick()
    {
        if (curStep == Step.step03)
        {
            // Deactivate Guide_Blue
            LM_Manager.ins.DeActivateCurrentGuideBlue();

            // Activate Window Menu and Guide_Blue
            windowClick.SetActive(true);
            //menu_Window.SetActive(true);
            menu_Window.GetComponent<UISprite>().depth = 6;
            guideBlue_Workspaces.SetActive(true);

            Debug.Log(cam.ScreenToWorldPoint(new Vector3(0, 0, 10f)).x - menu_Window.transform.position.x);
            MoveContainerHorizontally(cam.ScreenToWorldPoint(new Vector3(0, 0, 10f)).x - menu_Window.transform.position.x);
        }
    }

    public void Step03_OnWorkspacesClick()
    {
        if (curStep == Step.step03)
        {
            LM_Manager.ins.DeActivateCurrentGuideBlue();
            hightlight_Menu_Workspaces.SetActive(true);
            menu_Workspaces.depth = 6;
            if (LM_Manager.ins.curLanguage == LM_Manager.Language.en)
                guideBlue_Reset.SetActive(true);
            else if (LM_Manager.ins.curLanguage == LM_Manager.Language.kr)
                guideBlue_Reset_kr.SetActive(true);

            MoveContainerHorizontally(container.position.x + (menu_Window.transform.position.x - menu_Workspaces.transform.position.x) * 3 / 4);
        }
    }
    public void Step03_OnResetClick()
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        TweenPosition tween = LM_Manager.ins.MoveContainerHorizontally(containerOriginPos.x);
        //MoveContainerHorizontally(containerOriginPos.x);
        EventDelegate.Add(tween.onFinished, LM_Manager.ins.OnLevelComplete_213);
        panelAfter.SetActive(false);
        step03.SetActive(false);

    }
    void MoveContainerHorizontally(float newX)
    {
        Vector3 newPos = containerOriginPos;
        newPos.x = newX;
        Debug.Log("new WorldPosition : " + newPos);
        tp = TweenPosition.Begin(container.gameObject, 0.5f, newPos, true);
    }
}
