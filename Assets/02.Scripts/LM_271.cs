﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LM_271 : MonoBehaviour
{
    [Header("Step01")]
    public GameObject step01;
    public GameObject boxb_File;
    public GameObject menu_File;
    public GameObject boxb_Export;

    [Header("Step02")]
    public GameObject step02;
    public GameObject window_Export;
    float newX;
    public GameObject boxb_format;
    public GameObject format_List;
    public GameObject boxb_H264;
    public GameObject h_264;
    public GameObject boxb_Preset;
    public GameObject menu_Preset;
    public GameObject boxb_FullHD;
    public GameObject fullHD;
    public GameObject export_Button;
    public GameObject boxb_ExportButton;
    public GameObject encodingBox;
    public GameObject loadingBar;
    public GameObject window_Output;

    [Header("_kr Variables")]
    public GameObject boxb_format_kr;
    public GameObject h_264_kr;
    public GameObject boxb_Preset_kr;
    public GameObject fullHD_kr;

    List<UISprite> lmSpriteList;

    void Start()
    {
        if (LM_Manager.ins.curLanguage == LM_Manager.Language.kr)
        {
            boxb_format = boxb_format_kr;
            h_264 = h_264_kr;
            boxb_Preset = boxb_Preset_kr;
            fullHD = fullHD_kr;
        }
        UIEventListener.Get(boxb_File).onClick = OnBoxbFileClick;
        UIEventListener.Get(boxb_Export).onClick = OnBoxbExportClick;
        UIEventListener.Get(boxb_format).onClick = OnBoxbFormatClick;
        UIEventListener.Get(boxb_H264).onClick = OnBoxbH264Click;
        UIEventListener.Get(boxb_Preset).onClick = OnBoxbPresetClick;
        UIEventListener.Get(boxb_FullHD).onClick = OnFullHDClick;

        SetSprites();
    }
    void SetSprites()
    {
        lmSpriteList = new List<UISprite>();
        lmSpriteList.Add(menu_File.GetComponent<UISprite>());
        lmSpriteList.Add(window_Export.GetComponent<UISprite>());
        lmSpriteList.Add(format_List.GetComponent<UISprite>());
        lmSpriteList.Add(h_264.GetComponent<UISprite>());
        lmSpriteList.Add(menu_Preset.GetComponent<UISprite>());
        lmSpriteList.Add(fullHD.GetComponent<UISprite>());
        lmSpriteList.Add(export_Button.GetComponent<UISprite>());
        lmSpriteList.Add(encodingBox.GetComponent<UISprite>());
        lmSpriteList.Add(loadingBar.GetComponent<UISprite>());
        lmSpriteList.Add(window_Output.GetComponent<UISprite>());
        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }
    void OnBoxbFileClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        menu_File.SetActive(true);
        boxb_Export.SetActive(true);
        TweenScale.Begin(boxb_Export, 0.5f, Vector3.one).from = Vector3.zero;
    }
    void OnBoxbExportClick(GameObject gameObject) 
    {
        step01.SetActive(false);
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = MoveWindowExport;
        step02.SetActive(true);
    }
    void MoveWindowExport(GameObject gameObject)
    {
        newX = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 10)).x - window_Export.transform.position.x;
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x + newX);
        tp.AddOnFinished(OnMoveFinish);
    }
    void OnMoveFinish()
    {
        boxb_format.SetActive(true);
        //TweenScale.Begin(boxb_format, 0.3f, Vector3.one).from = Vector3.zero;
        LM_Manager.ins.BoxTweenScale(boxb_format);
    }
    void OnBoxbFormatClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        format_List.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_H264);
    }
    void OnBoxbH264Click(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        format_List.SetActive(false);
        boxb_H264.SetActive(false);
        h_264.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Preset);
    }
    void OnBoxbPresetClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        menu_Preset.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_FullHD);
    }
    void OnFullHDClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        menu_Preset.SetActive(false);
        export_Button.SetActive(true);
        fullHD.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_ExportButton);
        UIEventListener.Get(boxb_ExportButton).onClick = OnExportButtonClick;
    }
    void OnExportButtonClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        encodingBox.SetActive(true);
        StartCoroutine(Co_Loading());
    }
    IEnumerator Co_Loading()
    {
        bool buffering = false;
        UIProgressBar bar = loadingBar.GetComponent<UIProgressBar>();
        while (bar.value < 1)
        {
            yield return null;
            bar.value += Time.deltaTime / 4;
            if (bar.value > 0.75f && !buffering)
            {
                buffering = true;
                yield return new WaitForSeconds(0.7f);
            }
        }
        Debug.Log("Loading Finish");
        window_Output.SetActive(true);
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
    }
}
