﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_114 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02,
        step03
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step00_Guide")]

    [Header("Step01")]
    public GameObject step01;
    public UISprite step01_text;
    public GameObject step01_text_redPoints;
    public GameObject step01_layerGroup1;
    public UISprite step01_layer1_1;
    public UISprite step01_layer1_2;
    public UISprite step01_layer1_3;
    public GameObject step01_box1;
    public GameObject step01_slider1;
    public GameObject step01_keyboardGroup1;
    public GameObject step01_keyboard1_s;
    public GameObject step01_layerGroup2;
    public UISprite step01_layer2_1;
    public UISprite step01_layer2_1_5;
    public UILabel step01_layer2_1_5_label;
    public UISprite step01_layer2_2;
    public UISprite step01_layer2_3;
    public GameObject step01_box2;
    public UISprite step01_scale2;
    public GameObject step01_box3;
    public GameObject step01_box3_dragPointer;
    public GameObject step01_box3_5;
    public GameObject step01_box3_5_dragPointer;
    public GameObject step01_slider2;
    public GameObject step01_keyframe1;
    public GameObject step01_keyframe1_particle;
    public GameObject step01_keyframe2;
    public GameObject step01_keyframe2_anchor;
    public GameObject step01_box4;
    public GameObject step01_box5;
    public GameObject step01_box5_dragPointer;
    public GameObject step01_box6;
    public GameObject step01_keyboardGroup2;
    public GameObject step01_keyboard2_space;


    [Header("Step02")]
    public GameObject step02;

    [Header("Step03")]
    public GameObject step03;

    public TweenAlpha delay0;
    public TweenAlpha delay1;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_114_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));


    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {
            step01_slider1.GetComponent<UISlider>().value += Time.deltaTime;

            if(step01_slider1.GetComponent<UISlider>().value >= 1f)
            {
                
                dragCount++;

                if (dragCount == 2) {
                    dragFlag = false;
                    StartCoroutine(EndDelay());
                } else
                    step01_slider1.GetComponent<UISlider>().value = 0f;

            }
        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_text);
        lmSpriteList.Add(step01_layer1_1);
        lmSpriteList.Add(step01_layer1_2);
        lmSpriteList.Add(step01_layer1_3);
        lmSpriteList.Add(step01_layer2_1);
        lmSpriteList.Add(step01_layer2_1_5);
        lmSpriteList.Add(step01_layer2_2);
        lmSpriteList.Add(step01_layer2_3);
        lmSpriteList.Add(step01_scale2);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_text.spriteName = "AE_114_text";
        step01_layer1_1.spriteName = "AE_114_layer_1";
        step01_layer1_2.spriteName = "AE_114_layer_2";
        step01_layer1_3.spriteName = "AE_114_layer_3";
        step01_layer2_1.spriteName = "AE_114_layer_1_select";
        step01_layer2_1_5.spriteName = "AE_114_layer_1_scale";
        step01_layer2_2.spriteName = "AE_114_layer_2";
        step01_layer2_3.spriteName = "AE_114_layer_3";
        step01_scale2.spriteName = "AE_114_scale_2";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        CameraManager.ins.MoveCamera(step01_keyboard1_s.transform.position, 0.5f, false);

        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
        {
            step01_box1.SetActive(false);
            step01_layer1_1.spriteName = "AE_114_layer_1_select";
            step01_text_redPoints.SetActive(true);

            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_114_1", "MID");

            step01_keyboardGroup1.SetActive(true);
            step01_keyboard1_s.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_keyboard1_s.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
            {
                LM_AE_Manager.ins.Off_Middle_Guide();
                step01_keyboardGroup1.SetActive(false);
                step01_layerGroup1.SetActive(false);
                step01_layerGroup2.SetActive(true);

                step01_box2.SetActive(true);
                step01_box2.GetComponent<TweenAlpha>().onFinished.Clear();
                step01_box2.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                {
                    LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_114_2", "MID");

                    step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                    step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                    {
                        step01_keyframe1.SetActive(true);
                        step01_box2.SetActive(false);

                        step01_box3.SetActive(true);
                        step01_box3_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                        step01_box3.GetComponent<TweenAlpha>().onFinished.Clear();
                        step01_box3.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                        {
                            step01_keyframe1_particle.SetActive(false);
                            step01_box3.transform.FindChild("PrefabDragAnim").gameObject.GetComponent<PrefabDragAnim>().StartDrag();

                            //CameraManager.ins.MoveCamera(step01_keyboard1_s.transform.position, 0.5f, false);


                            step01_slider2.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                step01_slider2.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                                CameraManager.SCREEN_MOVE_PAUSE = true;
                            }));
                            step01_slider2.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                                CameraManager.SCREEN_MOVE_PAUSE = false;
                            }));
                            step01_slider2.SetActive(true);
                            step01_slider2.GetComponent<BoxCollider>().enabled = true;
                            step01_slider2.GetComponent<UISlider>().onChange.Clear();
                            step01_slider2.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate
                            {
                                string valueStr = ((float)((float)step01_slider2.GetComponent<UISlider>().value * (float)2900f) + 100f).ToString("N1");
                                step01_layer2_1_5_label.text = valueStr + " " + valueStr;

                                step01_text.GetComponent<UISprite>().height = 246 + (int)((float)step01_slider2.GetComponent<UISlider>().value * (float)174f);

                                if (step01_slider2.GetComponent<UISlider>().value == 1f)
                                {
                                    step01_slider2.SetActive(false);
                                    step01_slider2.GetComponent<UISlider>().onChange.Clear();
                                    step01_box3.SetActive(false);
                                    LM_AE_Manager.ins.Off_Middle_Guide();
                                    CameraManager.ins.MoveCamera(step01_text.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                                        delay1.gameObject.SetActive(true);
                                        delay1.onFinished.Add(new EventDelegate(delegate
                                        {
                                            step01_box5.SetActive(true);
                                            step01_box6.SetActive(true);
                                            step01_box5_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                                            step01_box5.GetComponent<TweenAlpha>().onFinished.Clear();
                                            step01_box5.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                                            {
                                                step01_box5.transform.FindChild("PrefabDragAnim").gameObject.GetComponent<PrefabDragAnim>().StartDrag();

                                                CameraManager.ins.MoveCamera(step01_keyboard1_s.transform.position, 0.5f, false);

                                                step01_slider1.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                                    CameraManager.SCREEN_MOVE_PAUSE = true;
                                                }));
                                                step01_slider1.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                                                    CameraManager.SCREEN_MOVE_PAUSE = false;
                                                }));
                                                step01_slider1.GetComponent<BoxCollider>().enabled = true;
                                                step01_slider1.GetComponent<UISlider>().onChange.Clear();
                                                step01_slider1.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate
                                                {

                                                    step01_keyframe1.GetComponent<UISprite>().spriteName = "AE_114_keyframe_off";


                                                    if (step01_slider1.GetComponent<UISlider>().value == 1f)
                                                    {
                                                        step01_keyframe2.SetActive(true);
                                                        step01_box5.SetActive(false);
                                                        step01_box6.SetActive(false);

                                                        step01_box3_5.SetActive(true);
                                                        step01_box3_5_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                                                        step01_box3_5.transform.FindChild("PrefabDragAnim").gameObject.GetComponent<PrefabDragAnim>().StartDrag();

                                                        step01_slider2.SetActive(true);
                                                        step01_slider2.GetComponent<BoxCollider>().enabled = true;
                                                        step01_slider2.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                                            CameraManager.SCREEN_MOVE_PAUSE = true;
                                                        }));
                                                        step01_slider2.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                                                            CameraManager.SCREEN_MOVE_PAUSE = false;
                                                        }));
                                                        step01_slider2.GetComponent<UISlider>().onChange.Clear();
                                                        step01_slider2.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate
                                                        {
                                                            string valueStr2 = ((float)((float)step01_slider2.GetComponent<UISlider>().value * (float)2900f) + 100f).ToString("N1");

                                                            step01_layer2_1_5_label.text = valueStr2 + " " + valueStr2;
                                                            step01_text.GetComponent<UISprite>().height = 246 + (int)((float)step01_slider2.GetComponent<UISlider>().value * (float)174f);

                                                            if (step01_slider2.GetComponent<UISlider>().value == 0f)
                                                            {
                                                                step01_slider2.GetComponent<BoxCollider>().enabled = false;
                                                                step01_box3_5.SetActive(false);

                                                                delay0.gameObject.SetActive(true);
                                                                delay0.onFinished.Add(new EventDelegate(delegate
                                                                {
                                                                    CameraManager.ins.MoveCamera(step01_box6.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                                                                        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_114_3", "MID");
                                                                    }));

                                                                    step01_keyboardGroup2.SetActive(true);
                                                                    step01_keyboard2_space.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                    step01_keyboard2_space.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                        step01_keyframe2.GetComponent<UISprite>().spriteName = "AE_114_keyframe_off";
                                                                        step01_keyframe2.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnStart;

                                                                        LM_AE_Manager.ins.Off_Middle_Guide();

                                                                        step01_keyboardGroup2.SetActive(false);

                                                                        OnStep02();
                                                                    }));
                                                                }));

                                                            }
                                                        }));
                                                    }
                                                }));
                                            }));
                                        }));
                                    }));

                                    
                                    
                                }
                            }));

                        }));
                    }));

                }));

            }));

        }));




    }

    public void OnStep02()
    {

        step01_keyframe2.transform.localPosition = step01_keyframe2_anchor.transform.localPosition;
        step01_slider1.GetComponent<UISlider>().onChange.Clear();
        step01_slider1.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate
        {
            step01_text.GetComponent<UISprite>().height = 246 + (int)((float)step01_slider1.GetComponent<UISlider>().value * (float)174f);
        }));

        step01_slider1.GetComponent<UISlider>().value = 0f;

        dragFlag = true;
    }
    public void OnStep03()
    {

    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_114_4");
    }
}
