﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_332 : MonoBehaviour
{
    float timelineX;
    Camera cam;
    Ray ray;
    RaycastHit hit;
    int percentage = 100;
    bool dragOn;
    bool dragStart;
    Vector3 mouseOriginPos;
    float mouseOffset;

    public GameObject boxb_File;
    public GameObject jpg_Clip;
    public GameObject polaroid_jpg;
    public GameObject clip_Sprite_Touch;
    public UISprite monitor_Image;
    public GameObject boxb_Pen;
    public GameObject panel_Source_Mask;

    [Header("Polaroid Var")]
    public GameObject boxb_Polaroid_1;
    public GameObject mask_Point_1;
    public GameObject boxb_Polaroid_2;
    public GameObject mask_Point_2;
    public GameObject boxb_Polaroid_3;
    public GameObject mask_Point_3;
    public GameObject boxb_Polaroid_4;
    public GameObject mask_Point_4;

    public GameObject mask_Line_1;
    public GameObject Sprite_Touch_mask_Line4;

    [Header("After Polaroid Var")]
    public GameObject boxb_Inverted;
    public GameObject inverted_Click;
    public GameObject polaroid_Image;
    public GameObject boxb_mp4Clip;
    public GameObject mp4_Clip;

    public GameObject boxb_Scale;
    public GameObject toggle_Animation;
    public GameObject boxb_Percent;
    public GameObject box_Drag;
    public UILabel label_Percent;

    [Header("SetSprites Var")]
    public UISprite mask_Line_2;
    public UISprite mask_Line_3;
    public UISprite mask_Line_4;

    [Header("KR Var")]
    public GameObject boxb_Inverted_kr;

    void Start()
    {
        if (LM_Manager.ins.curLanguage == LM_Manager.Language.kr)
        {
            boxb_Inverted = boxb_Inverted_kr;
        }
        cam = Camera.main;
        //UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnStageStart;
        LM_Manager.ins.Set_Start_Guide("LEARNING_AE_111_0", OnStageStart);
        SetSprites();
    }
    void SetSprites()
    {
        List<UISprite> lmSpriteList = new List<UISprite>();
        lmSpriteList.Add(jpg_Clip.GetComponent<UISprite>());
        lmSpriteList.Add(polaroid_jpg.GetComponent<UISprite>());
        lmSpriteList.Add(monitor_Image);
        lmSpriteList.Add(panel_Source_Mask.GetComponent<UISprite>());
        lmSpriteList.Add(mask_Point_1.GetComponent<UISprite>());
        lmSpriteList.Add(mask_Point_2.GetComponent<UISprite>());
        lmSpriteList.Add(mask_Point_3.GetComponent<UISprite>());
        lmSpriteList.Add(mask_Point_4.GetComponent<UISprite>());
        lmSpriteList.Add(mask_Line_1.GetComponent<UISprite>());
        lmSpriteList.Add(mask_Line_2);
        lmSpriteList.Add(mask_Line_3);
        lmSpriteList.Add(mask_Line_4);
        lmSpriteList.Add(inverted_Click.GetComponent<UISprite>());
        lmSpriteList.Add(polaroid_Image.GetComponent<UISprite>());
        lmSpriteList.Add(mp4_Clip.GetComponent<UISprite>());
        lmSpriteList.Add(toggle_Animation.GetComponent<UISprite>());

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }
    void OnStageStart(GameObject gameObject)
    {
        LM_Manager.ins.BoxTweenScale(boxb_File);
        UIEventListener.Get(boxb_File).onClick = OnBoxbFileClick;
    }
    void OnBoxbFileClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        timelineX = LM_Manager.ins.container.position.x +
            ((LM_Manager.ins.panel_Project.transform.position.x - jpg_Clip.transform.position.x) * 3 / 4);
        LM_Manager.ins.MoveContainerHorizontally(timelineX).SetOnFinished(() =>
        {
            clip_Sprite_Touch.SetActive(true);
            UIEventListener.Get(jpg_Clip).onClick = Onjpg_ClipClick;
        });
        StartCoroutine(Co_MoveFileName());
    }
    IEnumerator Co_MoveFileName()
    {
        polaroid_jpg.SetActive(true);
        while (Vector3.Distance(polaroid_jpg.transform.position, jpg_Clip.transform.position) > 0.2f)
        {
            polaroid_jpg.transform.position = Vector3.Lerp(polaroid_jpg.transform.position, jpg_Clip.transform.position, Time.deltaTime * 2);
            yield return null;
        }
        polaroid_jpg.SetActive(false);
    }
    void Onjpg_ClipClick(GameObject gameObject)
    {
        jpg_Clip.GetComponent<BoxCollider>().enabled = false;
        jpg_Clip.GetComponent<TweenAlpha>().Finish();
        //jpg_Clip.GetComponent<UISprite>().alpha = 1f;
        clip_Sprite_Touch.SetActive(false);
        monitor_Image.gameObject.SetActive(true);
        LM_Manager.ins.panel_Source.spriteName = "effect_control_1";
        LM_Manager.ins.MoveContainerHorizontally(0).SetOnFinished(() =>
        {
            LM_Manager.ins.panel_Pause.SetActive(true);
            UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
            {
                LM_Manager.ins.BoxTweenScale(boxb_Pen);
                UIEventListener.Get(boxb_Pen).onClick = OnBoxbPenClick;
            };
        });
    }
    void OnBoxbPenClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        panel_Source_Mask.SetActive(true);
        LM_Manager.ins.MoveContainerHorizontally(timelineX).SetOnFinished(() =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_Polaroid_1);
            UIEventListener.Get(boxb_Polaroid_1).onClick = OnBoxbPolaroid1_Click;
        });
    }
    void OnBoxbPolaroid1_Click(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        mask_Point_1.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Polaroid_2);
        UIEventListener.Get(boxb_Polaroid_2).onClick = OnBoxbPolaroid2_Click;
    }
    void OnBoxbPolaroid2_Click(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        mask_Point_2.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Polaroid_3);
        UIEventListener.Get(boxb_Polaroid_3).onClick = OnBoxbPolaroid3_Click;
    }
    void OnBoxbPolaroid3_Click(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        mask_Point_3.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Polaroid_4);
        UIEventListener.Get(boxb_Polaroid_4).onClick = OnBoxbPolaroid4_Click;
    }
    void OnBoxbPolaroid4_Click(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        mask_Point_4.SetActive(true);
        Sprite_Touch_mask_Line4.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Polaroid_1);
        UIEventListener.Get(boxb_Polaroid_1).onClick = OnBoxb_Polaroid_Complete;
    }
    void OnBoxb_Polaroid_Complete(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        Sprite_Touch_mask_Line4.SetActive(false);
        mask_Line_1.SetActive(true);
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
        {
            TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(0f);
            tp.SetOnFinished(() =>
            {
                LM_Manager.ins.BoxTweenScale(boxb_Inverted);
                UIEventListener.Get(boxb_Inverted).onClick = OnBoxb_InvertedClick;
            });
        };
    }
    void OnBoxb_InvertedClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        inverted_Click.SetActive(true);
        mask_Point_1.SetActive(false);
        mask_Point_2.SetActive(false);
        mask_Point_3.SetActive(false);
        mask_Point_4.SetActive(false);
        mask_Line_1.SetActive(false);
        polaroid_Image.SetActive(true);
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(timelineX);
        tp.SetOnFinished(() =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_mp4Clip);
            UIEventListener.Get(boxb_mp4Clip).onClick = OnBoxb_mp4ClipClick;
        });
    }
    void OnBoxb_mp4ClipClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        mp4_Clip.SetActive(true);
        LM_Manager.ins.panel_Source.spriteName = "effect_control_mp4";
        panel_Source_Mask.SetActive(false);
        toggle_Animation.SetActive(true);
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(0f);
        tp.SetOnFinished(() =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_Scale);
            UIEventListener.Get(boxb_Scale).onClick = OnBoxb_ScaleClick;
        });
    }
    void OnBoxb_ScaleClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        LM_Manager.ins.BoxTweenScale(boxb_Percent);
        UIEventListener.Get(boxb_Percent).onClick = OnBoxb_PercentClick;
    }
    void OnBoxb_PercentClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        LM_Manager.ins.BoxTweenScale(box_Drag);
        dragOn = true;
    }

    void Update()
    {
        if (dragOn)
        {
            SetPercentage();
        }
    }
    void SetPercentage()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("DragStart"))
            {
                Debug.Log("DragStart");
                dragStart = true;
                mouseOriginPos = Input.mousePosition;
            }
        }
        else if (dragStart && Input.GetMouseButton(0))
        {
            mouseOffset = mouseOriginPos.x - Input.mousePosition.x;
            //Debug.Log(mouseOffset);
            percentage -= Mathf.CeilToInt(mouseOffset * 0.01f);
            percentage = Mathf.Clamp(percentage, 0, 200);
            label_Percent.text = string.Format("{0}%", percentage);
            if (percentage == 200)
            {
                polaroid_Image.GetComponent<UISprite>().spriteName = "flower_after";
                TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(timelineX);
                tp.SetOnFinished(OnDragFinish);
                dragOn = false;
                return;
            }
        }
        else if (dragStart && Input.GetMouseButtonUp(0))
        {
            dragStart = false;
        }
    }
    void OnDragFinish()
    {
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
    }
}
