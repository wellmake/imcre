﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_212 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_groupKeyboard;
    public GameObject step01_keyboard_Ctrl;
    public GameObject step01_keyboard_I;
    public UISprite step01_spriteImportPopup;
    public GameObject step01_keyboard_Shift;
    public GameObject step01_box1;
    public GameObject step01_box2;
    public UISprite step01_spriteImport1;
    public GameObject step01_box3;
    public UISprite step01_spriteImport2;
    public GameObject step01_box4;
    public UISprite step01_spriteImport3;
    public GameObject step01_box5;
    public UISprite step01_spriteImportBtn;

    [Header("Step02")]
    public GameObject step02;
    public UISprite step02_spriteYellowPreview;
    public UISprite step02_spriteBlueListItem;
    public UISprite step02_spriteVideoListItem;
    public UISprite step02_spriteYellowListItem;
    public UISprite step02_spriteYellowListItem_dragable;
    public GameObject step02_box2;
    public GameObject step02_box3;
    public GameObject step02_box3_dragPointer;
    public UISprite step02_spriteCompIcon;
    public GameObject step02_dragEnd;
    public GameObject step02_groupYellow;
    public GameObject step02_glitters;
    public UISprite step02_spriteYellowListItem2;
    public GameObject step02_empty;
    public UISprite step02_spriteProject3;
    public UISprite step02_spriteYellowCompBtn;
    public UISprite step02_spriteYellowTimeline;
    public UISprite step02_spriteYellowTab;
    public UISprite step02_spriteIndicator;

    [Header("Step03")]
    public GameObject step03;
    public UISprite step03_spriteYellowTimeline;
    public UISprite step03_spriteYellowCompBtn;
    public GameObject step03_empty;
    public UISprite step03_spriteProject3;
    public UISprite step03_spriteBluePreview;
    public UISprite step03_spriteBlueListItem;
    public UISprite step03_spriteBlueListItem_dragable;
    public UISprite step03_spriteVideoListItem;
    public UISprite step03_spriteYellowListItem;
    public UISprite step03_spriteYellowListItem2;
    public UISprite step03_spriteYellowTab;
    public GameObject step03_box4;
    public GameObject step03_box5;
    public GameObject step03_box5_dragPointer;
    public UISprite step03_spriteCompIcon;
    public GameObject step03_dragEnd;
    public GameObject step03_groupBlue;
    public GameObject step03_glitters;
    public UISprite step03_spriteBlueListItem2;
    public UISprite step03_spriteVideoListItem2;
    public UISprite step03_spriteYellowListItem3;
    public UISprite step03_spriteYellowListItem4;
    public GameObject step03_empty2;
    public UISprite step03_spriteProject2;
    public UISprite step03_spriteBlueCompBtn;
    public UISprite step03_spriteBlueTimeline;
    public UISprite step03_spriteBlueTab;
    public UISprite step03_spriteIndicator;

    [Header("Step04")]
    public GameObject step04;
    public UISprite step04_spriteBlueTimeline;
    public UISprite step04_spriteBlueCompBtn;
    public GameObject step04_empty;
    public UISprite step04_spriteProject2;
    public UISprite step04_spriteVideoPreview;
    public UISprite step04_spriteBlueListItem;
    public UISprite step04_spriteBlueListItem2;
    public UISprite step04_spriteVideoListItem;
    public UISprite step04_spriteVideoListItem_dragable;
    public UISprite step04_spriteYellowListItem;
    public UISprite step04_spriteYellowListItem2;
    public UISprite step04_spriteYellowTab;
    public UISprite step04_spriteBlueTab;
    public GameObject step04_box4;
    public GameObject step04_box5;
    public GameObject step04_box5_dragPointer;
    public UISprite step04_spriteCompIcon;
    public GameObject step04_dragEnd;
    public GameObject step04_groupVideo;
    public GameObject step04_glitters;
    public UISprite step04_spriteVideoListItem2;
    public UISprite step04_spriteVideoListItem3;
    public UISprite step04_spriteYellowListItem3;
    public UISprite step04_spriteYellowListItem4;
    public UISprite step04_spriteProject1;
    public UISprite step04_spriteVideoCompBtn;
    public UISprite step04_spriteVideoTimeline;
    public UISprite step04_spriteVideoTab;
    public UISprite step04_spriteIndicator;

    [Header("Step05")]
    public GameObject step05;
    public UISprite step05_spriteVideoCompBtn;
    public UISprite step05_spriteYellowTab;
    public UISprite step05_spriteBlueTab;
    public UISprite step05_spriteVideoTab;
    public UISprite step05_spriteProject1;
    public UISprite step05_spriteYellowPreview;
    public UISprite step05_spriteBlueListItem;
    public UISprite step05_spriteBlueListItem2;
    public UISprite step05_spriteVideoListItem;
    public UISprite step05_spriteVideoListItem2;
    public UISprite step05_spriteYellowListItem;
    public UISprite step05_spriteYellowListItem_dragable;
    public UISprite step05_spriteYellowListItem2;
    public GameObject step05_box6;
    public GameObject step05_box7;
    public GameObject step05_box7_dragPointer;
    public GameObject step05_dragEnd1;
    public UISprite step05_spriteTimeline1;
    public UISprite step05_spriteTimeline2;
    public UISprite step05_spriteProject3;
    public UISprite step05_spriteBlueListItem_dragable;
    public GameObject step05_box8;
    public GameObject step05_box9;
    public GameObject step05_box9_dragPointer;
    public GameObject step05_dragEnd2;
    public UISprite step05_spriteTimeline3;
    public UISprite step05_spriteProject2;
    public GameObject step05_glitters;
    public GameObject step05_cameraMovePoint;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;
    public TweenAlpha delay5;
    public TweenAlpha delay6;
    public TweenAlpha delay7;
    public TweenAlpha delay8;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_212_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_box2.GetComponent<UISprite>().topAnchor.absolute = 223;
            step01_box2.GetComponent<UISprite>().bottomAnchor.absolute = 31;
            step01_box2.GetComponent<UISprite>().leftAnchor.absolute = -178;
            step01_box2.GetComponent<UISprite>().rightAnchor.absolute = 13;

            step01_spriteImport1.GetComponent<UISprite>().topAnchor.absolute = 224;
            step01_spriteImport1.GetComponent<UISprite>().bottomAnchor.absolute = 35;
            step01_spriteImport1.GetComponent<UISprite>().leftAnchor.absolute = -163;
            step01_spriteImport1.GetComponent<UISprite>().rightAnchor.absolute = 4;

            step01_box3.GetComponent<UISprite>().topAnchor.absolute = 197;
            step01_box3.GetComponent<UISprite>().bottomAnchor.absolute = 32;
            step01_box3.GetComponent<UISprite>().leftAnchor.absolute = 34;
            step01_box3.GetComponent<UISprite>().rightAnchor.absolute = 225;

            step01_spriteImport2.GetComponent<UISprite>().topAnchor.absolute = 224;
            step01_spriteImport2.GetComponent<UISprite>().bottomAnchor.absolute = 35;
            step01_spriteImport2.GetComponent<UISprite>().leftAnchor.absolute = 51;
            step01_spriteImport2.GetComponent<UISprite>().rightAnchor.absolute = 216;

            step01_box4.GetComponent<UISprite>().topAnchor.absolute = 225;
            step01_box4.GetComponent<UISprite>().bottomAnchor.absolute = 35;
            step01_box4.GetComponent<UISprite>().leftAnchor.absolute = 248;
            step01_box4.GetComponent<UISprite>().rightAnchor.absolute = 436;

            step01_spriteImport3.GetComponent<UISprite>().topAnchor.absolute = 226;
            step01_spriteImport3.GetComponent<UISprite>().bottomAnchor.absolute = 35;
            step01_spriteImport3.GetComponent<UISprite>().leftAnchor.absolute = 262;
            step01_spriteImport3.GetComponent<UISprite>().rightAnchor.absolute = 424;

            step01_box5.GetComponent<UISprite>().topAnchor.absolute = -311;
        }
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteImportPopup);
        lmSpriteList.Add(step01_spriteImport1);
        lmSpriteList.Add(step01_spriteImport2);
        lmSpriteList.Add(step01_spriteImport3);
        lmSpriteList.Add(step01_spriteImportBtn);

        lmSpriteList.Add(step02_spriteYellowPreview);
        lmSpriteList.Add(step02_spriteBlueListItem);
        lmSpriteList.Add(step02_spriteVideoListItem);
        lmSpriteList.Add(step02_spriteYellowListItem);
        lmSpriteList.Add(step02_spriteYellowListItem_dragable);
        lmSpriteList.Add(step02_spriteCompIcon);
        lmSpriteList.Add(step02_spriteYellowListItem2);
        lmSpriteList.Add(step02_spriteProject3);
        lmSpriteList.Add(step02_spriteYellowCompBtn);
        lmSpriteList.Add(step02_spriteYellowTimeline);
        lmSpriteList.Add(step02_spriteYellowTab);
        lmSpriteList.Add(step02_spriteIndicator);

        lmSpriteList.Add(step03_spriteYellowTimeline);
        lmSpriteList.Add(step03_spriteYellowCompBtn);
        lmSpriteList.Add(step03_spriteProject3);
        lmSpriteList.Add(step03_spriteBluePreview);
        lmSpriteList.Add(step03_spriteBlueListItem);
        lmSpriteList.Add(step03_spriteBlueListItem_dragable);
        lmSpriteList.Add(step03_spriteVideoListItem);
        lmSpriteList.Add(step03_spriteYellowListItem);
        lmSpriteList.Add(step03_spriteYellowListItem2);
        lmSpriteList.Add(step03_spriteYellowTab);
        lmSpriteList.Add(step03_spriteCompIcon);
        lmSpriteList.Add(step03_spriteBlueListItem2);
        lmSpriteList.Add(step03_spriteVideoListItem2);
        lmSpriteList.Add(step03_spriteYellowListItem3);
        lmSpriteList.Add(step03_spriteYellowListItem4);
        lmSpriteList.Add(step03_spriteProject2);
        lmSpriteList.Add(step03_spriteBlueCompBtn);
        lmSpriteList.Add(step03_spriteBlueTimeline);
        lmSpriteList.Add(step03_spriteBlueTab);
        lmSpriteList.Add(step03_spriteIndicator);

        lmSpriteList.Add(step04_spriteBlueTimeline);
        lmSpriteList.Add(step04_spriteBlueCompBtn);
        lmSpriteList.Add(step04_spriteProject2);
        lmSpriteList.Add(step04_spriteVideoPreview);
        lmSpriteList.Add(step04_spriteBlueListItem);
        lmSpriteList.Add(step04_spriteBlueListItem2);
        lmSpriteList.Add(step04_spriteVideoListItem);
        lmSpriteList.Add(step04_spriteVideoListItem_dragable);
        lmSpriteList.Add(step04_spriteYellowListItem);
        lmSpriteList.Add(step04_spriteYellowListItem2);
        lmSpriteList.Add(step04_spriteYellowTab);
        lmSpriteList.Add(step04_spriteBlueTab);
        lmSpriteList.Add(step04_spriteCompIcon);
        lmSpriteList.Add(step04_spriteVideoListItem2);
        lmSpriteList.Add(step04_spriteVideoListItem3);
        lmSpriteList.Add(step04_spriteYellowListItem3);
        lmSpriteList.Add(step04_spriteYellowListItem4);
        lmSpriteList.Add(step04_spriteProject1);
        lmSpriteList.Add(step04_spriteVideoCompBtn);
        lmSpriteList.Add(step04_spriteVideoTimeline);
        lmSpriteList.Add(step04_spriteVideoTab);
        lmSpriteList.Add(step04_spriteIndicator);


        lmSpriteList.Add(step05_spriteVideoCompBtn);
        lmSpriteList.Add(step05_spriteYellowTab);
        lmSpriteList.Add(step05_spriteBlueTab);
        lmSpriteList.Add(step05_spriteVideoTab);
        lmSpriteList.Add(step05_spriteProject1);
        lmSpriteList.Add(step05_spriteYellowPreview);
        lmSpriteList.Add(step05_spriteBlueListItem);
        lmSpriteList.Add(step05_spriteBlueListItem2);
        lmSpriteList.Add(step05_spriteVideoListItem);
        lmSpriteList.Add(step05_spriteVideoListItem2);
        lmSpriteList.Add(step05_spriteYellowListItem);
        lmSpriteList.Add(step05_spriteYellowListItem_dragable);
        lmSpriteList.Add(step05_spriteYellowListItem2);
        lmSpriteList.Add(step05_spriteTimeline1);
        lmSpriteList.Add(step05_spriteTimeline2);
        lmSpriteList.Add(step05_spriteProject3);
        lmSpriteList.Add(step05_spriteBlueListItem_dragable);
        lmSpriteList.Add(step05_spriteTimeline3);
        lmSpriteList.Add(step05_spriteProject2);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteImportPopup.spriteName = "AE_212_Import-Popup";
        step01_spriteImport1.spriteName = "AE_212_Import_1";
        step01_spriteImport2.spriteName = "AE_212_Import_2";
        step01_spriteImport3.spriteName = "AE_212_Import_3";
        step01_spriteImportBtn.spriteName = "AE_212_Import-Button";

        step02_spriteYellowPreview.spriteName = "AE_212_Yellow-Preview";
        step02_spriteBlueListItem.spriteName = "AE_212_Blue-1";
        step02_spriteVideoListItem.spriteName = "AE_212_Viedo_01-1";
        step02_spriteYellowListItem.spriteName = "AE_212_Yellow-2";
        step02_spriteYellowListItem_dragable.spriteName = "AE_212_Yellow-2";
        step02_spriteCompIcon.spriteName = "AE_212_Comp-Icon";
        step02_spriteYellowListItem2.spriteName = "AE_212_Yellow-1";
        step02_spriteProject3.spriteName = "AE_212_Project_3";
        step02_spriteYellowCompBtn.spriteName = "AE_212_ellow-Comp-Button";
        step02_spriteYellowTimeline.spriteName = "AE_212_Yellow-Layer-2";
        step02_spriteYellowTab.spriteName = "AE_212_Yellow-Button(Timeline)-2";
        step02_spriteIndicator.spriteName = "AE_212_indicator";
        
        step03_spriteYellowTimeline.spriteName = "AE_212_Yellow-Layer-2";
        step03_spriteYellowCompBtn.spriteName = "AE_212_ellow-Comp-Button";
        step03_spriteProject3.spriteName = "AE_212_Project_3";
        step03_spriteBluePreview.spriteName = "AE_212_Blue-Comp-2-Preview";
        step03_spriteBlueListItem.spriteName = "AE_212_Blue-2";
        step03_spriteBlueListItem_dragable.spriteName = "AE_212_Blue-2";
        step03_spriteVideoListItem.spriteName = "AE_212_Viedo_01-1";
        step03_spriteYellowListItem.spriteName = "AE_212_Yellow-Cpmp-1";
        step03_spriteYellowListItem2.spriteName = "AE_212_Yellow-1";
        step03_spriteYellowTab.spriteName = "AE_212_Yellow-Button(Timeline)-1";
        step03_spriteCompIcon.spriteName = "AE_212_Comp-Icon";
        step03_spriteBlueListItem2.spriteName = "AE_212_Blue-1";
        step03_spriteVideoListItem2.spriteName = "AE_212_Viedo_01-1";
        step03_spriteYellowListItem3.spriteName = "AE_212_Yellow-Cpmp-1";
        step03_spriteYellowListItem4.spriteName = "AE_212_Yellow-1";
        step03_spriteProject2.spriteName = "AE_212_Project_2";
        step03_spriteBlueCompBtn.spriteName = "AE_212_Blue-Comp-Button";
        step03_spriteBlueTimeline.spriteName = "AE_212_Blue-Layer-2";
        step03_spriteBlueTab.spriteName = "AE_212_Blue-Button(Timeline)-2";
        step03_spriteIndicator.spriteName = "AE_212_indicator";

        step04_spriteBlueTimeline.spriteName = "AE_212_Blue-Layer-2";
        step04_spriteBlueCompBtn.spriteName = "AE_212_Blue-Comp-Button";
        step04_spriteProject2.spriteName = "AE_212_Project_2";
        step04_spriteVideoPreview.spriteName = "AE_212_Video_01-2-Preview";
        step04_spriteBlueListItem.spriteName = "AE_212_Blue-Comp-1";
        step04_spriteBlueListItem2.spriteName = "AE_212_Blue-1";
        step04_spriteVideoListItem.spriteName = "AE_212_Video_01-2";
        step04_spriteVideoListItem_dragable.spriteName = "AE_212_Video_01-2";
        step04_spriteYellowListItem.spriteName = "AE_212_Yellow-Cpmp-1";
        step04_spriteYellowListItem2.spriteName = "AE_212_Yellow-1";
        step04_spriteYellowTab.spriteName = "AE_212_Yellow-Button(Timeline)-1";
        step04_spriteBlueTab.spriteName = "AE_212_Blue-Button(Timeline)-1";
        step04_spriteCompIcon.spriteName = "AE_212_Comp-Icon";
        step04_spriteVideoListItem2.spriteName = "AE_212_Video-Comp-2";
        step04_spriteVideoListItem3.spriteName = "AE_212_Viedo_01-1";
        step04_spriteYellowListItem3.spriteName = "AE_212_Yellow-Cpmp-1";
        step04_spriteYellowListItem4.spriteName = "AE_212_Yellow-1";
        step04_spriteProject1.spriteName = "AE_212_Project_1";
        step04_spriteVideoCompBtn.spriteName = "AE_212_Video_01-Comp-Button";
        step04_spriteVideoTimeline.spriteName = "AE_212_Video_01-Layer-2";
        step04_spriteVideoTab.spriteName = "AE_212_Video_01-Button(Timeline)-2";
        step04_spriteIndicator.spriteName = "AE_212_indicator";

        step05_spriteVideoCompBtn.spriteName = "AE_212_Video_01-Comp-Button";
        step05_spriteYellowTab.spriteName = "AE_212_Yellow-Button(Timeline)-1";
        step05_spriteBlueTab.spriteName = "AE_212_Blue-Button(Timeline)-1";
        step05_spriteVideoTab.spriteName = "AE_212_Video_01-Button(Timeline)-2";
        step05_spriteProject1.spriteName = "AE_212_Project_1";
        step05_spriteYellowPreview.spriteName = "AE_212_Yellow-Preview";
        step05_spriteBlueListItem.spriteName = "AE_212_Blue-Comp-1";
        step05_spriteBlueListItem2.spriteName = "AE_212_Blue-1";
        step05_spriteVideoListItem.spriteName = "AE_212_Video_01-Comp-1";
        step05_spriteVideoListItem2.spriteName = "AE_212_Viedo_01-1";
        step05_spriteYellowListItem.spriteName = "AE_212_Yellow-Comp-2";
        step05_spriteYellowListItem_dragable.spriteName = "AE_212_Yellow-Comp-2";
        step05_spriteYellowListItem2.spriteName = "AE_212_Yellow-1";
        step05_spriteTimeline1.spriteName = "AE_212_Video_01-Layer-2";
        step05_spriteTimeline2.spriteName = "AE_212_Video_01-Layer-1";
        step05_spriteProject3.spriteName = "AE_212_Project_3";
        step05_spriteBlueListItem_dragable.spriteName = "AE_212_Blue-Comp-2";
        step05_spriteTimeline3.spriteName = "AE_212_Video_01-Layer-1";
        step05_spriteProject2.spriteName = "AE_212_Project_2";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);
        step01_groupKeyboard.SetActive(true);
        step01_keyboard_Ctrl.GetComponent<TweenScale>().enabled = true;
        step01_keyboard_Ctrl.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_keyboard_Ctrl.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
        {
            step01_keyboard_Ctrl.GetComponent<TweenScale>().enabled = false;
            step01_keyboard_Ctrl.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
            step01_keyboard_Ctrl.GetComponent<BoxCollider>().enabled = false;

            step01_keyboard_I.GetComponent<TweenScale>().enabled = true;
            step01_keyboard_I.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_keyboard_I.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
            {
                step01_keyboard_I.GetComponent<TweenScale>().enabled = false;
                step01_keyboard_I.transform.localScale = new Vector3(0.9f, 0.9f, 1f);

                CameraManager.ins.MoveCamera(step01_spriteImportPopup.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
                {
                    step01_groupKeyboard.SetActive(false);

                    step01_spriteImportPopup.gameObject.SetActive(true);
                    step01_spriteImportPopup.GetComponent<TweenScale>().onFinished.Clear();
                    step01_spriteImportPopup.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                    {
                        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_212_1", "TOP");
                        
                        step01_keyboard_Shift.gameObject.SetActive(true);
                        step01_keyboard_Shift.GetComponent<BoxCollider>().enabled = true;
                        step01_keyboard_Shift.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_keyboard_Shift.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                        {
                            step01_keyboard_Shift.GetComponent<TweenScale>().enabled = false;
                            step01_keyboard_Shift.transform.localScale = new Vector3(0.9f, 0.9f, 1f);

                            step01_box2.SetActive(true);
                            step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                            {
                                step01_box2.SetActive(false);

                                step01_spriteImport1.gameObject.SetActive(true);

                                step01_box3.SetActive(true);
                                step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                                step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                {
                                    step01_box3.SetActive(false);

                                    step01_spriteImport2.gameObject.SetActive(true);

                                    step01_box4.SetActive(true);
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                    {
                                        step01_box4.SetActive(false);

                                        step01_spriteImport3.gameObject.SetActive(true);

                                        step01_box5.SetActive(true);
                                        step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                        step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                        {
                                            LM_AE_Manager.ins.Off_Middle_Guide();
                                            step01_box5.SetActive(false);
                                            step01_keyboard_Shift.gameObject.SetActive(false);

                                            step01_spriteImportBtn.gameObject.SetActive(true);
                                            step01_spriteImportBtn.GetComponent<TweenAlpha>().onFinished.Clear();
                                            step01_spriteImportBtn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                                            {
                                                step01.SetActive(false);
                                                OnStep02();
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));


    }


    public void OnStep02()
    {
        step02_spriteYellowPreview.gameObject.SetActive(false);
        step02.SetActive(true);

        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate
        {
            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_212_2", "BOTTOM");
            step02_box2.SetActive(true);
            step02_box3.SetActive(true);
            step02_box3_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

            step02_spriteYellowListItem_dragable.gameObject.SetActive(true);
            step02_spriteYellowListItem_dragable.transform.localPosition = step02_spriteYellowListItem.transform.localPosition;
            step02_spriteYellowListItem_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = true;
                step02_spriteYellowListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            }));
            step02_spriteYellowListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
            step02_spriteYellowListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
            {
                CameraManager.SCREEN_MOVE_PAUSE = false;
                if (step02_spriteYellowListItem_dragable.transform.position.y <= step02_dragEnd.transform.position.y + 0.1f && step02_spriteYellowListItem_dragable.transform.position.y >= step02_dragEnd.transform.position.y - 0.1f)
                {
                    step02_spriteYellowPreview.gameObject.SetActive(true);
                    LM_AE_Manager.ins.Off_Middle_Guide();
                    step02_spriteCompIcon.gameObject.SetActive(false);
                    step02_box2.SetActive(false);
                    step02_box3.SetActive(false);
                    step02_spriteYellowListItem_dragable.gameObject.SetActive(false);

                    step02_spriteYellowListItem.spriteName = "AE_212_Yellow-Cpmp-1";
                    step02_groupYellow.SetActive(true);

                    delay0.gameObject.SetActive(true);
                    delay0.onFinished.Clear();
                    delay0.onFinished.Add(new EventDelegate(delegate
                    {
                        CameraManager.ins.MoveCamera(step02_spriteProject3.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
                        {
                            step02_glitters.SetActive(false);

                            delay1.gameObject.SetActive(true);
                            delay1.onFinished.Clear();
                            delay1.onFinished.Add(new EventDelegate(delegate
                            {
                                step02.SetActive(false);
                                OnStep03();
                                
                            }));
                        }));
                    }));
                } else
                {
                    step02_spriteYellowListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                    step02_spriteYellowListItem_dragable.transform.localPosition = step02_spriteYellowListItem.transform.localPosition;
                }
            }));
        }));
    }

    public void OnStep03()
    {
        step03_spriteBluePreview.spriteName = "AE_212_Yellow-Preview";
        step03_spriteBluePreview.GetComponent<UISprite>().aspectRatio = (float)step03_spriteBluePreview.GetComponent<UISprite>().GetAtlasSprite().width / (float)step03_spriteBluePreview.GetComponent<UISprite>().GetAtlasSprite().height;

        step03.SetActive(true);

        
        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate
        {
            step03_box4.SetActive(true);
            step03_box5.SetActive(true);
            step03_box5_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

            step03_spriteBlueListItem_dragable.gameObject.SetActive(true);
            step03_spriteBlueListItem_dragable.transform.localPosition = step03_spriteBlueListItem.transform.localPosition;
            step03_spriteBlueListItem_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = true;
                step03_spriteBlueListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            }));
            step03_spriteBlueListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
            step03_spriteBlueListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
            {
                CameraManager.SCREEN_MOVE_PAUSE = false;
                if (step03_spriteBlueListItem_dragable.transform.position.y <= step03_dragEnd.transform.position.y + 0.1f && step03_spriteBlueListItem_dragable.transform.position.y >= step03_dragEnd.transform.position.y - 0.1f)
                {
                    step03_spriteBluePreview.spriteName = "AE_212_Blue-Comp-2-Preview";
                    step03_spriteBluePreview.GetComponent<UISprite>().aspectRatio = (float)step03_spriteBluePreview.GetComponent<UISprite>().GetAtlasSprite().width / (float)step03_spriteBluePreview.GetComponent<UISprite>().GetAtlasSprite().height;

                    step03_spriteCompIcon.gameObject.SetActive(false);
                    step03_box4.SetActive(false);
                    step03_box5.SetActive(false);
                    step03_spriteBlueListItem_dragable.gameObject.SetActive(false);

                    step03_empty.gameObject.SetActive(false);
                    step03_spriteProject3.gameObject.SetActive(false);
                    step03_groupBlue.SetActive(true);

                    delay2.gameObject.SetActive(true);
                    delay2.onFinished.Clear();
                    delay2.onFinished.Add(new EventDelegate(delegate
                    {
                        CameraManager.ins.MoveCamera(step03_spriteProject2.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
                        {
                            step03_glitters.SetActive(false);

                            delay3.gameObject.SetActive(true);
                            delay3.onFinished.Clear();
                            delay3.onFinished.Add(new EventDelegate(delegate
                            {
                                step03.SetActive(false);
                                OnStep04();

                            }));
                        }));
                    }));
                } else
                {
                    step03_spriteBlueListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                    step03_spriteBlueListItem_dragable.transform.localPosition = step03_spriteBlueListItem.transform.localPosition;
                }
            }));
        }));
    }

    public void OnStep04()
    {
        step04_spriteVideoPreview.spriteName = "AE_212_Blue-Comp-2-Preview";
        step04_spriteVideoPreview.GetComponent<UISprite>().aspectRatio = (float)step04_spriteVideoPreview.GetComponent<UISprite>().GetAtlasSprite().width / (float)step04_spriteVideoPreview.GetComponent<UISprite>().GetAtlasSprite().height;

        step04.SetActive(true);

        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate
        {
            step04_box4.SetActive(true);
            step04_box5.SetActive(true);
            step04_box5_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

            step04_spriteVideoListItem_dragable.gameObject.SetActive(true);
            step04_spriteVideoListItem_dragable.transform.localPosition = step04_spriteVideoListItem.transform.localPosition;
            step04_spriteVideoListItem_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = true;
                step04_spriteVideoListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            }));
            step04_spriteVideoListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
            step04_spriteVideoListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
            {
                CameraManager.SCREEN_MOVE_PAUSE = false;
                if (step04_spriteVideoListItem_dragable.transform.position.y <= step04_dragEnd.transform.position.y + 0.1f && step04_spriteVideoListItem_dragable.transform.position.y >= step04_dragEnd.transform.position.y - 0.1f)
                {
                    step04_spriteVideoPreview.spriteName = "AE_212_Video_01-2-Preview";
                    step04_spriteVideoPreview.GetComponent<UISprite>().aspectRatio = (float)step04_spriteVideoPreview.GetComponent<UISprite>().GetAtlasSprite().width / (float)step04_spriteVideoPreview.GetComponent<UISprite>().GetAtlasSprite().height;

                    step04_spriteCompIcon.gameObject.SetActive(false);
                    step04_box4.SetActive(false);
                    step04_box5.SetActive(false);
                    step04_spriteVideoListItem_dragable.gameObject.SetActive(false);

                    step04_groupVideo.SetActive(true);

                    delay4.gameObject.SetActive(true);
                    delay4.onFinished.Clear();
                    delay4.onFinished.Add(new EventDelegate(delegate
                    {
                        CameraManager.ins.MoveCamera(step04_spriteProject1.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
                        {
                            step04_glitters.SetActive(false);

                            delay5.gameObject.SetActive(true);
                            delay5.onFinished.Clear();
                            delay5.onFinished.Add(new EventDelegate(delegate
                            {
                                step04.SetActive(false);
                                OnStep05();

                            }));
                        }));
                    }));
                }
                else
                {
                    step04_spriteVideoListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                    step04_spriteVideoListItem_dragable.transform.localPosition = step04_spriteVideoListItem.transform.localPosition;
                }
            }));
        }));
    }

    public void OnStep05()
    {

        step05_spriteYellowPreview.spriteName = "AE_212_Video_01-2-Preview";
        step05_spriteYellowPreview.GetComponent<UISprite>().aspectRatio = (float)step05_spriteYellowPreview.GetComponent<UISprite>().GetAtlasSprite().width / (float)step05_spriteYellowPreview.GetComponent<UISprite>().GetAtlasSprite().height;


        step05.SetActive(true);

        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate
        {
            step05_box6.SetActive(true);
            step05_box7.SetActive(true);
            step05_box7_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_212_3", "TOP");
            step05_spriteYellowListItem_dragable.gameObject.SetActive(true);
            step05_spriteYellowListItem_dragable.transform.localPosition = step05_spriteYellowListItem.transform.localPosition;
            step05_spriteYellowListItem_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = true;
                step05_spriteYellowListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            }));
            step05_spriteYellowListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
            step05_spriteYellowListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
            {
                CameraManager.SCREEN_MOVE_PAUSE = false;
                if (step05_spriteYellowListItem_dragable.transform.position.y <= step05_dragEnd1.transform.position.y + 0.2f && step05_spriteYellowListItem_dragable.transform.position.y >= step05_dragEnd1.transform.position.y - 0.2f)
                {
                    step05_spriteYellowPreview.spriteName = "AE_212_Yellow-Preview";
                    step05_spriteYellowPreview.GetComponent<UISprite>().aspectRatio = (float)step05_spriteYellowPreview.GetComponent<UISprite>().GetAtlasSprite().width / (float)step05_spriteYellowPreview.GetComponent<UISprite>().GetAtlasSprite().height;


                    LM_AE_Manager.ins.Off_Middle_Guide();
                    step05_box6.SetActive(false);
                    step05_box7.SetActive(false);
                    step05_spriteYellowListItem_dragable.gameObject.SetActive(false);

                    step05_spriteYellowListItem.spriteName = "AE_212_Yellow-Cpmp-1";
                    step05_spriteTimeline1.spriteName = "AE_212_Yellow-Layer-2";
                    step05_spriteTimeline2.gameObject.SetActive(true);
                    step05_spriteProject3.gameObject.SetActive(true);

                    delay6.gameObject.SetActive(true);
                    delay6.onFinished.Clear();
                    delay6.onFinished.Add(new EventDelegate(delegate
                    {
                        step05_box8.SetActive(true);
                        step05_box9.SetActive(true);
                        step05_box9_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

                        step05_spriteBlueListItem_dragable.gameObject.SetActive(true);
                        step05_spriteBlueListItem_dragable.transform.localPosition = step05_spriteBlueListItem.transform.localPosition;
                        step05_spriteBlueListItem_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                            CameraManager.SCREEN_MOVE_PAUSE = true;
                            step05_spriteBlueListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                        }));
                        step05_spriteBlueListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                        step05_spriteBlueListItem_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
                        {
                            CameraManager.SCREEN_MOVE_PAUSE = false;
                            if (step05_spriteBlueListItem_dragable.transform.position.y <= step05_dragEnd2.transform.position.y + 0.2f && step05_spriteBlueListItem_dragable.transform.position.y >= step05_dragEnd2.transform.position.y - 0.2f)
                            {

                                step05_spriteYellowPreview.spriteName = "AE_212_Blue-Comp-2-Preview";
                                step05_spriteYellowPreview.GetComponent<UISprite>().aspectRatio = (float)step05_spriteYellowPreview.GetComponent<UISprite>().GetAtlasSprite().width / (float)step05_spriteYellowPreview.GetComponent<UISprite>().GetAtlasSprite().height;


                                step05_box8.SetActive(false);
                                step05_box9.SetActive(false);
                                step05_spriteBlueListItem_dragable.gameObject.SetActive(false);

                                step05_spriteBlueListItem.spriteName = "AE_212_Blue-Comp-1";
                                step05_spriteTimeline1.spriteName = "AE_212_Blue-Layer-2";
                                step05_spriteTimeline2.spriteName = "AE_212_Yellow-Comp-Layer-1";
                                step05_spriteTimeline3.gameObject.SetActive(true);
                                step05_spriteProject2.gameObject.SetActive(true);
                                step05_glitters.SetActive(true);

                                delay7.gameObject.SetActive(true);
                                delay7.onFinished.Clear();
                                delay7.onFinished.Add(new EventDelegate(delegate
                                {
                                    CameraManager.ins.MoveCamera(step05_cameraMovePoint.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                        delay8.gameObject.SetActive(true);
                                        delay8.onFinished.Clear();
                                        delay8.onFinished.Add(new EventDelegate(delegate
                                        {
                                            StartCoroutine(EndDelay());
                                        }));
                                    }));
                                }));
                            }
                            else
                            {
                                step05_spriteBlueListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                                step05_spriteBlueListItem_dragable.transform.localPosition = step05_spriteBlueListItem.transform.localPosition;
                            }
                        }));
                    }));
                }
                else
                {
                    step05_spriteYellowListItem_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                    step05_spriteYellowListItem_dragable.transform.localPosition = step05_spriteYellowListItem.transform.localPosition;
                }
            }));
        }));
    }


    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_212_4");
    }
}
