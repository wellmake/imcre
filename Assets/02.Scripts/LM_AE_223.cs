﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_223 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step00_Guide")]

    [Header("Step01")]
    public GameObject step01;
    public UISprite step01_spriteTimelineItemA;
    public UISprite step01_spriteTimelineItemB;
    public GameObject step01_slider1;
    public GameObject step01_box1;
    public GameObject step01_box1_dragPointer;
    public GameObject step01_box2;
    public GameObject step01_box3;
    public UISprite step01_spriteTimelineA;
    public GameObject step01_keyboardGroup1;
    public GameObject step01_keyboardGroup1_Ctrl;
    public GameObject step01_keyboardGroup1_squareBraketRight;
    public UISprite step01_spriteTimelineA_copy;
    public UISprite step01_spriteTimelineItemC;
    public UISprite step01_spriteTimelineItemC_back;
    public GameObject step01_box4;
    public GameObject step01_keyboardGroup2;
    public GameObject step01_keyboardGroup2_delete;
    public UISprite step01_spriteTimelineD;
    public GameObject step01_box5;
    public GameObject step01_slider2;
    public UISprite step01_spriteTimelineB_back;
    public GameObject step01_box6;
    public GameObject step01_box7;
    public GameObject step01_box7_dragPointer;
    public UISprite step01_spriteTimelineItemB_dragable;
    public GameObject step01_box8;
    public GameObject step01_box9;
    public GameObject step01_box9_dragPointer;
    public UISprite step01_spriteTimelineItemB_last;
    public UISprite step01_spriteTimelineItemB_back_last;

    public TweenAlpha step01_delay0;
    public TweenAlpha step01_delay1;
    public TweenAlpha step01_delay2;

    [Header("Step02")]
    public GameObject step02;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_223_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));


    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteTimelineItemA);
        lmSpriteList.Add(step01_spriteTimelineItemB);
        lmSpriteList.Add(step01_spriteTimelineA);
        lmSpriteList.Add(step01_spriteTimelineA_copy);
        lmSpriteList.Add(step01_spriteTimelineItemC);
        lmSpriteList.Add(step01_spriteTimelineItemC_back);
        lmSpriteList.Add(step01_spriteTimelineD);
        lmSpriteList.Add(step01_spriteTimelineB_back);
        lmSpriteList.Add(step01_spriteTimelineItemB_dragable);
        lmSpriteList.Add(step01_spriteTimelineItemB_last);
        lmSpriteList.Add(step01_spriteTimelineItemB_back_last);


        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteTimelineItemA.spriteName = "AE_223_0001s_0001_img-003-copy-2";
        step01_spriteTimelineItemB.spriteName = "AE_223_0001s_0001_img-003-copy-2";
        step01_spriteTimelineA.spriteName = "AE_223_0001s_0002_A.mp4_";
        step01_spriteTimelineA_copy.spriteName = "AE_223_0000s_0001_Layer-2";
        step01_spriteTimelineItemC.spriteName = "AE_223_0000s_0000_img-009";
        step01_spriteTimelineItemC_back.spriteName = "AE_223_0000s_0002_Layer-3";
        step01_spriteTimelineD.spriteName = "AE_223_0001_img-010";
        step01_spriteTimelineB_back.spriteName = "AE_223_0001_img-010";
        step01_spriteTimelineItemB_dragable.spriteName = "AE_223_0000_Layer-1";
        step01_spriteTimelineItemB_last.spriteName = "AE_223_0000_Layer-1";
        step01_spriteTimelineItemB_back_last.spriteName = "AE_223_0001_img-010";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        CameraManager.ins.MoveCamera(step01_keyboardGroup2_delete.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
            step01_box1.SetActive(true);
            step01_box2.SetActive(true);
            step01_box1_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
            step01_slider1.GetComponent<BoxCollider>().enabled = true;

            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_223_1", "MID");

            step01_slider1.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                step01_slider1.transform.Find("Thumb").GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                CameraManager.SCREEN_MOVE_PAUSE = true;
            }));
            step01_slider1.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = false;
            }));

            step01_slider1.GetComponent<UISlider>().onChange.Clear();
            step01_slider1.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate {
                
                if (step01_slider1.GetComponent<UISlider>().value == 1f)
                {

                    step01_slider1.GetComponent<BoxCollider>().enabled = false;
                    LM_AE_Manager.ins.Off_Middle_Guide();

                    step01_box1.SetActive(false);
                    step01_box2.SetActive(false);

                    step01_box3.SetActive(true);
                    step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                    step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                        step01_box3.SetActive(false);
                        step01_spriteTimelineItemA.spriteName = "AE_223_0001s_0000_img-007";
                        step01_spriteTimelineA.gameObject.SetActive(true);

                        step01_keyboardGroup1.SetActive(true);
                        step01_keyboardGroup1_Ctrl.GetComponent<TweenScale>().enabled = true;
                        step01_keyboardGroup1_Ctrl.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_keyboardGroup1_Ctrl.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step01_keyboardGroup1_Ctrl.GetComponent<TweenScale>().enabled = false;
                            step01_keyboardGroup1_Ctrl.transform.localScale = new Vector3(0.9f, 0.9f, 1f);

                            step01_keyboardGroup1_squareBraketRight.GetComponent<TweenScale>().enabled = true;
                            step01_keyboardGroup1_squareBraketRight.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_keyboardGroup1_squareBraketRight.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                step01_keyboardGroup1_squareBraketRight.GetComponent<TweenScale>().enabled = false;
                                step01_keyboardGroup1.SetActive(false);

                                step01_spriteTimelineA_copy.gameObject.SetActive(true);
                                step01_spriteTimelineItemC.gameObject.SetActive(true);
                                step01_spriteTimelineItemC_back.gameObject.SetActive(true);

                                step01_delay0.gameObject.SetActive(true);
                                step01_delay0.onFinished.Clear();
                                step01_delay0.onFinished.Add(new EventDelegate(delegate {
                                    LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_223_2", "MID");

                                    step01_box4.SetActive(true);
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

                                        step01_box4.SetActive(false);
                                        step01_keyboardGroup2.SetActive(true);
                                        step01_keyboardGroup2_delete.GetComponent<TweenScale>().enabled = true;
                                        step01_keyboardGroup2_delete.GetComponent<UIEventTrigger>().onClick.Clear();
                                        step01_keyboardGroup2_delete.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                            LM_AE_Manager.ins.Off_Middle_Guide();

                                            step01_spriteTimelineA.gameObject.SetActive(false);
                                            step01_keyboardGroup2.SetActive(false);
                                            step01_spriteTimelineA_copy.gameObject.SetActive(false);
                                            step01_spriteTimelineItemC.gameObject.SetActive(false);
                                            step01_spriteTimelineItemC_back.gameObject.SetActive(false);
                                            step01_spriteTimelineD.gameObject.SetActive(true);

                                            step01_delay1.gameObject.SetActive(true);
                                            step01_delay1.onFinished.Clear();
                                            step01_delay1.onFinished.Add(new EventDelegate(delegate {
                                                LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_223_3", "MID");

                                                step01_box5.SetActive(true);
                                                step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                                step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                    LM_AE_Manager.ins.Off_Middle_Guide();
                                                    step01_box5.SetActive(false);
                                                    step01_spriteTimelineItemB.spriteName = "AE_223_0001s_0000_img-007";

                                                    step01_spriteTimelineB_back.gameObject.SetActive(true);
                                                    step01_box6.SetActive(true);
                                                    step01_box7.SetActive(true);
                                                    step01_box7_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                                                    step01_slider2.SetActive(true);
                                                    step01_slider2.GetComponent<BoxCollider>().enabled = true;
                                                    step01_slider2.GetComponent<UISlider>().onChange.Clear();
                                                    float itemB_width = step01_spriteTimelineItemB.width;
                                                    int itemB_rightAnchor = step01_spriteTimelineItemB.rightAnchor.absolute;
                                                    step01_slider2.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                                        CameraManager.SCREEN_MOVE_PAUSE = true;
                                                    }));
                                                    step01_slider2.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                                                        CameraManager.SCREEN_MOVE_PAUSE = false;
                                                    }));
                                                    step01_slider2.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate {
                                                        step01_spriteTimelineItemB.rightAnchor.Set(0.5f, ((-1f * (itemB_width / 2f)) * (1 - step01_slider2.GetComponent<UISlider>().value)) + itemB_rightAnchor);

                                                        step01_spriteTimelineItemB.spriteName = "AE_223_0000_Layer-1";

                                                        if (step01_slider2.GetComponent<UISlider>().value == 0f)
                                                        {
                                                            step01_box6.SetActive(false);
                                                            step01_box7.SetActive(false);
                                                            step01_slider2.SetActive(false);

                                                            step01_spriteTimelineItemB_dragable.gameObject.SetActive(true);
                                                            step01_box8.SetActive(true);
                                                            step01_box9.SetActive(true);
                                                            step01_box9_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                                                            step01_spriteTimelineItemB_dragable.transform.localPosition = step01_spriteTimelineItemB.transform.localPosition;
                                                            step01_spriteTimelineItemB_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                                                CameraManager.SCREEN_MOVE_PAUSE = true;
                                                                step01_spriteTimelineItemB_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                                                            }));
                                                            step01_spriteTimelineItemB_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                                                                CameraManager.SCREEN_MOVE_PAUSE = false;
                                                            }));
                                                            step01_spriteTimelineItemB_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                                                            step01_spriteTimelineItemB_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                                                                if(step01_spriteTimelineItemB_dragable.transform.localPosition.x < step01_spriteTimelineItemB.transform.localPosition.x - step01_spriteTimelineItemB_dragable.width + 20f)
                                                                {
                                                                    step01_spriteTimelineItemB_dragable.gameObject.SetActive(false);
                                                                    step01_box8.SetActive(false);
                                                                    step01_box9.SetActive(false);
                                                                    step01_spriteTimelineItemB.gameObject.SetActive(false);
                                                                    step01_spriteTimelineB_back.gameObject.SetActive(false);

                                                                    step01_spriteTimelineItemB_back_last.gameObject.SetActive(true);
                                                                    step01_spriteTimelineItemB_last.gameObject.SetActive(true);
                                                                    step01_delay2.gameObject.SetActive(true);
                                                                    step01_delay2.onFinished.Clear();
                                                                    step01_delay2.onFinished.Add(new EventDelegate(delegate {
                                                                        StartCoroutine(EndDelay());
                                                                    }));
                                                                } else
                                                                {
                                                                    step01_spriteTimelineItemB_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                                                                    step01_spriteTimelineItemB_dragable.transform.localPosition = step01_spriteTimelineItemB.transform.localPosition;
                                                                }
                                                            }));
                                                        }
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }

	        }));

        }));
    }


    public void OnStep02()
    {

    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_223_4");
    }
}
