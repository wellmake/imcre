﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_323 : MonoBehaviour
{
    Camera cam;
    Ray ray;
    RaycastHit hit;
    bool dragMode;
    bool dragOn;
    Vector3 mouseOriginPos;
    Vector3 mouseCurPos;
    float mouseOffset;
    int percentage = 100;

    float newX;

    public UISprite panel_Project_Inside;
    public GameObject boxb_mp4Select;
    public GameObject select;
    public GameObject fileName;
    public GameObject bar_mp4;
    public GameObject bar_Sprite_Touch;
    public UISprite monitor_Image;
    public GameObject boxb_BlendMode;
    public GameObject blendMode_List;
    public GameObject boxb_Screen;
    public GameObject boxb_Opacity;
    public GameObject blendMode_Screen;
    public GameObject boxb_Percent;
    public GameObject boxb_Percent_Drag;
    UILabel label_Percent;
    public GameObject opacity_70;
    public GameObject boxb_Eyeblue;
    public GameObject eye_Blue;
    public GameObject eyeblue_Sprite_Touch;
    void Start()
    {
        //LM_Manager.ins.UpdatePanelPosition();
        bar_mp4.SetActive(false);
        cam = Camera.main;
        label_Percent = boxb_Percent_Drag.GetComponentInChildren<UILabel>();
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_mp4Select);
            UIEventListener.Get(boxb_mp4Select).onClick = OnBoxbmp4SelectClick;
        };
        SetSprites();
    }
    void SetSprites()
    {
        List<UISprite> lmSpriteList = new List<UISprite>();
        lmSpriteList.Add(panel_Project_Inside);
        lmSpriteList.Add(select.GetComponent<UISprite>());
        lmSpriteList.Add(fileName.GetComponent<UISprite>());
        lmSpriteList.Add(bar_mp4.GetComponent<UISprite>());
        lmSpriteList.Add(monitor_Image);
        lmSpriteList.Add(blendMode_List.GetComponent<UISprite>());
        lmSpriteList.Add(blendMode_Screen.GetComponent<UISprite>());
        lmSpriteList.Add(opacity_70.GetComponent<UISprite>());
        lmSpriteList.Add(eye_Blue.GetComponent<UISprite>());

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }
    void OnBoxbmp4SelectClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        select.SetActive(true);
        newX = LM_Manager.ins.container.position.x +
            (Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 10)).x - LM_Manager.ins.panel_Timeline.transform.position.x);
        //TweenPosition.Begin(fileName, 1f, bar_mp4.transform.position, true);
        StartCoroutine(Co_MoveFileName());
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(newX);
        tp.SetOnFinished(() =>
        {
            bar_mp4.SetActive(true);
            bar_Sprite_Touch.SetActive(true);
            UIEventListener.Get(bar_mp4).onClick = OnBar_mp4Click;
        });
    }
    IEnumerator Co_MoveFileName()
    {
        while (Vector3.Distance(fileName.transform.position, bar_mp4.transform.position) > 0.2f)
        {
            fileName.transform.position = Vector3.Lerp(fileName.transform.position, bar_mp4.transform.position, Time.deltaTime * 2);
            yield return null;
        }
        select.SetActive(false);
    }
    void OnBar_mp4Click(GameObject gameObject)
    {
        UIEventListener.Get(bar_mp4).Clear();
        bar_mp4.GetComponent<TweenAlpha>().enabled = false;
        bar_mp4.GetComponent<UISprite>().alpha = 1f;
        bar_Sprite_Touch.SetActive(false);
        monitor_Image.gameObject.SetActive(true);
        monitor_Image.spriteName = "program_pannel_(2)";
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
        {
            LM_Manager.ins.panel_Source.spriteName = "effect_control";
            TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(0f);
            tp.SetOnFinished(() =>
            {
                LM_Manager.ins.BoxTweenScale(boxb_BlendMode);
                UIEventListener.Get(boxb_BlendMode).onClick = OnBoxb_BlendModeClick;
            });
        };
    }
    void OnBoxb_BlendModeClick(GameObject gameObject)
    {
        gameObject.SetActive(false);
        blendMode_List.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Screen);
        UIEventListener.Get(boxb_Screen).onClick = OnBoxb_ScreenClick;
    }
    void OnBoxb_ScreenClick(GameObject gameObject)
    {
        blendMode_List.SetActive(false);
        blendMode_Screen.SetActive(true);
        monitor_Image.spriteName = "program_pannel_(3)";
        LM_Manager.ins.BoxTweenScale(boxb_Opacity);
        UIEventListener.Get(boxb_Opacity).onClick = OnBoxb_OpacityClick;
    }
    void OnBoxb_OpacityClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        LM_Manager.ins.BoxTweenScale(boxb_Percent);
        UIEventListener.Get(boxb_Percent).onClick = OnBoxb_PercentClick;
    }
    void OnBoxb_PercentClick(GameObject gameObject)
    {
        gameObject.SetActive(false);
        LM_Manager.ins.BoxTweenScale(boxb_Percent_Drag);
        dragMode = true;
    }
    void Update()
    {
        if (dragMode)
        {
            DragPercentage();
        }
    }
    void DragPercentage()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("DragStart"))
            {
                Debug.Log("DragStart");
                mouseOriginPos = Input.mousePosition;
                //mouseOriginPos.z = boxb_Percent_Drag.transform.position.z - cam.transform.position.z;
                //mouseOriginPos = cam.ScreenToWorldPoint(mouseOriginPos);
            }
        }
        else if (Input.GetMouseButton(0))
        {
            mouseOffset = mouseOriginPos.x - Input.mousePosition.x;
            Debug.Log(mouseOffset);
            percentage -= Mathf.CeilToInt(mouseOffset * 0.01f);
            percentage = Mathf.Clamp(percentage, 70, 100);
            label_Percent.text = string.Format("{0}%", percentage);
            if(percentage == 70)
            {
                monitor_Image.spriteName = "program_pannel_(4)";
                LM_Manager.ins.panel_Pause.SetActive(true);
                UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnDragFinish;
                dragMode = false;
                return;
            }
        }
    }
    void OnDragFinish(GameObject gameObject)
    {
        boxb_Percent_Drag.SetActive(false);
        opacity_70.SetActive(true);
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(newX);
        tp.SetOnFinished(() =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_Eyeblue);
            UIEventListener.Get(boxb_Eyeblue).onClick = OnBoxb_EyeblueClick;
        });
    }
    void OnBoxb_EyeblueClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        eye_Blue.SetActive(true);
        monitor_Image.gameObject.SetActive(false);
        LM_Manager.ins.BoxTweenScale(boxb_Eyeblue);
        LM_Manager.ins.BoxTweenScale(eyeblue_Sprite_Touch);
        UIEventListener.Get(boxb_Eyeblue).onClick = OnBoxb_EyeblueClick_2;
    }
    void OnBoxb_EyeblueClick_2(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        monitor_Image.gameObject.SetActive(true);
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
    }
}
