﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_225 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
	public GameObject step01_box1;
	public UISprite step01_spriteMenu1;
    public GameObject step01_box2;
    public UISprite step01_spriteImport;
    public UISprite step01_spriteMenu2;
    public GameObject step01_box3;
    public UISprite step01_file_choosing;

    [Header("Step02")]
    public GameObject step02;
    public UISprite step02_spriteImportFile1;
    public GameObject step02_box4;
    public UISprite step02_spriteImportFile_ps;
    public GameObject step02_box5;
    public UISprite step02_spriteFotage1;
    public GameObject step02_glitters;
    public GameObject step02_box6;
    public UISprite step02_spriteFotage2;
    public UISprite step02_spriteProject1;
    public UISprite step02_spriteProject4;
    public UISprite step02_spriteProject4_dragable;
    public GameObject step02_box7;
    public GameObject step02_box8;
    public GameObject step02_box8_dragPointer;
    public GameObject step02_dragEnd;
    public UISprite step02_spriteProject3;
    public UISprite step02_spriteComp1;
    public UISprite step02_spriteComposition1;
    public UISprite step02_spriteComposition2;
    public UISprite step02_spriteTimeline2;
    public UISprite step02_spriteImportFile2;
    public GameObject step02_box9;
    public UISprite step02_spriteImportFile_ai;
    public GameObject step02_box10;
    public UISprite step02_spriteImportFile4;
    public GameObject step02_box11;
    public UISprite step02_spriteImportFile5;
    public UISprite step02_spriteImportFile6;
    public GameObject step02_box12;
    public UISprite step02_spriteProject6;
    public UISprite step02_spriteComp2;

    public TweenAlpha step01_delay0;
    public TweenAlpha step01_delay1;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_225_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_box1.GetComponent<UISprite>().rightAnchor.absolute = 91;

            step01_box2.GetComponent<UISprite>().topAnchor.absolute = 427;
            step01_box2.GetComponent<UISprite>().bottomAnchor.absolute = 371;

            step01_spriteImport.GetComponent<UISprite>().topAnchor.absolute = 418;
            step01_spriteImport.GetComponent<UISprite>().leftAnchor.absolute = 5;
            step01_spriteImport.GetComponent<UISprite>().rightAnchor.absolute = 591;

            step01_spriteMenu2.GetComponent<UISprite>().topAnchor.absolute = 423;

            step01_box3.GetComponent<UISprite>().topAnchor.absolute = 429;
            step01_box3.GetComponent<UISprite>().bottomAnchor.absolute = 366;

            step01_file_choosing.GetComponent<UISprite>().topAnchor.absolute = 419;
            step01_file_choosing.GetComponent<UISprite>().leftAnchor.absolute = 599;
            step01_file_choosing.GetComponent<UISprite>().rightAnchor.absolute = -424;

            step02_box5.GetComponent<UISprite>().bottomAnchor.absolute = -389;

            step02_box10.GetComponent<UISprite>().topAnchor.absolute = -153;
            step02_box10.GetComponent<UISprite>().bottomAnchor.absolute = -225;
            step02_box10.GetComponent<UISprite>().leftAnchor.absolute = -162;
            step02_box10.GetComponent<UISprite>().rightAnchor.absolute = 251;

            step02_spriteImportFile4.GetComponent<UISprite>().topAnchor.absolute = -223;
            step02_spriteImportFile4.GetComponent<UISprite>().bottomAnchor.absolute = -233;
            step02_spriteImportFile4.GetComponent<UISprite>().leftAnchor.absolute = -144;
            step02_spriteImportFile4.GetComponent<UISprite>().rightAnchor.absolute = 240;

            step02_box11.GetComponent<UISprite>().topAnchor.absolute = -246;
            step02_box11.GetComponent<UISprite>().bottomAnchor.absolute = -299;
            step02_box11.GetComponent<UISprite>().leftAnchor.absolute = -161;
            step02_box11.GetComponent<UISprite>().rightAnchor.absolute = 250;

            step02_spriteImportFile5.GetComponent<UISprite>().topAnchor.absolute = -261;
            step02_spriteImportFile5.GetComponent<UISprite>().bottomAnchor.absolute = -280;
            step02_spriteImportFile5.GetComponent<UISprite>().leftAnchor.absolute = -142;
            step02_spriteImportFile5.GetComponent<UISprite>().rightAnchor.absolute = 232;

            step02_spriteImportFile6.GetComponent<UISprite>().topAnchor.absolute = -177;
            step02_spriteImportFile6.GetComponent<UISprite>().bottomAnchor.absolute = -201;
            step02_spriteImportFile6.GetComponent<UISprite>().leftAnchor.absolute = -145;
            step02_spriteImportFile6.GetComponent<UISprite>().rightAnchor.absolute = 236;

            step02_box12.GetComponent<UISprite>().topAnchor.absolute = -318;
            step02_box12.GetComponent<UISprite>().bottomAnchor.absolute = -384;
            step02_box12.GetComponent<UISprite>().leftAnchor.absolute = 140;
            step02_box12.GetComponent<UISprite>().rightAnchor.absolute = 305;

            step01_spriteMenu1.GetComponent<UISprite>().aspectRatio = (float)step01_spriteMenu1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteMenu1.GetComponent<UISprite>().GetAtlasSprite().height;
            step01_spriteImport.GetComponent<UISprite>().aspectRatio = (float)step01_spriteImport.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteImport.GetComponent<UISprite>().GetAtlasSprite().height;
            step01_spriteMenu2.GetComponent<UISprite>().aspectRatio = (float)step01_spriteMenu2.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteMenu2.GetComponent<UISprite>().GetAtlasSprite().height;
            step01_file_choosing.GetComponent<UISprite>().aspectRatio = (float)step01_file_choosing.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_file_choosing.GetComponent<UISprite>().GetAtlasSprite().height;
            step02_spriteProject1.GetComponent<UISprite>().aspectRatio = (float)step02_spriteProject1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step02_spriteProject1.GetComponent<UISprite>().GetAtlasSprite().height;
        }
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteMenu1);
        lmSpriteList.Add(step01_spriteImport);
        lmSpriteList.Add(step01_spriteMenu2);
        lmSpriteList.Add(step01_file_choosing);
        lmSpriteList.Add(step02_spriteImportFile1);
        lmSpriteList.Add(step02_spriteImportFile_ps);
        lmSpriteList.Add(step02_spriteFotage1);
        lmSpriteList.Add(step02_spriteFotage2);
        lmSpriteList.Add(step02_spriteProject1);
        lmSpriteList.Add(step02_spriteProject4);
        lmSpriteList.Add(step02_spriteProject4_dragable);
        lmSpriteList.Add(step02_spriteProject3);
        lmSpriteList.Add(step02_spriteComp1);
        lmSpriteList.Add(step02_spriteComposition1);
        lmSpriteList.Add(step02_spriteComposition2);
        lmSpriteList.Add(step02_spriteTimeline2);
        lmSpriteList.Add(step02_spriteImportFile2);
        lmSpriteList.Add(step02_spriteImportFile_ai);
        lmSpriteList.Add(step02_spriteImportFile4);
        lmSpriteList.Add(step02_spriteImportFile5);
        lmSpriteList.Add(step02_spriteImportFile6);
        lmSpriteList.Add(step02_spriteProject6);
        lmSpriteList.Add(step02_spriteComp2);


        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteMenu1.spriteName = "AE_225_Menu-1";
        step01_spriteImport.spriteName = "AE_225_Import";
        step01_spriteMenu2.spriteName = "AE_225_Menu-2";
        step01_file_choosing.spriteName = "AE_225_File-choosing";
        step02_spriteImportFile1.spriteName = "AE_225_Import-File_1";
        step02_spriteImportFile_ps.spriteName = "AE_225_Import-File_2";
        step02_spriteFotage1.spriteName = "AE_225_Fotage_1";
        step02_spriteFotage2.spriteName = "AE_225_Fotage_2";
        step02_spriteProject1.spriteName = "AE_225_Project_1";
        step02_spriteProject4.spriteName = "AE_225_Project_4";
        step02_spriteProject4_dragable.spriteName = "AE_225_Project_4";
        step02_spriteProject3.spriteName = "AE_225_Project_3";
        step02_spriteComp1.spriteName = "AE_225_comp_1";
        step02_spriteComposition1.spriteName = "AE_225_Composition_1";
        step02_spriteComposition2.spriteName = "AE_225_Composition_2";
        step02_spriteTimeline2.spriteName = "AE_225_Timeline_2";
        step02_spriteImportFile2.spriteName = "AE_225_Import-File_1";
        step02_spriteImportFile_ai.spriteName = "AE_225_Import-File_3";
        step02_spriteImportFile4.spriteName = "AE_225_Import-File_4";
        step02_spriteImportFile5.spriteName = "AE_225_Import-File_5";
        step02_spriteImportFile6.spriteName = "AE_225_Import-File_6";
        step02_spriteProject6.spriteName = "AE_225_Project_6";
        step02_spriteComp2.spriteName = "AE_225_comp_2";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        step01.SetActive(true);
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            step01_box1.SetActive(false);

            step01_spriteMenu1.gameObject.SetActive(true);
            step01_box2.SetActive(true);
            step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                step01_box2.SetActive(false);
                step01_spriteImport.gameObject.SetActive(true);
                step01_spriteImport.GetComponent<TweenAlpha>().onFinished.Clear();
                step01_spriteImport.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                    step01_spriteMenu2.gameObject.SetActive(true);

                    CameraManager.ins.MoveCamera(step01_file_choosing.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                        step01_box3.SetActive(true);
                        step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step01_file_choosing.gameObject.SetActive(true);
                            step01_box3.SetActive(false);
                            step01_file_choosing.GetComponent<TweenAlpha>().onFinished.Clear();
                            step01_file_choosing.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                
                                step01.SetActive(false);

                                OnStep02();
                            }));
                        }));
                    }));
                }));
            }));
        }));

    }


    public void OnStep02()
    {

        step02.SetActive(true);

        CameraManager.ins.MoveCamera(step02_spriteImportFile1.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
            step02_spriteImportFile1.gameObject.SetActive(true);
            step02_spriteImportFile1.GetComponent<TweenScale>().onFinished.Clear();
            step02_spriteImportFile1.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                step02_box4.SetActive(true);
                step02_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                step02_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                    step02_box4.SetActive(false);
                    step02_spriteImportFile_ps.gameObject.SetActive(true);

                    step02_box5.SetActive(true);
                    step02_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                    step02_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                        step02_box5.SetActive(false);
                        step02_spriteImportFile1.gameObject.SetActive(false);
                        step02_spriteImportFile_ps.gameObject.SetActive(false);

                        step02_spriteFotage1.gameObject.SetActive(true);
                        step02_glitters.SetActive(true);
                        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_225_1", "BOTTOM");

                        step02_box6.SetActive(true);
                        step02_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                        step02_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step02_box6.SetActive(false);

                            step02_spriteFotage2.gameObject.SetActive(true);

                            step02_spriteFotage2.GetComponent<TweenAlpha>().onFinished.Clear();
                            step02_spriteFotage2.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                step02_spriteFotage1.gameObject.SetActive(false);
                                step02_glitters.SetActive(false);
                                LM_AE_Manager.ins.Off_Middle_Guide();
                                step02_spriteFotage2.gameObject.SetActive(false);

                                step02_spriteProject1.gameObject.SetActive(true);
                                step02_spriteProject4.gameObject.SetActive(true);

                                CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                    step02_box7.SetActive(true);
                                    step02_box8.SetActive(true);
                                    step02_box8_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

                                    step02_spriteProject4_dragable.gameObject.SetActive(true);
                                    step02_spriteProject4_dragable.transform.localPosition = step02_spriteProject4.transform.localPosition;
                                    step02_spriteProject4_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                        CameraManager.SCREEN_MOVE_PAUSE = true;
                                        step02_spriteProject4_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                                    }));
                                    step02_spriteProject4_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                                    step02_spriteProject4_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                                        CameraManager.SCREEN_MOVE_PAUSE = false;

                                        if (step02_spriteProject4_dragable.transform.position.y <= step02_dragEnd.transform.position.y + 0.1f && step02_spriteProject4_dragable.transform.position.y >= step02_dragEnd.transform.position.y - 0.1f)
                                        {
                                            step02_box7.SetActive(false);
                                            step02_box8.SetActive(false);

                                            step02_spriteProject4.gameObject.SetActive(false);
                                            step02_spriteProject4_dragable.gameObject.SetActive(false);

                                            step02_spriteProject1.spriteName = "AE_225_Project_1_comp";
                                            step02_spriteProject1.GetComponent<UISprite>().aspectRatio = (float)step02_spriteProject1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step02_spriteProject1.GetComponent<UISprite>().GetAtlasSprite().height;
                                            step02_spriteProject3.gameObject.SetActive(true);
                                            step02_spriteComp1.gameObject.SetActive(true);
                                            step02_spriteComposition1.gameObject.SetActive(true);
                                            step02_spriteComposition2.gameObject.SetActive(true);
                                            step02_spriteTimeline2.gameObject.SetActive(true);

                                            step01_delay0.gameObject.SetActive(true);
                                            step01_delay0.GetComponent<TweenAlpha>().onFinished.Clear();
                                            step01_delay0.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                CameraManager.ins.MoveCamera(step02_spriteImportFile2.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                                    LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_225_2", "BOTTOM");
                                                    step02_spriteImportFile2.gameObject.SetActive(true);
                                                    step02_spriteImportFile2.GetComponent<TweenScale>().onFinished.Clear();
                                                    step02_spriteImportFile2.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                                                        step02_box9.SetActive(true);
                                                        step02_box9.GetComponent<UIEventTrigger>().onClick.Clear();
                                                        step02_box9.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                            step02_box9.SetActive(false);

                                                            step02_spriteImportFile_ai.gameObject.SetActive(true);
                                                            step02_box10.SetActive(true);
                                                            step02_box10.GetComponent<UIEventTrigger>().onClick.Clear();
                                                            step02_box10.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                step02_box10.SetActive(false);

                                                                step02_spriteImportFile4.gameObject.SetActive(true);
                                                                step02_box11.SetActive(true);
                                                                step02_box11.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                step02_box11.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                    step02_box11.SetActive(false);

                                                                    step02_spriteImportFile5.gameObject.SetActive(true);
                                                                    step02_spriteImportFile5.GetComponent<TweenAlpha>().onFinished.Clear();
                                                                    step02_spriteImportFile5.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                                        step02_spriteImportFile4.gameObject.SetActive(false);
                                                                        step02_spriteImportFile5.gameObject.SetActive(false);

                                                                        step02_spriteImportFile6.gameObject.SetActive(true);

                                                                        step02_box12.SetActive(true);
                                                                        step02_box12.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                        step02_box12.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                            LM_AE_Manager.ins.Off_Middle_Guide();
                                                                            step02_box12.SetActive(false);
                                                                            step02_spriteImportFile2.gameObject.SetActive(false);
                                                                            step02_spriteImportFile_ai.gameObject.SetActive(false);
                                                                            step02_spriteImportFile6.gameObject.SetActive(false);

                                                                            step02_spriteProject6.gameObject.SetActive(true);
                                                                            step02_spriteComp2.gameObject.SetActive(true);

                                                                            CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                                                                step01_delay1.gameObject.SetActive(true);
                                                                                step01_delay1.GetComponent<TweenAlpha>().onFinished.Clear();
                                                                                step01_delay1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                                                    StartCoroutine(EndDelay());
                                                                                }));
                                                                            }));
                                                                        }));
                                                                    }));
                                                                }));

                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        } else
                                        {
                                            step02_spriteProject4_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                                            step02_spriteProject4_dragable.transform.localPosition = step02_spriteProject4.transform.localPosition;
                                        }
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_224_1");
    }
}
