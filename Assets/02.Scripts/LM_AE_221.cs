﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_221 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step00_Guide")]
    public UISprite step00_intro;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_keyboardGroup1;
    public GameObject step01_keyboard1_Ctrl;
    public GameObject step01_keyboard1_I;
    public UISprite step01_importFile;
    public GameObject step01_keyboard2_Ctrl;
    public UISprite step01_a;
    public UISprite step01_b;
    public UISprite step01_c;
    public UISprite step01_d;
    public GameObject step01_spriteCheck;
    public GameObject step01_box1;
    public GameObject step01_box2;
    public GameObject step01_box3;
    public GameObject step01_box4;
    public GameObject step01_box5;
    public GameObject step01_box6;
    public UISprite step01_newComp;
    public GameObject step01_box7;
    public UISprite step01_newComp_ok;

    [Header("Step02")]
    public GameObject step02;
    public UISprite step02_spriteBg3;
    public UISprite step02_spriteProject2;
    public UISprite step02_spriteProject2_bg;
    public UISprite step02_project1;
    public UISprite step02_timeline1;
    public GameObject step02_indicator;
    public GameObject step02_glitters1;
    public GameObject step02_glitters2;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_221_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_box5.GetComponent<UISprite>().topAnchor.absolute = -200;
            step01_box5.GetComponent<UISprite>().bottomAnchor.absolute = -258;
            step01_box5.GetComponent<UISprite>().leftAnchor.absolute = 293;
            step01_box5.GetComponent<UISprite>().rightAnchor.absolute = 353;

            step01_box6.GetComponent<UISprite>().topAnchor.absolute = -295;
            step01_box6.GetComponent<UISprite>().bottomAnchor.absolute = -367;
            step01_box6.GetComponent<UISprite>().leftAnchor.absolute = 232;
            step01_box6.GetComponent<UISprite>().rightAnchor.absolute = 402;

            step01_spriteCheck.GetComponent<UISprite>().topAnchor.absolute = -192;
            step01_spriteCheck.GetComponent<UISprite>().bottomAnchor.absolute = -262;
            step01_spriteCheck.GetComponent<UISprite>().leftAnchor.absolute = 313;
            step01_spriteCheck.GetComponent<UISprite>().rightAnchor.absolute = 339;

            step01_box7.GetComponent<UISprite>().topAnchor.absolute = -349;
            step01_box7.GetComponent<UISprite>().bottomAnchor.absolute = -415;
            step01_box7.GetComponent<UISprite>().leftAnchor.absolute = 167;
            step01_box7.GetComponent<UISprite>().rightAnchor.absolute = 364;

            step01_newComp_ok.GetComponent<UISprite>().topAnchor.absolute = -328;
            step01_newComp_ok.GetComponent<UISprite>().bottomAnchor.absolute = -435;
            step01_newComp_ok.GetComponent<UISprite>().leftAnchor.absolute = 179;
            step01_newComp_ok.GetComponent<UISprite>().rightAnchor.absolute = 353;
            step01_newComp.GetComponent<UISprite>().aspectRatio = (float)step01_newComp.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_newComp.GetComponent<UISprite>().GetAtlasSprite().height;
        }
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step00_intro);
        lmSpriteList.Add(step01_importFile);
        lmSpriteList.Add(step01_a);
        lmSpriteList.Add(step01_b);
        lmSpriteList.Add(step01_c);
        lmSpriteList.Add(step01_d);
        lmSpriteList.Add(step01_newComp);
        lmSpriteList.Add(step01_newComp_ok);
        lmSpriteList.Add(step02_spriteBg3);
        lmSpriteList.Add(step02_spriteProject2);
        lmSpriteList.Add(step02_spriteProject2_bg);
        lmSpriteList.Add(step02_project1);
        lmSpriteList.Add(step02_timeline1);


        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step00_intro.spriteName = "AE_221_bg_2";
        step01_importFile.spriteName = "AE_221_Import-File";
        step01_a.spriteName = "AE_221_A.mp4";
        step01_b.spriteName = "AE_221_B.mp4";
        step01_c.spriteName = "AE_221_C.mp4";
        step01_d.spriteName = "AE_221_D.mp4";
        step01_newComp.spriteName = "AE_221_New-Comp";
        step01_newComp_ok.spriteName = "AE_221_New-Comp_OK";
        step02_spriteBg3.spriteName = "AE_221_bg_3";
        step02_spriteProject2.spriteName = "AE_221_Project_2";
        step02_spriteProject2_bg.spriteName = "AE_221_bg_3";
        step02_project1.spriteName = "AE_221_Project_3";
        step02_timeline1.spriteName = "AE_221_Timeline_1";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        step01.SetActive(true);
        step01_keyboard1_Ctrl.GetComponent<TweenScale>().enabled = true;
        step01_keyboard1_I.GetComponent<TweenScale>().enabled = false;
        step01_keyboardGroup1.SetActive(true);
        step01_keyboard1_Ctrl.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_keyboard1_Ctrl.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
        {
            step01_keyboard1_Ctrl.GetComponent<TweenScale>().enabled = false;
            step01_keyboard1_Ctrl.transform.localScale = Vector3.one;

            step01_keyboard1_I.GetComponent<TweenScale>().enabled = true;
            step01_keyboard1_I.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_keyboard1_I.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
            {
                step01_keyboardGroup1.SetActive(false);
                CameraManager.ins.MoveCamera(step01_importFile.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
                {
                    step01_importFile.gameObject.SetActive(true);
                    step01_importFile.GetComponent<TweenScale>().onFinished.Clear();
                    step01_importFile.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                    {
                        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_221_1", "TOP");
                        step01_keyboard2_Ctrl.SetActive(true);
                        step01_keyboard2_Ctrl.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_keyboard2_Ctrl.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                        {
                            step01_keyboard2_Ctrl.GetComponent<TweenScale>().enabled = false;
                            step01_keyboard2_Ctrl.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
                            step01_box1.SetActive(true);
                            step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                            {
                                step01_box1.SetActive(false);
                                step01_a.gameObject.SetActive(true);
                                step01_box2.SetActive(true);
                                step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                                step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                {
                                    step01_box2.SetActive(false);
                                    step01_b.gameObject.SetActive(true);
                                    step01_box3.SetActive(true);
                                    step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                    {
                                        step01_box3.SetActive(false);
                                        step01_c.gameObject.SetActive(true);
                                        step01_box4.SetActive(true);
                                        step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                        step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                        {
                                            step01_box4.SetActive(false);
                                            step01_d.gameObject.SetActive(true);
                                            step01_keyboard2_Ctrl.SetActive(false);

                                            step01_box5.SetActive(true);
                                            step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                            step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                            {
                                                step01_box5.SetActive(false);

                                                step01_spriteCheck.SetActive(true);
                                                
                                                step01_box6.SetActive(true);
                                                step01_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                                                step01_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                {
                                                    step01_box6.SetActive(false);
                                                    step01_importFile.gameObject.SetActive(false);
                                                    step01_a.gameObject.SetActive(false);
                                                    step01_b.gameObject.SetActive(false);
                                                    step01_c.gameObject.SetActive(false);
                                                    step01_d.gameObject.SetActive(false);
                                                    step01_spriteCheck.SetActive(false);
                                                    LM_AE_Manager.ins.Off_Middle_Guide();

                                                    step01_newComp.gameObject.SetActive(true);
                                                    step01_newComp.GetComponent<TweenScale>().onFinished.Clear();
                                                    step01_newComp.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                                                    {
                                                        step01_box7.SetActive(true);
                                                        step01_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                                                        step01_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                        {
                                                            step01_box7.SetActive(false);
                                                            step01_newComp_ok.gameObject.SetActive(true);
                                                            step01_newComp_ok.GetComponent<TweenAlpha>().onFinished.Clear();
                                                            step01_newComp_ok.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                                                            {
                                                                step00_intro.gameObject.SetActive(false);
                                                                step01_newComp.gameObject.SetActive(false);
                                                                step01_box7.gameObject.SetActive(false);
                                                                step01_newComp_ok.gameObject.SetActive(false);
                                                                OnStep02();
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));


            }));
        }));
    }

    public void OnStep02()
    {
        step02.SetActive(true);

        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
            step02_glitters1.SetActive(true);
            step02_glitters2.SetActive(true);

            step02_glitters1.GetComponent<TweenAlpha>().onFinished.Clear();
            step02_glitters1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                StartCoroutine(EndDelay());
            }));
        }));

    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_221_2");
    }
}
