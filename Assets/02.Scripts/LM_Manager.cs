﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class LM_Manager : MonoBehaviour
{
    public static LM_Manager ins;
    public Transform container;
    public UILabel titleLabel;
    public ParticleSystem clickParticle;
    public List<GameObject> modes;
    Camera cam;
    Ray ray;
    RaycastHit hit;

    GameObject curLevel;
    UIWidget panel_Source_UIWidget;
    public Vector3 containerOriginPos;

    [Header("Ref_ScreenWidth")]
    public UIWidget titlePanel;

    [Header("Sprites")]
    public UISprite topMenu;
    public UISprite underMenu;
    public UISprite panel_Source;
    public UISprite panel_Project;
    public UISprite panel_Monitor;
    public UISprite panel_Tool;
    public UISprite panel_Audio;
    public UISprite panel_Timeline;
    public NGUIAtlas common_Atlas;
    public NGUIAtlas lm_Atlas;
    List<UISprite> panel_Sprites;

    [Header("Popup Panels")]
    public GameObject exitWindow;
    public GameObject panel_Pause;
    public GameObject guide_Touch;
    public GameObject panel_Complete;


    [Header("Guide Panels")]
    public GameObject start_guide;
    public GameObject Btn_Next;
    public GameObject Btn_Skip;
    public UILabel start_guide_Lable;

    public GameObject middle_guide;
    public UILabel middle_guide_Lable;

    public GameObject end_guide;
    public UILabel end_guide_Lable;

    [Header("Stage Setting")]
    public Language curLanguage;
    public Stage stage;
    public static Stage static_stage;
    public static Language static_curLanguage;

    [Header("Option")]
    public GameObject panel_Option;
    public UIToggle toggleLanguage;
    public UITable table;
    public GameObject item;
    static bool isOptionSelected = false;
    public UILabel selectedStage;


    public csvReader csvreader;

    public List<Dictionary<string, string>> learningmode_guide_en;
    public List<Dictionary<string, string>> learningmode_guide_ko;
    public List<Dictionary<string, string>> learningmode_title_en;
    public List<Dictionary<string, string>> learningmode_title_ko;

    public enum Language
    {
        en,
        kr
    }
    public enum Stage
    {
        LM_112,
        LM_213,
        LM_224,
        LM_231,
        LM_241,
        LM_252,
        LM_261,
        LM_271,
        LM_311,
        LM_323,
        LM_332
    }

    private void Awake()
    {
        if (isOptionSelected)
        {
            stage = static_stage;
            curLanguage = static_curLanguage;
            Debug.Log(stage + ", " + curLanguage);
        }
        ins = this;
        curLevel = modes.Find(x => x.name.Contains(stage.ToString()));
        foreach (var stage in modes)
        {
            if (stage.Equals(curLevel))
                continue;
            stage.SetActive(false);
        }
        AddPanels();
        SetAtlas();
        Init_Sprites();        
        panel_Source_UIWidget = panel_Source.GetComponent<UIWidget>();
        Init_Stage();
    }
    private void Start()
    {
        containerOriginPos = container.position;
        //panel_Pause.SetActive(true);
        cam = Camera.main;

        for (int i = 0; i < modes.Count; i++)
        {
            GameObject instance = Instantiate(item, table.transform);
            instance.name = instance.GetComponentInChildren<UILabel>().text = modes[i].name;
            UIEventListener.Get(instance).onClick = Option_SetStage;
            UIEventListener.Get(instance).onClick += Option_OnStageSelect;
            instance.GetComponent<UIDragScrollView>().scrollView = table.GetComponent<UIScrollView>();
        }
        UpdatePanelPosition();

        scvRead();
    }

    public void scvRead() {
        learningmode_guide_en = csvreader.Read("learningmode_guide_en");
        learningmode_guide_ko = csvreader.Read("learningmode_guide_ko");
        learningmode_title_en = csvreader.Read("learningmode_title_en.csv");
        learningmode_title_ko = csvreader.Read("learningmode_title_ko.csv");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            clickParticle.transform.position = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
            clickParticle.Play();
        }
    }
    void AddPanels()
    {
        panel_Sprites = new List<UISprite>();
        panel_Sprites.Add(panel_Source);
        panel_Sprites.Add(panel_Project);
        panel_Sprites.Add(panel_Monitor);
        panel_Sprites.Add(panel_Tool);
        panel_Sprites.Add(panel_Timeline);
        panel_Sprites.Add(panel_Audio);
    }
    void SetAtlas()
    {
        if (curLanguage == Language.en)
        {
            lm_Atlas = Resources.Load(curLevel.name) as NGUIAtlas;
        }
        else if (curLanguage == Language.kr)
        {
            lm_Atlas = Resources.Load(curLevel.name + "_kr") as NGUIAtlas;
        }
    }
    void Init_Sprites()
    {
        if (curLanguage == Language.en)
        {
            topMenu.spriteName = "menu_en";
            underMenu.spriteName = "pr_menu_en";
        }
        else if (curLanguage == Language.kr)
        {
            topMenu.spriteName = "menu_kr";
            underMenu.spriteName = "pr_menu_kr";
        }
        foreach (var item in panel_Sprites)
        {
            item.atlas = lm_Atlas;
        }
        panel_Source.spriteName = "panel_Source";
        panel_Project.spriteName = "panel_Project";
        panel_Monitor.spriteName = "panel_Monitor";
        panel_Tool.spriteName = "panel_Tool";
        panel_Timeline.spriteName = "panel_Timeline";
        panel_Audio.spriteName = "panel_Audio";
    }
    void Init_Stage()
    {
        if (curLevel == null)
        {
            Debug.LogError("Level not found");
        }
        curLevel.SetActive(true);
        switch (curLevel.name)
        {
            case "LM_112":
                SetPanel_SourceDimension(titlePanel.width * 3 / 5, titlePanel.height * 6);
                SetTitleText("Simplest form of ‘Video Editing’!", "간단한 방법으로 '영상편집' 뚝딱!");
                break;
            case "LM_213":
                SetPanel_SourceDimension(titlePanel.width, titlePanel.height * 5);
                SetTitleText("Let's Customize the Workspace", "작업하는 공간을 내 맘대로 만들자");
                break;
            case "LM_224":
                SetPanel_SourceDimension(titlePanel.width * 3 / 4, titlePanel.height * 4);
                SetTitleText("#ProjectPanel #BasicUse #Leggo", "#프로젝트패널 #기본사용법 #가즈아");
                break;
            case "LM_231":
                SetPanel_SourceDimension(titlePanel.width * 1 / 3, titlePanel.height * 3);
                SetTitleText("Timeline Panel is where the Editing happens", "타임라인 패널은 영상을 편집하는 곳");
                break;
            case "LM_241":
                SetPanel_SourceDimension(titlePanel.width * 1 / 3, titlePanel.height * 4);
                SetTitleText("Cut, Paste and Move Videos", "영상을 자르고~ 붙이고~ 착착 배치하기");
                break;
            case "LM_252":
                SetPanel_SourceDimension(titlePanel.width * 2 / 3, titlePanel.height * 6);
                SetTitleText("Subtitles and Shapes, all in Legacy Title!", "레거시 타이틀(제목)에서 자막과 도형을 한번에!");
                break;
            case "LM_261":
                SetPanel_SourceDimension(titlePanel.width, titlePanel.height * 7);
                SetTitleText("Putting Motion in Action (Keyframe 1)", "영상에 움직임을 넣어봐요 (키프레임1)");
                break;
            case "LM_271":
                SetPanel_SourceDimension(titlePanel.width * 4 / 5, titlePanel.height * 6);
                SetTitleText("If you are done Editing, go Exporting", "편집을 다 했다면, 꼭 영상으로 출력해요");
                break;
            case "LM_311":
                SetPanel_SourceDimension(titlePanel.width * 4 / 5, titlePanel.height * 7);
                SetTitleText("Going from Video A → Video B! Transition", "A영상→B영상으로 넘어갈 때! 트렌지션");
                break;
            case "LM_323":
                SetPanel_SourceDimension(titlePanel.width * 4 / 5, titlePanel.height * 7);
                SetTitleText("Spice up your Video with Blending Mode!", "블랜드(중첩)모드로 영상 분위기를 바꿔봐!");
                break;
            case "LM_332":
                SetPanel_SourceDimension(titlePanel.width * 4 / 5, titlePanel.height * 6);
                SetTitleText("Practicing Masking", "마스크 작업 응용해보기");
                break;
            default:
                break;
        }
    }
    void SetPanel_SourceDimension(int width, int height)
    {
        panel_Source_UIWidget.SetDimensions(width, height);
        UpdatePanelPosition();
    }
    // Run this inside Init_Stage() if panels are not fit
    public void UpdatePanelPosition()
    {
        foreach (var item in panel_Sprites)
        {
            item.ResetAndUpdateAnchors();
        }
    }
    void SetTitleText(string title_en, string title_kr)
    {
        if (curLanguage == Language.en)
        {
            titleLabel.text = title_en;
        }
        else if (curLanguage == Language.kr)
        {
            titleLabel.text = title_kr;
        }
    }
    public void DeActivateCurrentGuideBlue()
    {
        // Deactivate Guide_Blue
        ray = cam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit) && (hit.collider.tag == "Guide_Blue"))
        {
            hit.collider.gameObject.SetActive(false);
        }
    }
    public TweenPosition MoveContainerHorizontally(float newX)
    {
        //Debug.Log(newX);
        Vector3 newPos = containerOriginPos;
        newPos.x = newX;
        return TweenPosition.Begin(container.gameObject, 0.5f, newPos, true);
    }
    public TweenScale BoxTweenScale(GameObject box)
    {
        box.SetActive(true);
        TweenScale ts = TweenScale.Begin(box, 0.3f, Vector3.one);
        ts.from = Vector3.zero;
        return ts;
    }

    public void Set_Start_Guide(string Guide_text, UIEventListener.VoidDelegate func) {
        start_guide.SetActive(true);

        if (curLanguage == Language.en) {
           for(int i = 0; i < learningmode_guide_en.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_en[i];
                string value;
                if(tmp.TryGetValue(Guide_text, out value)) {                    
                    start_guide_Lable.text = value;
                }
                
            }
            
        } else if (curLanguage == Language.kr) {
            for (int i = 0; i < learningmode_guide_en.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_ko[i];
                string value;
                if(tmp.TryGetValue(Guide_text, out value)) {
                    start_guide_Lable.text = value;
                }
            }        
        }
        UIEventListener.Get(Btn_Next).onClick = func;
        UIEventListener.Get(Btn_Skip).onClick = func;
    }

    public void On_Middle_Guide(string Guide_text) {
        middle_guide.SetActive(true);
        if (curLanguage == Language.en) {
            for (int i = 0; i < learningmode_guide_en.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_en[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    start_guide_Lable.text = value;
                }

            }

        } else if (curLanguage == Language.kr) {
            for (int i = 0; i < learningmode_guide_en.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_ko[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    start_guide_Lable.text = value;
                }
            }
        }
    }

    public void Off_Middle_Guide() {
        middle_guide.SetActive(false);
    }


    public void On_End_Guide(string Guide_text) {
        end_guide.SetActive(true);
        if (curLanguage == Language.en) {
            for (int i = 0; i < learningmode_guide_en.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_en[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    start_guide_Lable.text = value;
                }

            }

        } else if (curLanguage == Language.kr) {
            for (int i = 0; i < learningmode_guide_en.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_ko[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    start_guide_Lable.text = value;
                }
            }
        }
    }

    public void Off_End_Guide() {
        end_guide.SetActive(false);
    }

    public void On_Start_Guide_Click() {
        start_guide.SetActive(false);
    }

    public void OnXButtonClick()
    {
        exitWindow.SetActive(true);
    }
    public void OnRepeatButtonClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void OnGuide_TouchClick()
    {
        panel_Pause.SetActive(false);
    }
    public void OnCloseButtonClick()
    {
        Debug.Log("Quit");
        Application.Quit();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }
    public void OnCancelButtonClick()
    {
        exitWindow.SetActive(false);
    }
    public void OnLevelComplete_213()
    {
        panel_Complete.SetActive(true);
    }
    public void OnLevelComplete(GameObject gameObject)
    {
        panel_Complete.SetActive(true);
    }


    #region Option Functions

    public void OnOptionClick()
    {
        panel_Option.SetActive(true);
    }
    void Option_SetLanguage()
    {
        if (toggleLanguage.value)
            static_curLanguage = Language.en;
        else
            static_curLanguage = Language.kr;
    }
    void Option_SetStage(GameObject gameObject)
    {
        stage = (Stage)Enum.Parse(typeof(Stage), gameObject.name);
    }
    public void Option_OnStartClick()
    {
        isOptionSelected = true;
        Option_SetLanguage();
        static_stage = stage;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Option_OnCancelClick()
    {
        panel_Option.SetActive(false);
    }
    public void Option_OnStageSelect(GameObject gameObject)
    {
        selectedStage.text = gameObject.name;
    }

    #endregion
}
