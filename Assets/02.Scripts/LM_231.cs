﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_231 : MonoBehaviour
{
    [Header("Step01")]
    public GameObject step01;
    Transform panel_Timeline;
    public Transform dragItem;
    Vector3 newDragItemPosition;
    public Transform lineTop;
    public Transform v1_Clip;
    [Header("Step01_Sprites")]
    public UISprite v2Clip_long_Sprite;
    public UISprite fx_gray_1_Sprite;
    //public UISprite v1Clip_Sprite;
    public UISprite fx_gray_2_Sprite;
    public UISprite fx_gray_3_Sprite;
    public UISprite triangle_left_1_Sprite;
    public UISprite triangle_left_2_Sprite;
    public UISprite timeline_v3_Sprite;
    public UISprite lineBottom_Sprite;
    public UISprite dragItem_Sprite;
    //public UISprite lineTop_Sprite;

    [Header("Step02")]
    public GameObject step02;
    public GameObject mouse_R;
    public GameObject mouse_Pointer;
    public Transform v2_fx;
    public GameObject mouse_rightClick;
    public GameObject rightClickMenu;
    public GameObject boxb_Opacity;
    public GameObject boxb_Menu_Opacity;
    public UISprite opacity;
    [Header("Step02_Sprites")]
    public UISprite timeline_v2_Sprite;
    public UISprite step02_timeline_v3_Sprite;
    public UISprite v2_Clip_big_Sprite;
    //public UISprite v2_fx_gray;
    public UISprite motion_Sprite;
    public UISprite timeRemapping_Sprite;
    public UISprite menu_Opacity_Sprite;

    [Header("Step03")]
    public GameObject step03;
    public Transform keyframe_Blue;
    public Transform guideLine_Left;
    float keyframe_Blue_OriginY;
    public Transform box_Blank;
    public float keyframe_fitIn_Offset = 0.07f;
    [Header("Step03_Sprites")]
    public UISprite keyframe_Gray;
    public UISprite guideLine;
    public UISprite guideLine_Right;

    List<UISprite> lmSpriteList;
    Camera cam;
    Ray ray;
    RaycastHit hit;
    bool DragOn;

    public enum Step
    {
        step01,
        step03
    }
    public Step step;
    void Start()
    {
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick += OnStart_MoveCamera;
        lmSpriteList = new List<UISprite>();
        panel_Timeline = LM_Manager.ins.panel_Timeline.transform;
        cam = Camera.main;
        UIEventListener.Get(mouse_rightClick).onClick += OnMouseRightClick;
        UIEventListener.Get(boxb_Opacity).onClick += OnOpacityClick;
        UIEventListener.Get(boxb_Menu_Opacity).onClick += OnStep03;
        step01.SetActive(true);
        step02.SetActive(false);
        step03.SetActive(false);
        step = Step.step01;
        SetSprites();
    }
    void SetSprites()
    {
        UISprite v2Clip_long = v2Clip_long_Sprite;
        UISprite fx_gray_1 = fx_gray_1_Sprite;
        UISprite v1Clip = v1_Clip.GetComponent<UISprite>();
        UISprite fx_gray_2 = fx_gray_2_Sprite;
        UISprite fx_gray_3 = fx_gray_3_Sprite;
        UISprite triangle_left_1 = triangle_left_1_Sprite;
        UISprite triangle_left_2 = triangle_left_2_Sprite;
        UISprite timeline_v3 = timeline_v3_Sprite;
        UISprite lineBottom = lineBottom_Sprite;
        UISprite lineTop = this.lineTop.GetComponent<UISprite>();
        UISprite dragItem = dragItem_Sprite;

        UISprite opacity = this.opacity;
        UISprite timeline_v2 = timeline_v2_Sprite;
        UISprite step02_timeline_v3 = step02_timeline_v3_Sprite;
        UISprite v2_Clip_big = v2_Clip_big_Sprite;
        UISprite v2_fx_gray = v2_fx.GetComponent<UISprite>();
        UISprite motion = motion_Sprite;
        UISprite timeRemapping = timeRemapping_Sprite;
        UISprite menu_Opacity = menu_Opacity_Sprite;

        UISprite guideline = guideLine;
        UISprite keyframe_B = keyframe_Blue.GetComponent<UISprite>();
        UISprite keyframe_G = keyframe_Gray;
        UISprite guideline_Right = guideLine_Right;
        UISprite guideline_Left = guideLine_Left.GetComponent<UISprite>();

        lmSpriteList.Add(v2Clip_long);
        lmSpriteList.Add(fx_gray_1);
        lmSpriteList.Add(v1Clip);
        lmSpriteList.Add(fx_gray_2);
        lmSpriteList.Add(fx_gray_3);
        lmSpriteList.Add(triangle_left_1);
        lmSpriteList.Add(triangle_left_2);
        lmSpriteList.Add(timeline_v3);
        lmSpriteList.Add(lineBottom);
        lmSpriteList.Add(lineTop);
        lmSpriteList.Add(dragItem);

        lmSpriteList.Add(opacity);
        lmSpriteList.Add(timeline_v2);
        lmSpriteList.Add(step02_timeline_v3);
        lmSpriteList.Add(v2_Clip_big);
        lmSpriteList.Add(v2_fx_gray);
        lmSpriteList.Add(motion);
        lmSpriteList.Add(timeRemapping);
        lmSpriteList.Add(menu_Opacity);

        lmSpriteList.Add(guideline);
        lmSpriteList.Add(keyframe_B);
        lmSpriteList.Add(keyframe_G);
        lmSpriteList.Add(guideline_Right);
        lmSpriteList.Add(guideline_Left);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }

        v2Clip_long.spriteName = "V2_clip_(long)";
        fx_gray_1.spriteName = "fx_gray_box";
        v1Clip.spriteName = "V1_clip_(long)";
        fx_gray_2.spriteName = "fx_gray_box";
        fx_gray_3.spriteName = "fx_gray_box";
        triangle_left_1.spriteName = "triangle_left";
        triangle_left_2.spriteName = "triangle_left";
        timeline_v3.spriteName = "timelinepanel_v3";
        lineBottom.spriteName = "dots_line";
        lineTop.spriteName = "dots_line";
        dragItem.spriteName = "dots_line";

        opacity.spriteName = "fx_box2";
        timeline_v2.spriteName = "timelinepanel_v2";
        step02_timeline_v3.spriteName = "timelinepanel_v3";
        v2_Clip_big.spriteName = "V2_clip_(big)";
        v2_fx_gray.spriteName = "fx_gray_box";
        motion.spriteName = "fx_box1";
        timeRemapping.spriteName = "fx_box3";
        menu_Opacity.spriteName = "fx_box2";

        guideline.spriteName = "line_a";
        keyframe_B.spriteName = "key_frame_blue";
        keyframe_G.spriteName = "key_frame_gray";
        guideline_Right.spriteName = guideline_Left.spriteName = "line_a";
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 angle = keyframe_Blue.position - guideLine_Left.position;
            Debug.Log(angle);
            Debug.Log(Mathf.Atan2(angle.y, angle.x) * Mathf.Rad2Deg);
        }

        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit) && hit.collider.tag == "DragStart")
            {
                Debug.Log("DragStart");
                DragOn = true;
                dragItem.gameObject.SetActive(true);
            }
        }
        if (Input.GetMouseButton(0) && DragOn)
        {
            newDragItemPosition = Input.mousePosition;
            newDragItemPosition.z = -cam.transform.position.z;
            newDragItemPosition = cam.ScreenToWorldPoint(newDragItemPosition);
            if (step == Step.step01)
            {
                newDragItemPosition.x = lineTop.position.x;
                dragItem.position = newDragItemPosition;
            }
            else if (step == Step.step03)
            {
                newDragItemPosition.x = keyframe_Blue.position.x;
                newDragItemPosition.y = Mathf.Clamp(newDragItemPosition.y, box_Blank.position.y, keyframe_Blue.parent.position.y);
                keyframe_Blue.position = newDragItemPosition;

                Vector3 angle = keyframe_Blue.position - guideLine_Left.position;
                guideLine_Left.localRotation = Quaternion.Euler(0, 0, Mathf.Atan2(angle.y, angle.x) * Mathf.Rad2Deg);
            }
        }
        if (DragOn && Input.GetMouseButtonUp(0))
        {
            if (step == Step.step01)
            {
                ray = cam.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit) && hit.collider.tag == "DragEnd" && DragOn)
                {
                    Debug.Log("DragEnd");
                    LM_Manager.ins.panel_Pause.SetActive(true);
                    if (step == Step.step01)
                    {
                        step01.SetActive(false);
                        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnStep02;
                    }
                }
                dragItem.gameObject.SetActive(false);
            }
            if (step == Step.step03)
            {
                if ((keyframe_Blue.position.y - box_Blank.position.y) < keyframe_fitIn_Offset)
                {
                    Debug.Log("finish");
                    LM_Manager.ins.panel_Pause.SetActive(true);
                    UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
                }
            }
            DragOn = false;
        }
    }
    void OnStart_MoveCamera(GameObject gameObject)
    {
        TweenPosition tp =
        LM_Manager.ins.MoveContainerHorizontally(cam.ScreenToWorldPoint(new Vector3(0, 0, 10f)).x - panel_Timeline.position.x);
        tp.AddOnFinished(SetTimelineSize);
        //SetTimelineSize();
    }
    void SetTimelineSize()
    {
        step01.SetActive(true);
        UISprite timelineSprite = panel_Timeline.GetComponent<UISprite>();
        timelineSprite.SetDimensions(LM_Manager.ins.titlePanel.width, timelineSprite.height);
        foreach (var item in step01.GetComponentsInChildren<UISprite>())
        {
            item.ResetAndUpdateAnchors();
        }
    }
    void OnStep02(GameObject gameObject)
    {
        lineTop.parent = step02.transform;
        v1_Clip.parent = step02.transform;
        step02.SetActive(true);
        step01.SetActive(false);
        foreach (var item in step02.GetComponentsInChildren<UISprite>())
        {
            item.ResetAndUpdateAnchors();
        }
        UITweener tp =
        TweenPosition.Begin(mouse_Pointer, 0.5f, v2_fx.position, true);
        tp.AddOnFinished(() => {
            mouse_R.SetActive(true);
            TweenScale.Begin(mouse_R, 0.5f, Vector3.one).from = Vector3.one * 0.5f;
        });
        tp.AddOnFinished(() =>
        {
            mouse_rightClick.SetActive(true);
        });
    }
    void OnMouseRightClick(GameObject gameObject)
    {
        LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x + (panel_Timeline.position.x - mouse_R.transform.position.x));
        rightClickMenu.SetActive(true);
        v2_fx.GetComponent<UISprite>().spriteName = "fx_yellow_box";
    }
    void OnOpacityClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        opacity.spriteName = "fx_box2_click";
        step02.transform.Find("MouseUI/Menu_Opacity").gameObject.SetActive(true);
    }
    void OnStep03(GameObject gameObject)
    {
        step = Step.step03;
        step02.transform.Find("MouseUI").gameObject.SetActive(false);
        step03.SetActive(true);
        foreach (var item in step03.GetComponentsInChildren<UISprite>())
        {
            item.ResetAndUpdateAnchors();
        }
        UIEventListener.Get(LM_Manager.ins.guide_Touch).Clear();
        LM_Manager.ins.panel_Pause.SetActive(true);
        LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x + (mouse_R.transform.position.x - panel_Timeline.position.x));
        //dragItem = keyframe_Blue;
    }
}
