﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Text.RegularExpressions;

public class csvReader : MonoBehaviour
{
    public List<Dictionary<string, string>> Read(string file) {
        var list = new List<Dictionary<string, string>>();
#if (UNITY_EDITOR || UNITY_STANDALONE)
        StreamReader sr = new StreamReader(Application.dataPath + "/StreamingAssets/" + file);

        bool endOfFile = false;
        while (!endOfFile) {
            string data_String = sr.ReadLine();
            if (data_String == null) {
                endOfFile = true;
                break;
            }
            var data_values = data_String.Split(','); //string, string타입
            var tmp = new Dictionary<string, string>();
            tmp.Add(data_values[0], data_values[1]); //int, string으로 바뀜
            list.Add(tmp);
        }


#else
        string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
        string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
        char[] TRIM_CHARS = { '\"' };
        TextAsset data = Resources.Load(file) as TextAsset;

        var lines = Regex.Split(data.text, LINE_SPLIT_RE);

        if (lines.Length <= 1) return list;

        var header = Regex.Split(lines[0], SPLIT_RE);
        for (var i = 1; i < lines.Length; i++) {

            var values = Regex.Split(lines[i], SPLIT_RE);
            if (values.Length == 0 || values[0] == "") continue;

            var entry = new Dictionary<string, string>();

            entry.Add(values[0], values[1]); //int, string으로 바뀜

            list.Add(entry);
        }
       
#endif



        return list;
    }
}

