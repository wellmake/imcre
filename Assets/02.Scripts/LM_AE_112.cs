﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_112 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02,
        step03
    }
    public Transform container;
    public Transform RootUI;

	[Header("Step00_Guide")]
	public UISprite step00_objPrj;
	public UISprite step00_objMov;

	[Header("Step01")]
    public GameObject step01;
	public UISprite step01_objPrj;
	public UISprite step01_objMov;
	public GameObject step01_movBox;
    public GameObject step01_pointer1;
    public UISprite step01_movMenu;
    public UISprite step01_movMenuOn;
    public GameObject step01_movMenuBox;
    public Guide_PointerAnimation step01_pointer2;

	[Header("Step02")]
    public GameObject step02;
    public UISprite step02_tool;
    public UISprite step02_comp1, step02_comp2, step02_comp3;
    public UISprite step02_obPrj;
    public UISprite step02_objMov1;
    public UISprite step02_objMov2;
    public UISprite step02_timeLine1, step02_timeLine2, step02_timeLine3;
    public UISlider step02_timeLine_Line;
    public GameObject step02_slierThumb;
    public GameObject step02_box1, step02_box2, step02_box3;
    public GameObject step02_box2_dragPointer;
    public GameObject step02_keyboardGroup;
    public GameObject step02_key_alt, step02_key_RightSquareBracket;
    public GameObject step02_keyPointer1, step02_keyPointer2;

    public TweenAlpha delay0;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;

    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if(LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
           // guideBlue_Window.SetActive(true);
        }
        else if(LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
          //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_112_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        //UIEventListener.Get(LM_AE_Manager.ins.guide_Touch).onClick = OnStep01;
        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_movMenuOn.topAnchor.absolute = -66;
            step01_movMenu.GetComponent<UISprite>().aspectRatio = (float)step01_movMenu.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_movMenu.GetComponent<UISprite>().GetAtlasSprite().height;
            step01_movMenuOn.GetComponent<UISprite>().aspectRatio = (float)step01_movMenuOn.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_movMenuOn.GetComponent<UISprite>().GetAtlasSprite().height;
            //file_SaveAs1.GetComponent<UISprite>().aspectRatio = (float)file_SaveAs1.GetComponent<UISprite>().GetAtlasSprite().width / (float)file_SaveAs1.GetComponent<UISprite>().GetAtlasSprite().height;
            //file_SaveAs2.GetComponent<UISprite>().aspectRatio = (float)file_SaveAs2.GetComponent<UISprite>().GetAtlasSprite().width / (float)file_SaveAs2.GetComponent<UISprite>().GetAtlasSprite().height;
            //file_SaveAs3.GetComponent<UISprite>().aspectRatio = (float)file_SaveAs3.GetComponent<UISprite>().GetAtlasSprite().width / (float)file_SaveAs3.GetComponent<UISprite>().GetAtlasSprite().height;
        }
        else
        {
        }
    }

    bool dragFlag = false;
    private void Update()
    {
        if(dragFlag)
        {
            
        }
        
    }


    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step00_objPrj);
        lmSpriteList.Add(step00_objMov);
        lmSpriteList.Add(step01_objPrj);
        lmSpriteList.Add(step01_objMov);
        lmSpriteList.Add(step01_movMenu);
        lmSpriteList.Add(step01_movMenuOn);

        lmSpriteList.Add(step02_tool);
        lmSpriteList.Add(step02_comp1);
        lmSpriteList.Add(step02_comp2);
        lmSpriteList.Add(step02_comp3);
        lmSpriteList.Add(step02_obPrj);
        lmSpriteList.Add(step02_objMov1);
        lmSpriteList.Add(step02_objMov2);
        lmSpriteList.Add(step02_timeLine1);
        lmSpriteList.Add(step02_timeLine2);
        lmSpriteList.Add(step02_timeLine3);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step00_objPrj.spriteName = "AE_112_Project_1";
        step00_objMov.spriteName = "AE_112_mov_1";
        step01_objPrj.spriteName = "AE_112_Project_1";
        step01_objMov.spriteName = "AE_112_mov_1";
        step01_movMenu.spriteName = "AE_112_pop_1";
        step01_movMenuOn.spriteName = "AE_112_pop_2";

        step02_tool.spriteName = "AE_112_Tool";
        step02_comp1.spriteName = "AE_112_Comp_1";
        step02_comp2.spriteName = "AE_112_Comp_2";
        step02_comp3.spriteName = "AE_112_Comp_3";
        step02_obPrj.spriteName = "AE_112_Project_2";
        step02_objMov1.spriteName = "AE_112_mov_3";
        step02_objMov2.spriteName = "AE_112_mov_2";
        step02_timeLine1.spriteName = "AE_112_Timeline_1";
        step02_timeLine2.spriteName = "AE_112_Timeline_2";
        step02_timeLine3.spriteName = "AE_112_Timeline_3";

        //text1.SetDimensions((int)((float)text1.GetSprite("AE_111_text_1").width * 1.91f), (int)((float)text1.GetSprite("AE_111_text_1").height * 1.91f));
        //text2.SetDimensions((int)((float)text2.GetSprite("AE_111_text_2").width * 1.91f), (int)((float)text2.GetSprite("AE_111_text_2").height * 1.91f));
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //file_menu.rightAnchor.absolute = 79;
        }
    }

    public void OnStep01(GameObject gameobject)
    {

        start_guide.SetActive(false);

		step00_objPrj.gameObject.SetActive(false);
		step00_objMov.gameObject.SetActive(false);

        step01.SetActive(true);

        step01_movBox.SetActive(true);
        step01_pointer1.SetActive(true);
        
        UIEventListener.Get(step01_movBox).onClick = new UIEventListener.VoidDelegate(delegate
        {
            step01_movBox.SetActive(false);
            step01_pointer1.SetActive(false);

            step01_movMenu.gameObject.GetComponent<TweenAlpha>().onFinished.Clear();
            step01_movMenu.gameObject.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
            {
                step01_movMenuOn.GetComponent<TweenAlpha>().onFinished.Clear();
                step01_movMenuOn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {

                    UIEventListener.Get(step01_movMenuBox).onClick = new UIEventListener.VoidDelegate(delegate {
                        step01_movMenu.gameObject.SetActive(false);
                        step01_movMenuBox.SetActive(false);
                        step01.GetComponent<TweenPosition>().onFinished.Clear();
                        step01.GetComponent<TweenPosition>().onFinished.Add(new EventDelegate(delegate
                        {
                            OnStep02(null);
                        }));
                        step01.GetComponent<TweenPosition>().Play();
                    });

                    step01_movMenuBox.SetActive(true);
                    step01_pointer2.b_AnimStart = true;
                }));
                step01_movMenuOn.gameObject.SetActive(true);


            }));
            step01_movMenu.gameObject.SetActive(true);

        });
    }

    public void OnStep02(GameObject gameobject)
    {
        step01.SetActive(false);
        step02.SetActive(true);

        CameraManager.ins.MoveCamera(step02_keyboardGroup.transform.position, 0.5f, false);

        step02_box1.SetActive(true);
        step02_box2.SetActive(true);
        step02_box2_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
        step02_box1.GetComponent<TweenAlpha>().onFinished.Clear();
        step02_box1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
        {
            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_112_1", "MID");

            step02_timeLine_Line.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                step02_timeLine_Line.transform.Find("Thumb").GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                CameraManager.SCREEN_MOVE_PAUSE = true;
            }));
            step02_timeLine_Line.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = false;
            }));
            step02_timeLine_Line.onChange.Clear();
            step02_timeLine_Line.onChange.Add(new EventDelegate(delegate {

                if(step02_timeLine_Line.value == 1f)
                {
                    delay0.gameObject.SetActive(true);
                    delay0.onFinished.Clear();
                    delay0.onFinished.Add(new EventDelegate(delegate {
                        step02_slierThumb.GetComponent<BoxCollider>().enabled = false;

                        step02_timeLine_Line.transform.localPosition = step02_box2.transform.localPosition;

                        step02_box1.SetActive(false);
                        step02_box2.SetActive(false);

                        step02_keyboardGroup.SetActive(true);
                        step02_key_alt.GetComponent<TweenScale>().enabled = true;

                        LM_AE_Manager.ins.Off_Middle_Guide();
                        UIEventListener.Get(step02_key_alt).onClick = new UIEventListener.VoidDelegate(delegate
                        {
                            step02_key_alt.GetComponent<TweenScale>().enabled = false;
                            step02_key_alt.GetComponent<TweenScale>().ResetToBeginning();

                            step02_key_RightSquareBracket.GetComponent<TweenScale>().enabled = true;

                            //step02_keyPointer1.SetActive(false);
                            //step02_keyPointer2.SetActive(true);

                            UIEventListener.Get(step02_key_RightSquareBracket).onClick = new UIEventListener.VoidDelegate(delegate
                            {
                                step02_key_RightSquareBracket.GetComponent<TweenScale>().enabled = false;
                                step02_key_RightSquareBracket.GetComponent<TweenScale>().ResetToBeginning();

                                step02_keyboardGroup.SetActive(false);
                                step02_box3.SetActive(true);
                                step02_timeLine3.gameObject.SetActive(true);
                                step02_box3.GetComponent<TweenAlpha>().onFinished.Clear();
                                step02_box3.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                                {
                                    CameraManager.ins.MoveCamera(step02_keyboardGroup.transform.position, 0.5f, false);
                                    StartCoroutine(EndDelay());
                                }));

                            });
                        });
                    }));
                    
                }
            }));

            
        }));
        
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_112_2");

        //EndGuide.SetActive(true);

    }
}
