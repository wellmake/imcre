﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_261 : MonoBehaviour
{
    Camera cam;
    float offsetX;
    Ray ray;
    RaycastHit hit;
    int percentage = 100;
    float curMouseX;
    public int dragOffset = 5;
    List<UISprite> lmSpriteList;

    [Header("Step01")]
    public UISprite monitor_Planet;
    public UISprite key_left;
    public UISprite key_center;
    public UISprite key_right;
    public GameObject edit_Line_half;
    public GameObject boxb_Scale;
    public GameObject keyframe_01;
    public GameObject toggle_Anim;
    public GameObject boxb_FrameSelect;
    public GameObject edit_Line;
    public GameObject boxb_KeyCenter;
    public GameObject boxb_Percent;
    public GameObject touch_Percent;
    public GameObject keyframe_02;

    [Header("Drag var")]
    public GameObject drag_Container;
    public UILabel dragItem;
    bool dragOn;
    [SerializeField]
    bool dragStart;

    [Header("Step02")]
    public GameObject boxb_Frame0;
    public GameObject boxb_Play;
    public UISprite playButton;
    void Start()
    {
        cam = Camera.main;
        OnStageStart();
        SetSprites();
    }
    void SetSprites()
    {
        lmSpriteList = new List<UISprite>();

        lmSpriteList.Add(monitor_Planet);
        lmSpriteList.Add(key_left);
        lmSpriteList.Add(key_center);
        lmSpriteList.Add(key_right);
        lmSpriteList.Add(edit_Line_half.GetComponent<UISprite>());
        lmSpriteList.Add(keyframe_01.GetComponent<UISprite>());
        lmSpriteList.Add(toggle_Anim.GetComponent<UISprite>());
        lmSpriteList.Add(edit_Line.GetComponent<UISprite>());
        lmSpriteList.Add(keyframe_02.GetComponent<UISprite>());
        lmSpriteList.Add(playButton);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }

    void Update()
    {
        if (dragStart)
            SetPercentage();
    }
    void OnStageStart()
    {
        LM_Manager.ins.panel_Pause.SetActive(true);
        offsetX = cam.ScreenToWorldPoint(new Vector3(0, 0, 10)).x - LM_Manager.ins.panel_Monitor.transform.position.x;
        LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x + offsetX);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = MoveBackToPanelSource;
    }
    void MoveBackToPanelSource(GameObject gameObject)
    {
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) => 
        {
            LM_Manager.ins.BoxTweenScale(boxb_Scale);
            UIEventListener.Get(boxb_Scale).onClick = OnBoxbScaleClick;
        };
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x - offsetX);
        tp.AddOnFinished(() =>
        {
            LM_Manager.ins.panel_Pause.SetActive(true);
        });
    }
    void OnBoxbScaleClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        keyframe_01.SetActive(true);
        toggle_Anim.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_FrameSelect);
            UIEventListener.Get(boxb_FrameSelect).onClick = OnBoxbFrameSelectClick;
        };
        LM_Manager.ins.panel_Pause.SetActive(true);
    }
    void OnBoxbFrameSelectClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        edit_Line.SetActive(true);
        edit_Line_half.SetActive(false);
        LM_Manager.ins.BoxTweenScale(boxb_KeyCenter);
        UIEventListener.Get(boxb_KeyCenter).onClick = OnBoxbKeyCenterClick;
    }
    void OnBoxbKeyCenterClick(GameObject gameObject)
    {
        key_center.spriteName = "add_remove_keyframe";
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        touch_Percent.SetActive(true);
        keyframe_02.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Percent);
        UIEventListener.Get(boxb_Percent).onClick = OnBoxbPercentClick;
    }
    void OnBoxbPercentClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        touch_Percent.SetActive(false);
        drag_Container.SetActive(true);
        dragStart = true;
    }
    void SetPercentage()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "DragStart")
            {
                dragOn = true;
                StartCoroutine("Co_Counting");
            }
        }
        if (dragOn && Input.GetMouseButtonUp(0))
        {
            StopCoroutine("Co_Counting");
            dragOn = false;
        }
    }
    IEnumerator Co_Counting()
    {
        while (percentage < 200)
        {
            curMouseX = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f)).x;
            percentage += (int)((curMouseX - hit.point.x) * dragOffset);
            percentage = Mathf.Clamp(percentage, 0, 200);
            dragItem.text = string.Format("{0}%", percentage);

            yield return new WaitForSeconds(0.05f);
        }
        LM_Manager.ins.panel_Pause.SetActive(true);
        drag_Container.transform.Find("Sprite_Drag").gameObject.SetActive(false);
        drag_Container.transform.Find("Sprite_Arrow").gameObject.SetActive(false);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_Frame0);
        };
        dragStart = false;
        UIEventListener.Get(boxb_Frame0).onClick = OnBoxbFrame0_Click;
    }
    void OnBoxbFrame0_Click(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        edit_Line.SetActive(false);
        TweenScale ts = LM_Manager.ins.BoxTweenScale(edit_Line_half);
        ts.AddOnFinished(() =>
        {
            playButton.gameObject.SetActive(true);
            LM_Manager.ins.BoxTweenScale(boxb_Play);
            float newX = LM_Manager.ins.container.position.x +
                (LM_Manager.ins.panel_Source.transform.position.x - LM_Manager.ins.panel_Monitor.transform.position.x);
            LM_Manager.ins.MoveContainerHorizontally(newX);
        });
        UIEventListener.Get(boxb_Play).onClick = OnBoxbPlayClick;
    }
    void OnBoxbPlayClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        TweenScale ts = TweenScale.Begin(monitor_Planet.gameObject, 1f, Vector3.one * 2);
        ts.AddOnFinished(() =>
        {
            LM_Manager.ins.panel_Pause.SetActive(true);
        });
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
    }
}
