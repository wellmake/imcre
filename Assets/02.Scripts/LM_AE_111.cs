﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_111 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02,
        step03
    }
    public Transform container;
    public Transform RootUI;
    [Header("Step01")]
    public GameObject step01;

    [Header("Step02")]
    public GameObject step02;
    public UISprite file_menu;
    public UISprite file_menubar;
    public UISprite file_SaveAs1;
    public UISprite file_SaveAs2;
    public UISprite file_SaveAs3;

    public Guide_PointerAnimation Step02_GuidePointer_Anim;
    [Header("Step03")]
    public GameObject step03;
    public UISprite SavePop;
    public UISprite text1;
    public UISprite text2;
    public GameObject Label_text;
    public Guide_PointerAnimation Step03_GuidePointer_Anim;
    [Header("Step04")]
    public GameObject step04;
    public UISprite savebutton;
    public Guide_PointerAnimation Step04_GuidePointer_Anim;
    [Header("Step05")]
    public GameObject step05;
    public UISprite folder_pop;
    public Guide_PointerAnimation Step05_GuidePointer_Anim;
    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;

    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;


        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if(LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
           // guideBlue_Window.SetActive(true);
        }
        else if(LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
          //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_111_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        //UIEventListener.Get(LM_AE_Manager.ins.guide_Touch).onClick = OnStep01;
        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));
    }
    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(file_menu);
        lmSpriteList.Add(file_menubar);
        lmSpriteList.Add(file_SaveAs1);
        lmSpriteList.Add(file_SaveAs2);
        lmSpriteList.Add(file_SaveAs3);

        lmSpriteList.Add(SavePop);
        lmSpriteList.Add(text1);
        lmSpriteList.Add(text2);
        lmSpriteList.Add(folder_pop);
        lmSpriteList.Add(savebutton);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        file_menu.spriteName = "AE_111_File";
        file_menubar.spriteName = "AE_File_2";
        file_SaveAs1.spriteName = "AE_111_Save as_1";
        file_SaveAs2.spriteName = "AE_111_Save as_2";
        file_SaveAs3.spriteName = "AE_111_Save as_3";

        SavePop.spriteName = "AE_111_Save as_pop";
        text1.spriteName = "AE_111_text_1";
        text2.spriteName = "AE_111_text_2";
        folder_pop.spriteName = "AE_111_Folder";
        savebutton.spriteName = "AE_111_Save";

        //text1.SetDimensions((int)((float)text1.GetSprite("AE_111_text_1").width * 1.91f), (int)((float)text1.GetSprite("AE_111_text_1").height * 1.91f));
        //text2.SetDimensions((int)((float)text2.GetSprite("AE_111_text_2").width * 1.91f), (int)((float)text2.GetSprite("AE_111_text_2").height * 1.91f));

        if(LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            file_menu.rightAnchor.absolute = 79;
            step01.GetComponent<UISprite>().rightAnchor.absolute = 85;
            file_SaveAs1.GetComponent<UISprite>().topAnchor.absolute = -356;
            file_SaveAs1.GetComponent<UISprite>().leftAnchor.absolute = 4;
            file_SaveAs1.GetComponent<UISprite>().rightAnchor.absolute = 583;
            file_SaveAs3.GetComponent<UISprite>().leftAnchor.absolute = 587;
            file_SaveAs3.GetComponent<UISprite>().rightAnchor.absolute = -575;
            file_menubar.GetComponent<UISprite>().aspectRatio = (float)file_menubar.GetComponent<UISprite>().GetAtlasSprite().width / (float)file_menubar.GetComponent<UISprite>().GetAtlasSprite().height;
            file_SaveAs1.GetComponent<UISprite>().aspectRatio = (float)file_SaveAs1.GetComponent<UISprite>().GetAtlasSprite().width / (float)file_SaveAs1.GetComponent<UISprite>().GetAtlasSprite().height;
            file_SaveAs2.GetComponent<UISprite>().aspectRatio = (float)file_SaveAs2.GetComponent<UISprite>().GetAtlasSprite().width / (float)file_SaveAs2.GetComponent<UISprite>().GetAtlasSprite().height;
            file_SaveAs3.GetComponent<UISprite>().aspectRatio = (float)file_SaveAs3.GetComponent<UISprite>().GetAtlasSprite().width / (float)file_SaveAs3.GetComponent<UISprite>().GetAtlasSprite().height;
            text1.GetComponent<UISprite>().aspectRatio = (float)text1.GetComponent<UISprite>().GetAtlasSprite().width / (float)text1.GetComponent<UISprite>().GetAtlasSprite().height;
            text2.GetComponent<UISprite>().aspectRatio = (float)text2.GetComponent<UISprite>().GetAtlasSprite().width / (float)text2.GetComponent<UISprite>().GetAtlasSprite().height;
        }
        else
        { 
            step01.GetComponent<UISprite>().rightAnchor.absolute = 52;
            file_SaveAs1.GetComponent<UISprite>().topAnchor.absolute = -356;
        }
    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        step01.SetActive(true);
        UIEventListener.Get(step01).onClick = OnStep02;
    }

    public void OnStep02(GameObject gameobject)
    {
        step01.SetActive(false);
        step02.SetActive(true);

        UIEventListener.Get(file_SaveAs1.gameObject).onClick = OnStep03;
    }
    public void Step02_ColorAlphaFinish()
    {
        Step02_GuidePointer_Anim.b_AnimStart= true;
    }

    public void OnStep03(GameObject obj)
    {
        file_SaveAs1.gameObject.SetActive(false);
        step03.SetActive(true);
        UIEventListener.Get(file_SaveAs2.gameObject).onClick = OnStep04;
    }
    public void Step03_ColorAlphaFinish()
    {
        Step03_GuidePointer_Anim.b_AnimStart = true;
    }
    public void OnStep04(GameObject obj)
    {
        CameraManager.ins.MoveCamera(SavePop.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
            step03.SetActive(false);
            step02.SetActive(false);
            step04.SetActive(true);
        }));
        
        //LM_AE_Manager.ins.Local_MoveContainerHorizontally(-1076, 0.5f);
    }
    public void OnStep06(GameObject obj)
    {
        step04.SetActive(false);
        savebutton.gameObject.SetActive(false); 
        step05.SetActive(true);
    }
    public void SaveButtonColorFinish()
    {
        
     Step05_GuidePointer_Anim.b_AnimStart = true;
       
    }
    public void OnText2Finish()
    {
        text2.GetComponent<BoxCollider>().enabled = true;
        //MiddleGuide.gameObject.SetActive(true);
        Step04_GuidePointer_Anim.b_AnimStart = true;
        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_111_1", "BOTTOM");
        UIEventListener.Get(text2.gameObject).onClick = OnStep05;
    }
    public void OnStep05(GameObject obj)
    {
        text1.gameObject.SetActive(false);
        Label_text.gameObject.SetActive(true);
        LM_AE_Manager.ins.Off_Middle_Guide();
        //MiddleGuide.gameObject.SetActive(false);
    }
    public void LabelTextFinish()
    {
        savebutton.gameObject.SetActive(true);
        UIEventListener.Get(savebutton.gameObject).onClick = OnStep06;
    }

    public void FolderPopFinish()
    {
        StartCoroutine(EndDelay());
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_111_2");

        //EndGuide.SetActive(true);

    }
}
