﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_113 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02,
        step03
    }
    public Transform container;
    public Transform RootUI;

	[Header("Step00_Guide")]
    public UISprite step00_EffectsPresets1;
    public UISprite step00_EffectsPresets1_1;
    public UISprite step00_EffectsPresets1_2;
    public UISlider step00_slider1;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_Box1;

	[Header("Step02")]
    public GameObject step02;
    public GameObject step02_panel2;
    public UISprite step02_EffectsPresets2;
    public UISprite step02_EffectsPresets3;
    public GameObject step02_Box1;
    public GameObject step02_Box2;
    public GameObject step02_Box2_dragPointer;
    public GameObject step02_dragEnd;

    [Header("Step03")]
    public GameObject step03;
    public UISprite step03_Timeline2;
    public UISprite step03_EffectsControls1;
    public GameObject step03_Slider;
    public UILabel step03_label;
    public UISprite step03_FinComp;
    public GameObject step03_Box3;
    public GameObject step03_Box4;
    public GameObject step03_Box4_dragPointer;

    public TweenAlpha delay0;
    public TweenAlpha delay1;

    List<UISprite> lmSpriteList;

    public TweenAlpha step01_delay0;

    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if(LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
           // guideBlue_Window.SetActive(true);
        }
        else if(LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
          //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_113_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        //UIEventListener.Get(LM_AE_Manager.ins.guide_Touch).onClick = OnStep01;
        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_Box1.GetComponent<UISprite>().topAnchor.absolute = 135;
            step01_Box1.GetComponent<UISprite>().bottomAnchor.absolute = 82;
            step00_EffectsPresets1_2.GetComponent<UISprite>().topAnchor.absolute = -255;
            step00_EffectsPresets1_2.GetComponent<UISprite>().leftAnchor.absolute = -375;
            step00_EffectsPresets1_2.GetComponent<UISprite>().rightAnchor.absolute = -16;
            step02_EffectsPresets2.GetComponent<UISprite>().topAnchor.absolute = 138;
            step02_EffectsPresets2.GetComponent<UISprite>().leftAnchor.absolute = -3;
            step02_EffectsPresets2.GetComponent<UISprite>().rightAnchor.absolute = 3;
            step02_Box1.GetComponent<UISprite>().topAnchor.absolute = -219;
            step02_Box1.GetComponent<UISprite>().bottomAnchor.absolute = -272;
            step02_Box2.transform.Find("SpriteArrow").GetComponent<UISprite>().topAnchor.absolute = -45;
            step02_Box2.transform.Find("SpriteArrow").GetComponent<UISprite>().bottomAnchor.absolute = -240;
            step02_Box2.transform.Find("DragStartPoint").GetComponent<UISprite>().topAnchor.absolute = -282;
            step02_Box2.transform.Find("DragStartPoint").GetComponent<UISprite>().bottomAnchor.absolute = -382;
            step02_EffectsPresets3.topAnchor.absolute = -374;
            step03_label.topAnchor.absolute = -257;
            step03_Slider.GetComponent<UISprite>().topAnchor.absolute = -292;
            step03_Slider.transform.Find("Background").GetComponent<UISprite>().topAnchor.absolute = -327;
            step03_Slider.transform.Find("Background").GetComponent<UISprite>().leftAnchor.absolute = 102;
            step03_Slider.transform.Find("Background").GetComponent<UISprite>().rightAnchor.absolute = 500;
            step03_Slider.transform.Find("Foreground").GetComponent<UISprite>().topAnchor.absolute = -327;
            step03_Slider.transform.Find("Foreground").GetComponent<UISprite>().leftAnchor.absolute = 102;
            step03_Slider.transform.Find("Foreground").GetComponent<UISprite>().rightAnchor.absolute = 500;
            step03_Box3.GetComponent<UISprite>().leftAnchor.absolute = 314;
            step03_Box3.GetComponent<UISprite>().rightAnchor.absolute = 402;
            step03_Box4.GetComponent<UISprite>().leftAnchor.absolute = 25;
            step03_Box4.GetComponent<UISprite>().rightAnchor.absolute = 13;
            step00_EffectsPresets1_1.GetComponent<UISprite>().aspectRatio = (float)step00_EffectsPresets1_1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step00_EffectsPresets1_1.GetComponent<UISprite>().GetAtlasSprite().height;
            step02_EffectsPresets2.GetComponent<UISprite>().aspectRatio = (float)step02_EffectsPresets2.GetComponent<UISprite>().GetAtlasSprite().width / (float)step02_EffectsPresets2.GetComponent<UISprite>().GetAtlasSprite().height;
            step02_EffectsPresets3.GetComponent<UISprite>().aspectRatio = (float)step02_EffectsPresets3.GetComponent<UISprite>().GetAtlasSprite().width / (float)step02_EffectsPresets3.GetComponent<UISprite>().GetAtlasSprite().height;
            step03_EffectsControls1.GetComponent<UISprite>().aspectRatio = (float)step03_EffectsControls1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step03_EffectsControls1.GetComponent<UISprite>().GetAtlasSprite().height;
        }
    }

    bool dragFlag = false;
    bool delayFlag = false;
    private void Update()
    {
        if(dragFlag)
        {
            
        }

        if(delayFlag)
        {
            step00_slider1.value += Time.deltaTime;

        }
    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step00_EffectsPresets1);
        lmSpriteList.Add(step00_EffectsPresets1_1);
        lmSpriteList.Add(step00_EffectsPresets1_2);
        lmSpriteList.Add(step02_EffectsPresets2);
        lmSpriteList.Add(step02_EffectsPresets3);
        lmSpriteList.Add(step03_Timeline2);
        lmSpriteList.Add(step03_EffectsControls1);
        lmSpriteList.Add(step03_FinComp);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step00_EffectsPresets1.spriteName = "AE_113_Effects_Presets_1";
        step00_EffectsPresets1_1.spriteName = "AE_113_Effects_Presets_1_1";
        step00_EffectsPresets1_2.spriteName = "AE_113_Effects_Presets_1_2";
        step02_EffectsPresets2.spriteName = "AE_113_Effects_Presets_2";
        step02_EffectsPresets3.spriteName = "AE_113_Effects_Presets_3";
        step03_Timeline2.spriteName = "AE_113_Timeline_2";
        step03_EffectsControls1.spriteName = "AE_113_Effects-Controls_1";
        step03_FinComp.spriteName = "AE_113_Fin-Comp";

    }

    public void OnStep01(GameObject gameobject)
    {
        CameraManager.ins.MoveCamera(true, false, 1f).onFinished.Add(new EventDelegate(delegate {
            start_guide.SetActive(false);

            step01.SetActive(true);

            UIEventListener.Get(step01_Box1).onClick = new UIEventListener.VoidDelegate(delegate
            {
                step01_Box1.SetActive(false);
                OnStep02();
            });
            step01_Box1.SetActive(true);

        }));


        
    }

    Vector3 firstEffectsPresets3_pos;
    public void OnStep02()
    {
        CameraManager.ins.MoveCamera(true, false, 0.5f);
        step02.SetActive(true);
        step02_EffectsPresets2.gameObject.SetActive(true);
        step02_EffectsPresets2.GetComponent<TweenAlpha>().onFinished.Clear();
        step02_EffectsPresets2.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {

            step02_panel2.GetComponent<UIScrollView>().enabled = false;
            step00_EffectsPresets1_1.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            step01_delay0.gameObject.SetActive(true);
            delayFlag = true;

            step00_slider1.onChange.Add(new EventDelegate(delegate {

                if(step00_slider1.value >= 1f)
                {
                    delayFlag = false;

                    LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_113_1", "TOP");

                    step02_EffectsPresets3.gameObject.SetActive(true);
                    firstEffectsPresets3_pos = step02_EffectsPresets3.transform.localPosition;

                    step02_Box1.SetActive(true);
                    step02_Box2.SetActive(true);
                    step02_Box2_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

                    step02_EffectsPresets3.GetComponent<UIEventTrigger>().onDragStart.Clear();
                    step02_EffectsPresets3.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate
                    {
                        CameraManager.SCREEN_MOVE_PAUSE = true;
                        step02_EffectsPresets3.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                        step02_EffectsPresets3.GetComponent<TweenAlpha>().enabled = false;
                        step02_EffectsPresets3.color = new Color(step02_EffectsPresets3.color.r, step02_EffectsPresets3.color.g, step02_EffectsPresets3.color.b, 255f / 255f);
                    }));
                    step02_EffectsPresets3.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                    step02_EffectsPresets3.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
                    {
                        CameraManager.SCREEN_MOVE_PAUSE = false;
                        if (step02_EffectsPresets3.transform.localPosition.y <= step02_dragEnd.transform.localPosition.y + 20f && step02_EffectsPresets3.transform.localPosition.y >= step02_dragEnd.transform.localPosition.y - 20f)
                        {
                            step02_EffectsPresets3.gameObject.SetActive(false);
                            LM_AE_Manager.ins.Off_Middle_Guide();
                            step02_Box1.SetActive(false);
                            step02_Box2.SetActive(false);
                            OnStep03();
                        }
                        else
                        {
                            step02_EffectsPresets3.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                            step02_EffectsPresets3.GetComponent<TweenAlpha>().enabled = true;
                            step02_EffectsPresets3.transform.localPosition = firstEffectsPresets3_pos;
                        }
                    }));
                }

            }));
        }));
        step02_EffectsPresets2.gameObject.SetActive(true);
    }

    public void OnStep03()
    {
        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate
        {
            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_113_2", "MID");
            step03_Slider.transform.Find("Thumb").GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            step03_Box3.SetActive(true);
            step03_Box4.SetActive(true);
            step03_Box4_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
        }));

        step03.SetActive(true);
        step03_Slider.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate
        {
            CameraManager.SCREEN_MOVE_PAUSE = true;
        }));
        step03_Slider.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
        {
            CameraManager.SCREEN_MOVE_PAUSE = false;
        }));
        step03_Slider.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate
        {
            CameraManager.SCREEN_MOVE_PAUSE = true;
        }));
        step03_Slider.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
        {
            CameraManager.SCREEN_MOVE_PAUSE = false;
        }));

        
        step03_Slider.GetComponent<UISlider>().onChange.Clear();
        step03_Slider.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate
        {
            step03_label.text = ((int)(step03_Slider.GetComponent<UISlider>().value * 32f)).ToString();

            if(((int)(step03_Slider.GetComponent<UISlider>().value * 32f)) <= 2)
            {
                step03_Box3.SetActive(false);
                step03_Box4.SetActive(false);

                step03_Slider.GetComponent<UISlider>().onChange.Clear();
                OnStep04();
            }

        }));
    }

    public void OnStep04()
    {
        step03_FinComp.GetComponent<TweenAlpha>().enabled = true;

        delay0.gameObject.SetActive(true);
        delay0.onFinished.Add(new EventDelegate(delegate
        {
            LM_AE_Manager.ins.Off_Middle_Guide();
            CameraManager.ins.MoveCamera(step03_FinComp.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
            {
                delay1.gameObject.SetActive(true);
                delay1.onFinished.Add(new EventDelegate(delegate
                {
                    StartCoroutine(EndDelay());
                }));
            }));
        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_113_3");
    }
}
