﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System;

namespace CallerManager
{


    public class Caller{

        private static Caller m_Instance;
        public static Caller GetInst
        {
            get
            {
                if(m_Instance ==null)
                {
                    m_Instance = new Caller();
                }
                return m_Instance;
            }
        }

        //생성자
        Caller()
        {
            LoadAll();
        }

        private StringBuilder m_StrBuilder = new StringBuilder();
        private XmlDocument m_XmlDoc = new XmlDocument();
        private Dictionary<string, string> m_ResDictInfos = new Dictionary<string, string>();
        private Dictionary<string, GameObject> m_DictGameObjects = new Dictionary<string, GameObject>();
        private Dictionary<string, List<GameObject>> m_DictPool = new Dictionary<string, List<GameObject>>();
        private List<GameObject> m_ListDeleteGroup = new List<GameObject>();


       

        private void LoadAll()
        {
            const string m_FileName1 = "Game_Info";

            LoadXML(m_FileName1, "GameGroup/GameInfo", new string[2] { "Alias", "Path" });
        }

        private void LoadXML(string strFileName, string strNodePathName, string[] strArrElementNames, string strXMLPath = "XML/")
        {
            m_StrBuilder.Length = 0;
            m_StrBuilder.Append(strXMLPath);
            m_StrBuilder.Append(strFileName);

            {
                TextAsset textAsset = Resources.Load<TextAsset>(m_StrBuilder.ToString());
                m_XmlDoc.LoadXml(textAsset.text);
            }

            {
                XmlNodeList MyNodes = m_XmlDoc.SelectNodes(strNodePathName);

                string element_1 = string.Empty;
                string element_2 = string.Empty;

                foreach (XmlNode node in MyNodes)
                {
                    element_1 = node.SelectSingleNode(strArrElementNames[0]).InnerText;
                    element_2 = node.SelectSingleNode(strArrElementNames[1]).InnerText;

                    if (!m_ResDictInfos.ContainsKey(element_1))
                        m_ResDictInfos.Add(element_1, element_2);
                }
            }
            //---
        }

        private GameObject LoadPrefab(string strAlias, Vector3 vPos, Quaternion quat)
        {
            GameObject ResultObject = null;

            if (m_ResDictInfos.ContainsKey(strAlias))
            {
                if (m_DictGameObjects.ContainsKey(strAlias))
                {
                    ResultObject = (GameObject.Instantiate(m_DictGameObjects[strAlias], vPos, quat) as GameObject);
                }
                else
                {
                    //---
                    {
                        GameObject MyGameObject = Resources.Load<GameObject>(m_ResDictInfos[strAlias]);

                        if (null != MyGameObject)
                        {
                            m_DictGameObjects.Add(strAlias, MyGameObject);
                            ResultObject = (GameObject.Instantiate(MyGameObject, vPos, quat) as GameObject);
                        }
                    }
                    Resources.UnloadUnusedAssets();
                }
            }

            if(ResultObject != null)
            {
                ResultObject.gameObject.SetActive(false);
                ResultObject.gameObject.SetActive(true);

                ParticleSystem resParticleSys = ResultObject.GetComponent<ParticleSystem>();
                if(resParticleSys != null && !resParticleSys.loop)
                {
                    ResultObject.AddComponent<ParticleTimer>();
                    ResultObject.GetComponent<ParticleTimer>().SetParticle(resParticleSys);
                    resParticleSys.playOnAwake = true;
                }
            }
            return ResultObject;
        }


        public AudioClip LoadAudioClip(string strAlias)
        {
            AudioClip resultObject = null;

            if (m_ResDictInfos.ContainsKey(strAlias))
            {
                resultObject = Resources.Load(m_ResDictInfos[strAlias]) as AudioClip;
            }

            return resultObject;
        }

        public GameObject GetResByPool(string strAlias, Vector3 vPos, Quaternion quat)
        {
            if (!m_ResDictInfos.ContainsKey(strAlias))
                return null;

            GameObject ResultObject = null;

            if (m_DictPool.ContainsKey(strAlias))
            {
                GameObject PoolElement = null;
                foreach (GameObject element in m_DictPool[strAlias])
                {
                    if (null == element)
                    {
                        m_ListDeleteGroup.Add(element);
                        continue;
                    }
                    if (!element.activeSelf)
                    {
                        PoolElement = element;
                        break;
                    }
                }

                foreach (GameObject obj in m_ListDeleteGroup)
                {
                    m_DictPool[strAlias].Remove(obj);
                }
                m_ListDeleteGroup.Clear();

                if (null != PoolElement)
                {
                    if (PoolElement.activeSelf)
                        PoolElement.SetActive(false);

                    PoolElement.transform.position = vPos;
                    PoolElement.transform.rotation = quat;
                    PoolElement.SetActive(true);
                    ResultObject = PoolElement;
                }
                else
                {
                    GameObject element = this.LoadPrefab(strAlias, vPos, quat);
                    if (null != element)
                        m_DictPool[strAlias].Add(element);

                    ResultObject = element;
                }
            }
            else
            {
                List<GameObject> listObjects = new List<GameObject>();
                m_DictPool.Add(strAlias, listObjects);

                GameObject element = this.LoadPrefab(strAlias, vPos, quat);
                if (null != element)
                    listObjects.Add(element);

                ResultObject = element;
            }

            if (!ResultObject.activeSelf)
                ResultObject.SetActive(true);

            return ResultObject;
        }

        public void Restore(GameObject param)
        {
            if (null != param)
                param.SetActive(false);
        }

    }
}