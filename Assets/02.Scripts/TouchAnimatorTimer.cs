﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchAnimatorTimer : MonoBehaviour
{
    private Animator m_anim;
    private AnimatorStateInfo animInfo;
    private void Awake()
    {
        m_anim = this.GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        animInfo = m_anim.GetCurrentAnimatorStateInfo(0);
        m_anim.SetBool("Click", false);
        if (animInfo.normalizedTime < 1.0f) { }
        else
            CallerManager.Caller.GetInst.Restore(this.gameObject);
    }

    private void OnEnable()
    {
        m_anim.SetBool("Click", true);
        m_anim.Play("TouchEffect");
    }
}
