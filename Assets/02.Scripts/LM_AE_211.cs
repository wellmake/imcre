﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_211 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public UISprite step01_spriteLakePreview;
    public UISprite step01_spriteFile;
    public UISprite step01_spritePumpkinTimeline;
    public GameObject step01_box1;
    public UISprite step01_spriteFile_lake;
    public UISprite step01_spriteFile_lake_dragable;
    public UISprite step01_spriteComp_choosing;
    public GameObject step01_box2;
    public GameObject step01_box3;
    public GameObject step01_box3_dragPointer;
    public GameObject dragEnd1;
    public UISprite step01_spriteLakeProjectBtn;
    public UISprite step01_spritePreview;
    public UISprite step01_spriteFile2;
    public UISprite step01_spriteFile_lake_comp;
    public UISprite step01_spriteLakeCompositionBtn;
    public UISprite step01_spriteLakeTimeline;
    public GameObject step01_glitters1;
    public GameObject step01_glitters2;

    public TweenAlpha delay0;
    public TweenAlpha delay1;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_211_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));


    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteLakePreview);
        lmSpriteList.Add(step01_spriteFile);
        lmSpriteList.Add(step01_spritePumpkinTimeline);
        lmSpriteList.Add(step01_spriteFile_lake);
        lmSpriteList.Add(step01_spriteFile_lake_dragable);
        lmSpriteList.Add(step01_spriteComp_choosing);
        lmSpriteList.Add(step01_spriteLakeProjectBtn);
        lmSpriteList.Add(step01_spritePreview);
        lmSpriteList.Add(step01_spriteFile2);
        lmSpriteList.Add(step01_spriteFile_lake_comp);
        lmSpriteList.Add(step01_spriteLakeCompositionBtn);
        lmSpriteList.Add(step01_spriteLakeTimeline);



        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteLakePreview.spriteName = "AE_211_Lake.mp4-Preview";
        step01_spriteFile.spriteName = "AE_AE_211_3-File";
        step01_spritePumpkinTimeline.spriteName = "AE_211_Pumpkin.MOV-Layer";
        step01_spriteFile_lake.spriteName = "AE_211_Lake.mp4";
        step01_spriteFile_lake_dragable.spriteName = "AE_211_Lake.mp4";
        step01_spriteComp_choosing.spriteName = "AE_211_Comp-choosing";
        step01_spriteLakeProjectBtn.spriteName = "AE_211_Lake-Project-Butten";
        step01_spritePreview.spriteName = "AE_211_Preview";
        step01_spriteFile2.spriteName = "AE_AE_211_3-File";
        step01_spriteFile_lake_comp.spriteName = "AE_211_Lake-Comp(Project)";
        step01_spriteLakeCompositionBtn.spriteName = "AE_211_Lake-Composition-Butten";
        step01_spriteLakeTimeline.spriteName = "AE_211_Lake.mp4";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);
        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_211_1", "MID");

        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            step01_box1.SetActive(false);
            LM_AE_Manager.ins.Off_Middle_Guide();
            step01_spriteFile_lake.gameObject.SetActive(true);
            step01_spriteFile_lake.GetComponent<TweenAlpha>().onFinished.Clear();
            step01_spriteFile_lake.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                step01_box2.SetActive(true);
                step01_box3.SetActive(true);
                step01_box3_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                step01_spriteComp_choosing.gameObject.SetActive(true);

                step01_spriteFile_lake_dragable.gameObject.SetActive(true);
                step01_spriteFile_lake_dragable.transform.localPosition = step01_spriteFile_lake.transform.localPosition;
                step01_spriteFile_lake_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                    CameraManager.SCREEN_MOVE_PAUSE = true;
                    step01_spriteFile_lake_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                }));
                step01_spriteFile_lake_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                step01_spriteFile_lake_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                    CameraManager.SCREEN_MOVE_PAUSE = false;

                    if (step01_spriteFile_lake_dragable.transform.position.y <= dragEnd1.transform.position.y + 0.1f && step01_spriteFile_lake_dragable.transform.position.y >= dragEnd1.transform.position.y - 0.1f)
                    {
                        step01_spriteFile.gameObject.SetActive(false);

                        step01_box2.SetActive(false);
                        step01_box3.SetActive(false);
                        step01_spriteComp_choosing.gameObject.SetActive(false);
                        step01_spriteFile_lake.gameObject.SetActive(false);
                        step01_spriteFile_lake_dragable.gameObject.SetActive(false);

                        step01_spriteFile2.gameObject.SetActive(true);
                        step01_spriteLakeProjectBtn.gameObject.SetActive(true);
                        step01_spritePreview.gameObject.SetActive(true);
                        step01_spriteFile_lake_comp.gameObject.SetActive(true);
                        step01_spriteLakeCompositionBtn.gameObject.SetActive(true);
                        step01_spriteLakeTimeline.gameObject.SetActive(true);
                        step01_glitters1.SetActive(true);
                        step01_glitters2.SetActive(true);

                        delay0.gameObject.SetActive(true);
                        delay0.GetComponent<TweenAlpha>().onFinished.Clear();
                        delay0.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                            CameraManager.ins.MoveCamera(step01_spritePreview.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                delay1.gameObject.SetActive(true);
                                delay1.GetComponent<TweenAlpha>().onFinished.Clear();
                                delay1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                    StartCoroutine(EndDelay());
                                }));
                            }));
                        }));
                    } else
                    {
                        step01_spriteFile_lake_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                        step01_spriteFile_lake_dragable.transform.localPosition = step01_spriteFile_lake.transform.localPosition;
                    }
                }));

            }));
        }));

    }


    public void OnStep02()
    {

    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_211_2");
    }
}
