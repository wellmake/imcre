﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_224 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step00_Guide")]
    public UISprite Step00_spriteIntro;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_mouse;
    public UISprite step01_spriteImportFile;
    public GameObject step01_box1;
    public UISprite step01_spriteImportFile_select;
    public GameObject step01_box2;
    public UISprite step01_spriteImportFile_check;
    public GameObject step01_box3;

    [Header("Step02")]
    public GameObject step02;
    public UISprite step02_spriteComposition2;
    public GameObject step02_alien;
    public UISprite step02_spriteProject1;
    public UISprite step02_spriteProject2;
    public UISprite step02_spriteComposition1;
    public UISprite step02_spriteComposition3;
    public UISprite step02_spriteTimeline;
    public GameObject step02_box4;
    public GameObject step02_box5;
    public GameObject step02_box5_dragPointer;
    public UISprite step02_spriteProject2_dragable;
    public UISprite step02_spriteProject3;

    public TweenAlpha step01_delay0;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_224_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_box2.GetComponent<UISprite>().topAnchor.absolute = -69;
            step01_box2.GetComponent<UISprite>().bottomAnchor.absolute = -125;
            step01_box2.GetComponent<UISprite>().leftAnchor.absolute = 206;
            step01_box2.GetComponent<UISprite>().rightAnchor.absolute = 260;

            step01_spriteImportFile_check.GetComponent<UISprite>().topAnchor.absolute = -86;
            step01_spriteImportFile_check.GetComponent<UISprite>().bottomAnchor.absolute = -106;
            step01_spriteImportFile_check.GetComponent<UISprite>().leftAnchor.absolute = 223;
            step01_spriteImportFile_check.GetComponent<UISprite>().rightAnchor.absolute = 245;

            step01_box3.GetComponent<UISprite>().leftAnchor.absolute = 121;
        }
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(Step00_spriteIntro);
        lmSpriteList.Add(step01_spriteImportFile);
        lmSpriteList.Add(step01_spriteImportFile_select);
        lmSpriteList.Add(step01_spriteImportFile_check);
        lmSpriteList.Add(step02_spriteComposition2);
        lmSpriteList.Add(step02_spriteProject1);
        lmSpriteList.Add(step02_spriteProject2);
        lmSpriteList.Add(step02_spriteComposition1);
        lmSpriteList.Add(step02_spriteComposition3);
        lmSpriteList.Add(step02_spriteTimeline);
        lmSpriteList.Add(step02_spriteProject2_dragable);
        lmSpriteList.Add(step02_spriteProject3);


        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        Step00_spriteIntro.spriteName = "AE_224_bg_2";
        step01_spriteImportFile.spriteName = "AE_224_import-File";
        step01_spriteImportFile_select.spriteName = "AE_224_import-File-Select";
        step01_spriteImportFile_check.spriteName = "AE_224_Import-File_Check";
        step02_spriteComposition2.spriteName = "AE_224_Composition_2";
        step02_spriteProject1.spriteName = "AE_224_Project_1";
        step02_spriteProject2.spriteName = "AE_224_Project_2";
        step02_spriteComposition1.spriteName = "AE_224_Composition_1";
        step02_spriteComposition3.spriteName = "AE_224_Composition_3";
        step02_spriteTimeline.spriteName = "AE_224_Timeline";
        step02_spriteProject2_dragable.spriteName = "AE_224_Project_2";
        step02_spriteProject3.spriteName = "AE_224_Project_3";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        step01.SetActive(true);
        step01_mouse.SetActive(true);
        step01_mouse.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_mouse.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            step01_mouse.SetActive(false);

            CameraManager.ins.MoveCamera(step01_spriteImportFile.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                step01_spriteImportFile.gameObject.SetActive(true);
                step01_spriteImportFile.GetComponent<TweenScale>().onFinished.Clear();
                step01_spriteImportFile.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                    step01_box1.SetActive(true);
                    step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
                    step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                        step01_box1.SetActive(false);

                        step01_spriteImportFile_select.gameObject.SetActive(true);
                        step01_box2.SetActive(true);
                        step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step01_box2.SetActive(false);

                            step01_spriteImportFile_check.gameObject.SetActive(true);
                            step01_box3.SetActive(true);
                            step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                Step00_spriteIntro.gameObject.SetActive(false);
                                step01.SetActive(false);

                                OnStep02();
                            }));
                        }));
                    }));
                }));
            }));
        }));
        
    }


    public void OnStep02()
    {
        step02.SetActive(true);

        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
            step02_box4.SetActive(true);
            step02_box5.SetActive(true);
            step02_box5_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
            step02_spriteProject2_dragable.gameObject.SetActive(true);
            step02_spriteProject2_dragable.transform.localPosition = step02_spriteProject2.transform.localPosition;

            step02_spriteProject2_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = true;
                step02_spriteProject2_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            }));
            step02_spriteProject2_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
            step02_spriteProject2_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = false;
                
                if(step02_spriteProject2_dragable.transform.position.y < step02_box5.transform.position.y + 0.1f && step02_spriteProject2_dragable.transform.position.y > step02_box5.transform.position.y - 0.1f)
                {
                    step02_box4.SetActive(false);
                    step02_box5.SetActive(false);
                    step02_spriteProject2_dragable.gameObject.SetActive(false);
                    step02_spriteProject3.gameObject.SetActive(true);

                    CameraManager.ins.MoveCamera(step02_spriteComposition2.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                        step02_alien.GetComponent<Spine.Unity.SkeletonAnimation>().enabled = true;

                        step01_delay0.gameObject.SetActive(true);
                        step01_delay0.onFinished.Clear();
                        step01_delay0.onFinished.Add(new EventDelegate(delegate {
                            step02_alien.GetComponent<Spine.Unity.SkeletonAnimation>().enabled = false;
                            StartCoroutine(EndDelay());
                        }));
                    }));

                } else
                {
                    step02_spriteProject2_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                    step02_spriteProject2_dragable.transform.localPosition = step02_spriteProject2.transform.localPosition;
                }
            }));
        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        //step02_alien.SetActive(false);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_224_1");
    }
}
