﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class LM_AE_Manager : MonoBehaviour
{
    public static LM_AE_Manager ins;
    public Transform container;
    public UILabel titleLabel;
    public ScreenManager scManager;
    public GameObject objUiTop;
    public GameObject objUiRoot;
    public List<GameObject> modes;
    Camera cam;
    Ray ray;
    RaycastHit hit;

    GameObject curLevel;
    UIWidget panel_Source_UIWidget;
    public Vector3 containerOriginPos;
    public Vector3 containerLocalOriginPos;
    [Header("Ref_ScreenWidth")]
    public UIWidget titlePanel;

    [Header("Sprites")]
    public UISprite BG;
    public NGUIAtlas lm_Atlas;
    List<UISprite> panel_Sprites;

    [Header("Guide Panels")]
    public GameObject start_guide;
    public GameObject Btn_Next;
    public GameObject Btn_Skip;
    public UILabel start_guide_Lable;

    public GameObject middle_guide;
    public UILabel middle_guide_Lable;

    public UISprite middle_ailen_point;

    public GameObject end_guide;
    public UILabel end_guide_Lable;

    [Header("Stage Setting")]
    public Language curLanguage;
    public Stage stage;
    public static Stage static_stage;
    public static Language static_curLanguage;
    public static bool isFirstLoad = true;

    [Header("Option")]
    public GameObject panel_Option;
    public UIToggle toggleLanguage;
    public UITable table;
    public GameObject item;
    static bool isOptionSelected = false;
    public UILabel selectedStage;

    [Header("Sounds")]
    public AudioSource audio;
    public AudioClip sounds_end, sounds_touch;
    public AudioClip[] sounds_guide;

    [Header("Etc")]
    public GameObject Guide_Touch_;
    public GameObject start_guide_;
    public GameObject Skip_;
    public GameObject MiddleGuide_;

    public csvReader csvreader;

    public List<Dictionary<string, string>> learningmode_guide_en;
    public List<Dictionary<string, string>> learningmode_guide_ko;
    public List<Dictionary<string, string>> learningmode_guide_vn;
    public List<Dictionary<string, string>> learningmode_title_en;
    public List<Dictionary<string, string>> learningmode_title_ko;
    public List<Dictionary<string, string>> learningmode_title_vn;

    public enum Language
    {
        en,
        kr,
        vn,

    }
    public enum Stage
    {
        AE_111,
        AE_112,
        AE_113,
        AE_114,
        AE_115,
        AE_211,
        AE_212,
        AE_213,
        AE_214,
        AE_221,
        AE_222,
        AE_223,
        AE_224,
        AE_225,
        AE_226,
        DR_111,
        DR_112,
        DR_113,
        DR_114,
        DR_115
    }

    private void Awake()
    {
        if (isOptionSelected)
        {
            stage = static_stage;
            curLanguage = static_curLanguage;
        }

        ins = this;


        AddPanels();

        GameObject go = Resources.Load("Prefabs_LM/Container_" + stage.ToString()) as GameObject;
        GameObject curStageContainer = NGUITools.AddChild(objUiRoot, go);
        curStageContainer.name = "Container";
        this.container = curStageContainer.transform;
        CameraManager.ins.objBG = this.container.transform.Find("BG").gameObject;
        this.BG = this.container.transform.Find("BG").GetComponent<UISprite>();
        this.container.transform.localPosition = new Vector3(0f, -60f, 0f);
        this.container.transform.Find("BG").GetComponent<UISprite>().SetAnchor(this.objUiTop.transform);
        this.container.transform.Find("BG").GetComponent<UISprite>().topAnchor.relative = 1;
        this.container.transform.Find("BG").GetComponent<UISprite>().topAnchor.absolute = -120;
        this.container.transform.Find("BG").GetComponent<UISprite>().bottomAnchor.relative = 0;
        this.container.transform.Find("BG").GetComponent<UISprite>().bottomAnchor.absolute = 0;
        this.container.transform.Find("BG").GetComponent<UISprite>().leftAnchor.relative = 0;
        this.container.transform.Find("BG").GetComponent<UISprite>().leftAnchor.absolute = 0;
        this.container.transform.Find("BG").GetComponent<UISprite>().rightAnchor.relative = 1;
        this.container.transform.Find("BG").GetComponent<UISprite>().rightAnchor.absolute = 2169;
        this.container.transform.Find("BG").GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;

        curLevel = container.Find(stage.ToString()).gameObject;

        /*
        curLevel = modes.Find(x => x.name.Contains(stage.ToString()));
        foreach (var stage in modes)
        {
            if (stage.Equals(curLevel))
                continue;
            stage.SetActive(false);
        }*/

        
        SetAtlas();
        Init_Sprites();
        scvRead();
        //start_guide_Lable.trueTypeFont = Resources.GetBuiltinResource(typeof(Font), "NotoSans-Bold.ttf") as Font;
        //panel_Source_UIWidget = panel_Source.GetComponent<UIWidget>();
        //Init_Stage();
    }
    private void Start()
    {
        //containerOriginPos = container.position;
        //containerLocalOriginPos = container.localPosition;
        //panel_Pause.SetActive(true);
        cam = Camera.main;
        this.audio = this.GetComponent<AudioSource>();
        audio.Play();

        GameObject[] goList = Resources.LoadAll<GameObject>("Prefabs_LM");
        for (int i = 0; i < goList.Length; i++)
        {
            GameObject instance = Instantiate(item, table.transform);
            instance.name = instance.GetComponentInChildren<UILabel>().text = goList[i].name.Replace("Container_", "");
            UIEventListener.Get(instance).onClick = Option_SetStage;
            UIEventListener.Get(instance).onClick += Option_OnStageSelect;
            instance.GetComponent<UIDragScrollView>().scrollView = table.GetComponent<UIScrollView>();
        }
        //UpdatePanelPosition();

        CameraManager.ins.InitCamera();

        Resources.UnloadUnusedAssets();
    }

    public void scvRead()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        learningmode_guide_en = csvreader.Read("learningmode_guide_en.csv");
        learningmode_guide_ko = csvreader.Read("learningmode_guide_ko.csv");
        learningmode_guide_vn = csvreader.Read("learningmode_guide_vn.csv");
        learningmode_title_en = csvreader.Read("learningmode_title_en.csv");
        learningmode_title_ko = csvreader.Read("learningmode_title_ko.csv");
        learningmode_title_vn = csvreader.Read("learningmode_title_vn.csv");


        //learningmode_guide_en = csvreader.Read("learningmode_guide_en");
        //learningmode_guide_ko = csvreader.Read("learningmode_guide_ko");
        //learningmode_title_en = csvreader.Read("learningmode_title_en");
        //learningmode_title_ko = csvreader.Read("learningmode_title_ko");
#else
        learningmode_guide_en = csvreader.Read("learningmode_guide_en");
        learningmode_guide_ko = csvreader.Read("learningmode_guide_ko");
        learningmode_guide_vn = csvreader.Read("learningmode_guide_vn");
        learningmode_title_en = csvreader.Read("learningmode_title_en");
        learningmode_title_ko = csvreader.Read("learningmode_title_ko");
        learningmode_title_vn = csvreader.Read("learningmode_title_vn");
#endif
    }


    private void Update()
    {
        //clickParticle.SetBool("Click", false);
        if (Input.GetMouseButtonDown(0))
        {


            Vector3 cpos = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 50f));

            CallerManager.Caller.GetInst.GetResByPool("TouchEffect", cpos, Quaternion.identity);

            audio.PlayOneShot(sounds_touch);
            //clickParticle.SetBool("Click", true);
            //clickParticle.Play("LineFX86");
        }

        GameObject go = middle_guide.transform.Find("Guide_Bubble/Ailen").gameObject;
        go.GetComponent<TweenPosition>().from = middle_ailen_point.transform.localPosition;
        go.GetComponent<TweenPosition>().to = new Vector3(middle_ailen_point.transform.localPosition.x, middle_ailen_point.transform.localPosition.y + 10, middle_ailen_point.transform.localPosition.z);
    }
    void AddPanels()
    {
        panel_Sprites = new List<UISprite>();
    }
    void SetAtlas()
    {
        if (curLanguage == Language.en)
        {
            lm_Atlas = Resources.Load(curLevel.name) as NGUIAtlas;
        }
        else if (curLanguage == Language.kr)
        {
            lm_Atlas = Resources.Load(curLevel.name + "_kr") as NGUIAtlas;
        }
    }
    void Init_Sprites()
    {
        if (curLanguage == Language.en)
        {
            BG.atlas = Resources.Load(curLevel.name + "_BG") as NGUIAtlas;
        }
        else if (curLanguage == Language.kr)
        {
            BG.atlas = Resources.Load(curLevel.name + "_BG_kr") as NGUIAtlas;
        }
        BG.spriteName = stage.ToString() + "_bg";
    }
    void SetPanel_SourceDimension(int width, int height)
    {
        panel_Source_UIWidget.SetDimensions(width, height);
        UpdatePanelPosition();
    }
    // Run this inside Init_Stage() if panels are not fit
    public void UpdatePanelPosition()
    {
        foreach (var item in panel_Sprites)
        {
            item.ResetAndUpdateAnchors();
        }
    }
    void SetTitleText(string title_en, string title_kr)
    {
        if (curLanguage == Language.en)
        {
            titleLabel.text = title_en;
        }
        else if (curLanguage == Language.kr)
        {
            titleLabel.text = title_kr;
        }
    }
    public void DeActivateCurrentGuideBlue()
    {
        // Deactivate Guide_Blue
        ray = cam.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit) && (hit.collider.tag == "Guide_Blue"))
        {
            hit.collider.gameObject.SetActive(false);
        }
    }
    public TweenPosition MoveContainerHorizontally(float newX)
    {
        Vector3 newPos = containerOriginPos;
        newPos.x = newX;

        return TweenPosition.Begin(container.gameObject, 0.5f, newPos, true);
    }
    public TweenPosition Local_MoveContainerHorizontally(float newX, float duration)
    {
        newX = CheckScreenResolution(newX);
        if (scManager.isFullScreen())
        {
            scManager.ChangeScreenPosition(newX, duration);
            return null;
        } else
        {
            Vector3 newPos = Vector3.zero;
            newPos.x = newX;
            newPos.y = -60;
            return TweenPosition.Begin(container.gameObject, duration, newPos, false);
        }
    }
    public float CheckScreenResolution(float posX)
    {
        float resolution = (float)Screen.height / (float)Screen.width;

        if(resolution > 1.7f && resolution < 1.8f)
        {
            // 16:9 해상도
            return posX;
        } else
        {
            // 그 외의 해상도
            return (resolution * posX) / 1.77778f;
        }
    }
    public TweenScale BoxTweenScale(GameObject box)
    {
        box.SetActive(true);
        TweenScale ts = TweenScale.Begin(box, 0.3f, Vector3.one);
        ts.from = Vector3.zero;
        return ts;
    }
    
    public void Set_Title(string Guide_text)
    {
        if (curLanguage == Language.en)
        {
            for (int i = 0; i < learningmode_title_en.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_title_en[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    titleLabel.text = value;
                }

            }

        }
        else if (curLanguage == Language.kr)
        {
            for (int i = 0; i < learningmode_title_ko.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_title_ko[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    titleLabel.text = value;
                }
            }
        } else if (curLanguage == Language.vn) {
            for (int i = 0; i < learningmode_title_vn.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_title_vn[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    titleLabel.text = value;
                }
            }
        }
    }

    public void Set_Start_Guide(string Guide_text, UIEventListener.VoidDelegate func)
    {
        int pick = UnityEngine.Random.Range(0, sounds_guide.Length);
        
        audio.PlayOneShot(sounds_guide[pick]);

        scvRead();
        start_guide.SetActive(true);

        if (curLanguage == Language.en)
        {
            for (int i = 0; i < learningmode_guide_en.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_en[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    start_guide_Lable.text = value;
                }
            }
        }
        else if (curLanguage == Language.kr)
        {
            for (int i = 0; i < learningmode_guide_ko.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_ko[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    start_guide_Lable.text = value;
                }
            }
        } else if (curLanguage == Language.vn) {
            for (int i = 0; i < learningmode_guide_vn.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_vn[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    start_guide_Lable.text = value;
                }
            }
        }

        start_guide_Lable.overflowMethod = UILabel.Overflow.ResizeFreely;
        start_guide_Lable.UpdateNGUIText();
        Vector2 size = NGUIText.CalculatePrintedSize(start_guide_Lable.text);
        if (size.x > 810 || size.y > 60)
        {
            start_guide_Lable.alignment = NGUIText.Alignment.Left;
            start_guide_Lable.overflowMethod = UILabel.Overflow.ResizeHeight;
        }
        else
        {
            start_guide_Lable.alignment = NGUIText.Alignment.Center;
        }

        UIEventListener.Get(Btn_Next).onClick = func;
        UIEventListener.Get(Btn_Skip).onClick = func;
    }

    public void On_Middle_Guide(string Guide_text, string position)
    {
        int pick = UnityEngine.Random.Range(0, sounds_guide.Length);
        audio.PlayOneShot(sounds_guide[pick]);

        if (position.Equals("MID"))
            middle_guide.transform.localPosition = new Vector3(0f, 0f, 0f);
        else if(position.Equals("BOTTOM"))
            middle_guide.transform.localPosition = new Vector3(0f, -530f, 0f);
        else if(position.Equals("TOP"))
            middle_guide.transform.localPosition = new Vector3(0f, 714f, 0f);

        middle_guide.SetActive(true);
        if (curLanguage == Language.en)
        {
            for (int i = 0; i < learningmode_guide_en.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_en[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    middle_guide_Lable.text = value;
                }

            }

        }
        else if (curLanguage == Language.kr)
        {
            for (int i = 0; i < learningmode_guide_ko.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_ko[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    middle_guide_Lable.text = value;
                }
            }
        } else if (curLanguage == Language.vn) {
            for (int i = 0; i < learningmode_guide_vn.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_vn[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    middle_guide_Lable.text = value;
                }
            }
        }
        /*
        middle_guide_Lable.overflowMethod = UILabel.Overflow.ResizeFreely;
        middle_guide_Lable.UpdateNGUIText();
        Vector2 size = NGUIText.CalculatePrintedSize(middle_guide_Lable.text);
        if (size.x > 810 || size.y > 60)
        {
            middle_guide_Lable.alignment = NGUIText.Alignment.Left;
            middle_guide_Lable.overflowMethod = UILabel.Overflow.ResizeHeight;
        }
        else
        {
            middle_guide_Lable.alignment = NGUIText.Alignment.Center;
        }*/
        
    }

    public void Off_Middle_Guide()
    {
        GameObject go = middle_guide.transform.Find("Guide_Bubble/Ailen").gameObject;
        go.GetComponent<TweenPosition>().ResetToBeginning();
        go.GetComponent<TweenPosition>().SetEndToCurrentValue();

        middle_guide.SetActive(false);
    }


    public void On_End_Guide(string Guide_text)
    {
        audio.PlayOneShot(sounds_end);
        //end_guide.transform.position = new Vector3(CameraManager.ins.transform.position.x, end_guide.transform.position.y, end_guide.transform.position.z);

        end_guide.SetActive(true);
        if (curLanguage == Language.en)
        {
            for (int i = 0; i < learningmode_guide_en.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_en[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    end_guide_Lable.text = value;
                }

            }

        }
        else if (curLanguage == Language.kr)
        {
            for (int i = 0; i < learningmode_guide_ko.Count; i++)
            {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_ko[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value))
                {
                    end_guide_Lable.text = value;
                }
            }
        } else if (curLanguage == Language.vn) {
            for (int i = 0; i < learningmode_guide_vn.Count; i++) {
                var tmp = new Dictionary<string, string>();
                tmp = learningmode_guide_vn[i];
                string value;
                if (tmp.TryGetValue(Guide_text, out value)) {
                    end_guide_Lable.text = value;
                }
            }
        }

        end_guide_Lable.overflowMethod = UILabel.Overflow.ResizeFreely;
        end_guide_Lable.UpdateNGUIText();
        Vector2 size = NGUIText.CalculatePrintedSize(end_guide_Lable.text);
        if (size.x > 810 || size.y > 60)
        {
            end_guide_Lable.alignment = NGUIText.Alignment.Left;
            end_guide_Lable.overflowMethod = UILabel.Overflow.ResizeHeight;
        }
        else
        {
            end_guide_Lable.alignment = NGUIText.Alignment.Center;
        }

        CameraManager.SCREEN_MOVE_PAUSE = false;
    }

    public void Off_End_Guide()
    {
        end_guide.SetActive(false);
    }

    public void On_Start_Guide_Click()
    {
        start_guide.SetActive(false);
    }

    public void OnRepeatButtonClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void OnCloseButtonClick()
    {
        Application.Quit();
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }

    #region Option Functions

    public void OnOptionClick()
    {
        panel_Option.SetActive(true);
    }
    void Option_SetLanguage()
    {
        if (toggleLanguage.value)
            static_curLanguage = Language.en;
        else
            static_curLanguage = Language.kr;
    }
    void Option_SetStage(GameObject gameObject)
    {
        stage = (Stage)Enum.Parse(typeof(Stage), gameObject.name);
    }
    public void Option_OnStartClick()
    {
        isFirstLoad = false;
        isOptionSelected = true;
        Option_SetLanguage();
        static_stage = stage;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
    public void Option_OnCancelClick()
    {
        panel_Option.SetActive(false);
    }
    public void Option_OnStageSelect(GameObject gameObject)
    {
        selectedStage.text = gameObject.name;
    }

    #endregion
}
