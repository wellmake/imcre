﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 카메라의 이동과 핀치줌을 구현
/// 본 컴포넌트는 최상단 메인카메라에 Add 되며 이동, 확대 등의 변동이 이루어지는 컨텐츠를 비추는 카메라임.
/// 'UI Root' 밑의 'NGUI Layer'를 비추며 이동하지 않거나 확대/축소 등의 내용이 반영되면 안되는 UI는 'UI Top' 하위 'Default Layer'로 표현해야함.
/// ex) 상단타이틀, 팝업 등
/// </summary>
public class CameraManager : MonoBehaviour
{
    public static CameraManager ins;

    public float moveSpeed = 0.1f;

    public GameObject objTitleBar;
    public GameObject objBG;

    Vector3 touchStart;
    public float zoomOutMin = 0.5f;
    public float zoomOutMax = 1f;

    public float cameraHalfWidth;
    public float cameraHalfHeight;

    public Vector2 minBound;
    public Vector2 maxBound;

    private void Start()
    {

    }

    /***********************************************************
     * 
     * SetBoxColliderArea()
     * 드래그를 통해 화면 이동 시, 배경을 기준으로 현재 카메라 확대값에 따른 이동 범위 제한을 위해
     * 이동 범위를 계산, Update()에서 호출하여 실시간으로 체크
     * 
    ************************************************************/
    public void SetBoxColliderArea()
    {
        objBG.GetComponent<BoxCollider2D>().size = new Vector2((float)objBG.GetComponent<UISprite>().width, objBG.GetComponent<UISprite>().height + ((float)objTitleBar.GetComponent<UISprite>().height / (1 + (1 - Camera.main.orthographicSize) * 2)));
        objBG.GetComponent<BoxCollider2D>().offset = new Vector2((float)objBG.GetComponent<UISprite>().width / 2f, (-1 * objBG.GetComponent<UISprite>().height / 2) + ((float)objTitleBar.GetComponent<UISprite>().height / (2 * (1 + (1 - Camera.main.orthographicSize) * 2))));

        minBound = objBG.GetComponent<BoxCollider2D>().bounds.min;
        maxBound = objBG.GetComponent<BoxCollider2D>().bounds.max;
    }

    private void Awake()
    {
        ins = this;

    }

    /***********************************************************
     * 
     * InitCamera()
     * 최초 실행 또는 스테이지 전환에 따른 씬 로드 시, 가로/세로 모드에 따른 카메라 orthographicSize 세팅
     * Landscape 상태에서 배경과 상단 타이틀바에 따른 비율 이슈로 카메라 최대값(1f)에서 배경 밖의 영역이 화면에 나타나게되는데
     * 이를 보이지 않게 해야하는 구현 요청에 따라 18:9 비율 해상도의 가로모드에서도 배경 밖의 영역이 나타나지 않는 orthographicSize 0.8f로 세팅함.
     * 스테이지 전환 시점에 가로모드였다면 씬 로드 시점의 카메라 위치값이 다르기때문에 SetBoxColliderArea()에서 계산된 범위를 기반으로 카메라를 좌측상단으로 이동(Invoke)
     * 
    ************************************************************/
    public void InitCamera()
    {
        if(ScreenManager.ins.isFullScreen())
        {
            Camera.main.orthographicSize = 0.8f;

            if (!LM_AE_Manager.isFirstLoad)
            {
                Invoke("CheckResolution", 1f);
            }
        }
        else
            Camera.main.orthographicSize = 1f;
    }

    public void CheckResolution()
    {
        MoveCamera(true, true, 0.3f);

    }

    /***********************************************************
     * 
     * Update()
     * 화면에 터치된 수에 따라 이동과 줌을 판단하여 동작함.
     * 
     * SCREEN_MOVE_PAUSE
     * 전역변수로 각 스테이지 개발 시, 예외적인 상황에서 카메라의 이동을 제한하기 위한 플래그.
     * true = 카메라 고정.
     * false = 카메라 이동 가능(기본값).
     * 
    ************************************************************/
    public static bool SCREEN_MOVE_PAUSE = false;
    private void Update()
    {
        SetBoxColliderArea();

        if (Input.GetMouseButtonDown(0) && !SCREEN_MOVE_PAUSE)
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            zoom(difference * 0.001f);

            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            MoveCamera(new Vector3(direction.x, direction.y, 0f));

        }
        else if (Input.GetMouseButton(0))
        {
            if (!SCREEN_MOVE_PAUSE)
            {
                Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
                MoveCamera(direction);
            } else
            {
                touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
        }
        
        zoom(Input.GetAxis("Mouse ScrollWheel"));
    }

    /***********************************************************
     * 
     * MoveCamera()
     * 카메라 이동을 위한 여러 함수 중 특정 객체를 중심으로한 시점이동을 위한 함수
     * 단, 상/하의 포지션은 세로값이 짧은 배경상 위치를 특정하기 어려우므로 상단으로 고정 또는 하단으로 설정하여 이동
     * isTop = true 카메라의 y값이 상단 최대로 설정되어 이동.
     * isTop = false 카메라의 y값이 하단 최대로 설정되어 이동.
     * 
    ************************************************************/
    public TweenPosition MoveCamera(Vector3 vec, float duration, bool isTop)
    {
        SCREEN_MOVE_PAUSE = true;

        if (Screen.orientation == ScreenOrientation.Portrait)
        {
            vec = new Vector3(vec.x, 0f, -50f);
            TweenPosition tw = UITweener.Begin<TweenPosition>(Camera.main.gameObject, duration);
            tw.from = Camera.main.gameObject.transform.position;
            tw.to = vec;
            tw.AddOnFinished(new EventDelegate(delegate {
                SCREEN_MOVE_PAUSE = false;
            }));
            return tw;
            //return TweenPosition.Begin(Camera.main.gameObject, duration, vec);

        }
        else
        {
            cameraHalfHeight = Camera.main.orthographicSize;
            cameraHalfWidth = cameraHalfHeight * Screen.width / Screen.height;

            if (vec == Vector3.zero)
                return TweenPosition.Begin(Camera.main.gameObject, duration, new Vector3(0f, 0f, -50f));
            else
            {
                
                if (isTop)
                {
                    TweenPosition tw = UITweener.Begin<TweenPosition>(Camera.main.gameObject, duration);
                    tw.from = Camera.main.gameObject.transform.position;
                    if (vec.x >= maxBound.x - cameraHalfWidth)
                        vec.x = maxBound.x - cameraHalfWidth;
                    else if (vec.x <= minBound.x + cameraHalfWidth)
                        vec.x = minBound.x + cameraHalfWidth;
                    tw.to = new Vector3(vec.x, (maxBound.y - cameraHalfHeight), -50f);
                    tw.AddOnFinished(new EventDelegate(delegate {
                        SCREEN_MOVE_PAUSE = false;
                    }));
                    return tw;
                } else
                {
                    TweenPosition tw = UITweener.Begin<TweenPosition>(Camera.main.gameObject, duration);
                    tw.from = Camera.main.gameObject.transform.position;
                    if (vec.x >= maxBound.x - cameraHalfWidth)
                        vec.x = maxBound.x - cameraHalfWidth;
                    else if (vec.x <= minBound.x + cameraHalfWidth)
                        vec.x = minBound.x + cameraHalfWidth;
                    tw.to = new Vector3(vec.x, (minBound.y + cameraHalfHeight), -50f);
                    tw.AddOnFinished(new EventDelegate(delegate {
                        SCREEN_MOVE_PAUSE = false;
                    }));
                    return tw;
                }
                    
            }
        }
    }

    /***********************************************************
     * 
     * MoveCamera()
     * 카메라 이동을 위한 여러 함수 중 상/하 좌/우 각각의 최대값으로의 이동을 위한 함수
     * isTop = true 카메라의 y값이 상단 최대로 설정되어 이동.
     * isTop = false 카메라의 y값이 하단 최대로 설정되어 이동.
     * isLeft = true 카메라의 x값이 좌측 최대로 설정되어 이동.
     * isLeft = false 카메라의 x값이 우측 최대로 설정되어 이동.
     * 
    ************************************************************/
    public TweenPosition MoveCamera(bool isTop, bool isLeft, float duration)
    {
        SCREEN_MOVE_PAUSE = true;

        cameraHalfHeight = Camera.main.orthographicSize;
        cameraHalfWidth = cameraHalfHeight * Screen.width / Screen.height;

        float posX, posY;
        if (isTop)
        {
            if (Camera.main.orthographicSize == 1f)
                posY = 0f;
            else
                posY = (maxBound.y - cameraHalfHeight);
        }
        else
        {
            if (Camera.main.orthographicSize == 1f)
                posY = 0f;
            else
                posY = (minBound.y + cameraHalfHeight);
        }

        if (isLeft)
            posX = minBound.x + cameraHalfWidth;
        else
            posX = maxBound.x - cameraHalfWidth;

        TweenPosition tw = UITweener.Begin<TweenPosition>(Camera.main.gameObject, duration);
        tw.from = Camera.main.gameObject.transform.position;
        tw.to = new Vector3(posX, posY, -50f);
        tw.AddOnFinished(new EventDelegate(delegate {
            SCREEN_MOVE_PAUSE = false;
        }));
        return tw;

    }

    /***********************************************************
     * 
     * MoveCamera()
     * 터치포인트를 기반으로 화면 이동을 위한 함수.
     * 
    ************************************************************/
    void MoveCamera(Vector3 vec)
    {
        float clampX, clampY;


        cameraHalfHeight = Camera.main.orthographicSize;
        cameraHalfWidth = cameraHalfHeight * Screen.width / Screen.height;

        if (Camera.main.transform.position.x + vec.x <= minBound.x + cameraHalfWidth)
        {
            clampX = minBound.x + cameraHalfWidth;
        }
        else if (Camera.main.transform.position.x + vec.x >= maxBound.x - cameraHalfWidth)
        {
            clampX = maxBound.x - cameraHalfWidth;
        }
        else
        {
            clampX = Camera.main.transform.position.x + vec.x;
        }

        if (Camera.main.transform.position.y + vec.y < minBound.y + cameraHalfHeight)
        {
            clampY = minBound.y + cameraHalfHeight;
        }
        else if (Camera.main.transform.position.y + vec.y >= maxBound.y - cameraHalfHeight)
        {
            if (Camera.main.orthographicSize == 1f)
                clampY = 0f;
            else
                clampY = maxBound.y - cameraHalfHeight;
        }
        else
        {
            clampY = Camera.main.transform.position.y + vec.y;
        }


        Camera.main.transform.position = new Vector3(clampX, clampY, -50f);
    }

    void zoom(float increment)
    {
        float temp_orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, zoomOutMin, zoomOutMax);

        if (Screen.orientation == ScreenOrientation.Portrait)
        {
            Camera.main.orthographicSize = temp_orthographicSize;
        }
        else
        {
            Camera.main.orthographicSize = temp_orthographicSize;
        }

    }
}