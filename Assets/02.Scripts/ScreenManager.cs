﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 가로/세로 모드의 전환 구현
/// </summary>
public class ScreenManager : MonoBehaviour
{
    public static ScreenManager ins;
    public GameObject objAilen;
    public GameObject objAilenPoint;
    public GameObject objAilenPoint2;

    public GameObject objTitle;

    public GameObject objBtnExitFullScreen;

    bool isChangedScreenPos = false;
    float lastScreenPositionX, lastScreenDuration;

    private void Awake()
    {
        ins = this;
    }

    // 스테이지 전환에 따라 가로/세로모드에서 시작 시 나타나는 팝업의 외계인의 위치를 조정
    void Start()
    {
        if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            objAilen.GetComponent<UISprite>().SetAnchor(this.objAilenPoint2.transform);
            objAilen.GetComponent<UISprite>().topAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().topAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().rightAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().rightAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;

        }
        else
        {
       
            objAilen.GetComponent<UISprite>().SetAnchor(this.objAilenPoint.transform);
            objAilen.GetComponent<UISprite>().topAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().topAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().rightAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().rightAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // 현재 스크린 orientation에 따른 가로/세로 전환
    // 가로모드에서는 카메라 최대 축소값을 0.8f로 고정(CameraManager > InitCamera() 참고)
    public void ChangeScreenOrientataion()
    {
        CameraManager.SCREEN_MOVE_PAUSE = true;
        CameraManager.ins.InitCamera();

        if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            objAilen.GetComponent<UISprite>().SetAnchor(this.objAilenPoint.transform);
            objAilen.GetComponent<UISprite>().topAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().topAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().rightAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().rightAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;

            //objTitle.SetActive(true);
            Screen.orientation = ScreenOrientation.Portrait;
            CameraManager.ins.zoomOutMax = 1f;

            
        } else
        {
            objAilen.GetComponent<UISprite>().SetAnchor(this.objAilenPoint2.transform);
            objAilen.GetComponent<UISprite>().topAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().topAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().bottomAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.relative = 0;
            objAilen.GetComponent<UISprite>().leftAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().rightAnchor.relative = 1;
            objAilen.GetComponent<UISprite>().rightAnchor.absolute = 0;
            objAilen.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;

            //objTitle.SetActive(false);
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            CameraManager.ins.zoomOutMax = 0.8f;
            Camera.main.orthographicSize = 0.8f;


            
        }
        Invoke("SetScreenSize", 2f);
    }

    // 가로/세로 모드 전환 시, 화면이 전환 된 이후 카메라 값을 좌측 상단으로 조정해주기 위한 함수
    public void SetScreenSize() {

        CameraManager.ins.MoveCamera(true, true, 0.3f).onFinished.Add(new EventDelegate(delegate {

            CameraManager.SCREEN_MOVE_PAUSE = false;
        }));
    }

    // 추후 가로모드(전체화면) 해제 버튼 생성 후 호출
    public void OnClickExitFullScreen()
    {
        objBtnExitFullScreen.SetActive(false);
        ChangeScreenOrientataion();
    }

    // 가로/세로 모드 확인
    public bool isFullScreen()
    {
        if (Screen.orientation == ScreenOrientation.Portrait)
            return false;
        else
            return true;
    }

    // 사용하지 않음
    public void ChangeScreenPosition(float newPosX, float duration)
    {
        isChangedScreenPos = true;
        lastScreenPositionX = newPosX;
        lastScreenDuration = duration;
    }
}
