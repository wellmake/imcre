﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 드래그 앤 드롭 구현
/// 객체가 활성화 된 시점에 StartDrag() 호출하여 드래그 앤 드롭 애니메이션 동작
/// </summary>
public class PrefabDragAnim : MonoBehaviour
{
    public GameObject objDragStartPoint;
    public GameObject objDragEndPoint;
    public bool isStop = false;

    Vector3 initPos;
    // Start is called before the first frame update
    void Start()
    {
        initPos = this.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        //CameraManager.SCREEN_MOVE_PAUSE = true;
    }

    private void OnDisable()
    {
        //CameraManager.SCREEN_MOVE_PAUSE = false;
    }

    public void StopDrag()
    {
        isStop = true;
        this.gameObject.SetActive(false);
    }


    public void StartDrag()
    {
        this.gameObject.GetComponent<TweenScale>().onFinished.Clear();
        this.gameObject.GetComponent<TweenScale>().from = new Vector3(1f, 1f, 1f);
        this.gameObject.GetComponent<TweenScale>().to = new Vector3(0.8f, 0.8f, 1f);
        this.gameObject.GetComponent<TweenScale>().duration = 0.3f;
        this.gameObject.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
        {
            this.gameObject.GetComponent<TweenScale>().onFinished.Clear();
            this.gameObject.GetComponent<TweenPosition>().onFinished.Clear();
            this.gameObject.GetComponent<TweenPosition>().from = new Vector3(objDragStartPoint.transform.localPosition.x, objDragStartPoint.transform.localPosition.y, this.transform.localPosition.z);
            this.gameObject.GetComponent<TweenPosition>().to = new Vector3(objDragEndPoint.transform.localPosition.x, objDragEndPoint.transform.localPosition.y, this.transform.localPosition.z);
            this.gameObject.GetComponent<TweenPosition>().duration = 1f;
            this.gameObject.GetComponent<TweenPosition>().onFinished.Add(new EventDelegate(delegate
            {
                //this.gameObject.GetComponent<TweenScale>().from = new Vector3(0.9f, 0.9f, 1f);
                //this.gameObject.GetComponent<TweenScale>().to = new Vector3(1f, 1f, 1f);
                //this.gameObject.GetComponent<TweenScale>().duration = 0.2f;
                this.gameObject.GetComponent<TweenScale>().onFinished.Clear();
                this.gameObject.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                {
                    ResetDrag();
                }));
                this.gameObject.GetComponent<TweenScale>().PlayReverse();

            }));
            this.gameObject.GetComponent<TweenPosition>().Play();

        }));
        this.gameObject.GetComponent<TweenScale>().PlayForward();
    }

    public void ResetDrag()
    {
        this.gameObject.GetComponent<TweenScale>().onFinished.Clear();
        this.gameObject.GetComponent<TweenPosition>().onFinished.Clear();

        this.gameObject.GetComponent<TweenScale>().ResetToBeginning();
        this.gameObject.GetComponent<TweenPosition>().ResetToBeginning();

        this.gameObject.transform.localScale = Vector3.one;
        if (!isStop)
            Invoke("StartDrag", 1.7f);
    }
}
