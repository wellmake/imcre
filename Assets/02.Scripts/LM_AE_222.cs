﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_222 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step00_Guide")]

    [Header("Step01")]
    public GameObject step01;
    public UISprite step01_sprite01;
    public UISprite step01_sprite02;
    public UISprite step01_sprite03;
    public UISprite step01_sprite04;
    public UISprite step01_sprite05;
    public GameObject step01_groupMP4;
    public UISprite step01_spriteA;
    public UISprite step01_spriteB;
    public UISprite step01_spriteC;
    public UISprite step01_spriteD;
    public UISprite step01_spriteFolder;
    public GameObject step01_box1;
    public GameObject step01_box1_5;
    public UISprite step01_spriteFolderInput;
    public UILabel step01_labelFolderInput;
    public UISprite step01_spriteFolderInputUntitle;
    public UISprite step01_spriteFolderInputSelected;
    public GameObject step01_keyboard_Shift;
    public GameObject step01_box2;
    public GameObject step01_box3;
    public GameObject step01_box4;
    public GameObject step01_box5;
    public GameObject step01_box5_dragPointer;
    public GameObject step01_box5_dragEndPoint;
    public GameObject step01_groupMP4_dragable;
    public UISprite step01_spriteA_2;
    public UISprite step01_spriteB_2;
    public UISprite step01_spriteC_2;
    public UISprite step01_spriteD_2;
    public GameObject step01_box6;
    public GameObject step01_box7;
    public GameObject step01_box8;
    public GameObject step01_box9;
    public GameObject step01_box9_dragPointer;
    public GameObject step01_box9_dragEndPoint;
    public GameObject step01_groupPNG_dragable;
    public GameObject step01_PNG_resetpoint;
    public UISprite step01_sprite01_2;
    public UISprite step01_sprite02_2;
    public UISprite step01_sprite03_2;
    public UISprite step01_sprite04_2;
    public UISprite step01_sprite05_2;
    public UISprite step01_spriteFolderInput2;
    public UILabel step01_labelFolderInput2;
    public UISprite step01_spriteFolderInputUntitle2;
    public UISprite step01_spriteFolderInputSelected2;
    public GameObject step01_box10;

    public TweenAlpha step01_delay1;
    public TweenAlpha step01_delay2;

    [Header("Step02")]
    public GameObject step02;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_222_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));


    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_sprite01);
        lmSpriteList.Add(step01_sprite02);
        lmSpriteList.Add(step01_sprite03);
        lmSpriteList.Add(step01_sprite04);
        lmSpriteList.Add(step01_sprite05);
        lmSpriteList.Add(step01_spriteA);
        lmSpriteList.Add(step01_spriteB);
        lmSpriteList.Add(step01_spriteC);
        lmSpriteList.Add(step01_spriteD);
        lmSpriteList.Add(step01_spriteFolder);
        lmSpriteList.Add(step01_spriteFolderInput);
        lmSpriteList.Add(step01_spriteFolderInputUntitle);
        lmSpriteList.Add(step01_spriteFolderInputSelected);
        lmSpriteList.Add(step01_spriteA_2);
        lmSpriteList.Add(step01_spriteB_2);
        lmSpriteList.Add(step01_spriteC_2);
        lmSpriteList.Add(step01_spriteD_2);
        lmSpriteList.Add(step01_sprite01_2);
        lmSpriteList.Add(step01_sprite02_2);
        lmSpriteList.Add(step01_sprite03_2);
        lmSpriteList.Add(step01_sprite04_2);
        lmSpriteList.Add(step01_sprite05_2);
        lmSpriteList.Add(step01_spriteFolderInput2);
        lmSpriteList.Add(step01_spriteFolderInputUntitle2);
        lmSpriteList.Add(step01_spriteFolderInputSelected2);


        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_sprite01.spriteName = "AE_222_01.png";
        step01_sprite02.spriteName = "AE_222_02.png";
        step01_sprite03.spriteName = "AE_222_03.png";
        step01_sprite04.spriteName = "AE_222_04.png";
        step01_sprite05.spriteName = "AE_222_05.png";
        step01_spriteA.spriteName = "AE_222_A.mp4";
        step01_spriteB.spriteName = "AE_222_B.mp4";
        step01_spriteC.spriteName = "AE_222_C.mp4";
        step01_spriteD.spriteName = "AE_222_D.mp4";
        step01_spriteFolder.spriteName = "AE_222_Folder-Icon";
        step01_spriteFolderInput.spriteName = "AE_222_Folder_1";
        step01_spriteFolderInputUntitle.spriteName = "AE_222_Folder_2";
        step01_spriteFolderInputSelected.spriteName = "AE_222_Video-Folder_Select";
        step01_spriteA_2.spriteName = "AE_222_A.mp4_Select";
        step01_spriteB_2.spriteName = "AE_222_B.mp4_Select";
        step01_spriteC_2.spriteName = "AE_222_C.mp4_Select";
        step01_spriteD_2.spriteName = "AE_222_D.mp4_Select";
        step01_sprite01_2.spriteName = "AE_222_01.png_Select";
        step01_sprite02_2.spriteName = "AE_222_02.png_Select";
        step01_sprite03_2.spriteName = "AE_222_03.png_Select";
        step01_sprite04_2.spriteName = "AE_222_04.png_Select";
        step01_sprite05_2.spriteName = "AE_222_05.png_Select";
        step01_spriteFolderInput2.spriteName = "AE_222_Folder_1";
        step01_spriteFolderInputUntitle2.spriteName = "AE_222_Folder_2";
        step01_spriteFolderInputSelected2.spriteName = "AE_222_Image-Folder";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        step01.SetActive(true);

        step01_spriteFolder.gameObject.SetActive(true);
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
        {
            step01_spriteFolder.gameObject.SetActive(false);
            step01_box1.SetActive(false);
            step01_spriteFolderInput.gameObject.SetActive(true);
            MoveDown(step01_sprite01.gameObject, 1f);
            MoveDown(step01_sprite02.gameObject, 1f);
            MoveDown(step01_sprite03.gameObject, 1f);
            MoveDown(step01_sprite04.gameObject, 1f);
            MoveDown(step01_sprite05.gameObject, 1f);
            MoveDown(step01_spriteA.gameObject, 1f);
            MoveDown(step01_spriteB.gameObject, 1f);
            MoveDown(step01_spriteC.gameObject, 1f);
            MoveDown(step01_spriteD.gameObject, 1f);

            step01_box1_5.SetActive(true);
            step01_box1_5.GetComponent<TweenAlpha>().onFinished.Clear();
            step01_box1_5.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
            {
                LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_222_1", "MID");

                step01_box1_5.GetComponent<UIEventTrigger>().onClick.Clear();
                step01_box1_5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                {
                    LM_AE_Manager.ins.Off_Middle_Guide();
                    step01_box1_5.SetActive(false);
                    step01_spriteFolderInputUntitle.gameObject.SetActive(false);
                    step01_labelFolderInput.gameObject.SetActive(true);
                    step01_labelFolderInput.GetComponent<TypewriterEffect>().onFinished.Clear();
                    step01_labelFolderInput.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate
                    {
                        step01_spriteFolderInput.gameObject.SetActive(false);
                        step01_spriteFolderInputSelected.gameObject.SetActive(true);
                        step01_spriteFolderInputSelected.GetComponent<TweenAlpha>().onFinished.Clear();
                        step01_spriteFolderInputSelected.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                        {
                            step01_keyboard_Shift.SetActive(true);
                            step01_keyboard_Shift.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_keyboard_Shift.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                            {
                                step01_keyboard_Shift.GetComponent<TweenScale>().enabled = false;
                                step01_keyboard_Shift.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);

                                step01_box2.SetActive(true);
                                step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                                step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                {
                                    step01_spriteA.spriteName = "AE_222_A.mp4_Select";

                                    step01_box2.SetActive(false);
                                    step01_box3.SetActive(true);
                                    step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                    {
                                        step01_box3.SetActive(false);
                                        step01_spriteB.spriteName = "AE_222_B.mp4_Select";
                                        step01_spriteC.spriteName = "AE_222_C.mp4_Select";
                                        step01_spriteD.spriteName = "AE_222_D.mp4_Select";

                                        
                                        step01_keyboard_Shift.transform.localScale = Vector3.one;
                                        step01_keyboard_Shift.SetActive(false);

                                        step01_box4.SetActive(true);
                                        step01_box5.SetActive(true);
                                        step01_box5_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                                        step01_groupMP4_dragable.SetActive(true);
                                        step01_groupMP4_dragable.transform.localPosition = step01_groupMP4.transform.localPosition;
                                        step01_groupMP4_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                                        step01_groupMP4_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                            CameraManager.SCREEN_MOVE_PAUSE = true;
                                            step01_groupMP4_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                                        }));
                                        step01_groupMP4_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
                                        {
                                            CameraManager.SCREEN_MOVE_PAUSE = false;
                                            if (step01_groupMP4_dragable.transform.position.y < step01_box4.transform.position.y + 0.05f && step01_groupMP4_dragable.transform.position.y > step01_box4.transform.position.y - 0.05f)
                                            {
                                                step01_groupMP4_dragable.SetActive(false);
                                                step01_box4.SetActive(false);
                                                step01_box5.SetActive(false);

                                                step01_spriteFolderInputSelected.spriteName = "AE_222_Video-Folder";

                                                step01_spriteA.spriteName = "AE_222_A.mp4_IN";
                                                step01_spriteB.spriteName = "AE_222_B.mp4_IN";
                                                step01_spriteC.spriteName = "AE_222_C.mp4_IN";
                                                step01_spriteD.spriteName = "AE_222_D.mp4_IN";

                                                MoveDown(step01_sprite01.gameObject, 4f);
                                                MoveDown(step01_sprite02.gameObject, 4f);
                                                MoveDown(step01_sprite03.gameObject, 4f);
                                                MoveDown(step01_sprite04.gameObject, 4f);
                                                MoveDown(step01_sprite05.gameObject, 4f);
                                                MoveUp(step01_spriteA.gameObject, 5f);
                                                MoveUp(step01_spriteB.gameObject, 5f);
                                                MoveUp(step01_spriteC.gameObject, 5f);
                                                MoveUp(step01_spriteD.gameObject, 5f);

                                                LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_222_2", "MID");

                                                step01_delay1.gameObject.SetActive(true);
                                                step01_delay1.onFinished.Clear();
                                                step01_delay1.onFinished.Add(new EventDelegate(delegate
                                                {
                                                    LM_AE_Manager.ins.Off_Middle_Guide();

                                                    step01_keyboard_Shift.SetActive(true);
                                                    step01_keyboard_Shift.GetComponent<TweenScale>().enabled = true;
                                                    step01_keyboard_Shift.GetComponent<UIEventTrigger>().onClick.Clear();
                                                    step01_keyboard_Shift.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                    {
                                                        step01_keyboard_Shift.GetComponent<TweenScale>().enabled = false;
                                                        step01_keyboard_Shift.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);

                                                        step01_box6.SetActive(true);
                                                        step01_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                                                        step01_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                        {
                                                            step01_box6.SetActive(false);
                                                            step01_box7.SetActive(true);

                                                            step01_sprite01.spriteName = "AE_222_01.png_Select";

                                                            step01_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                                                            step01_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                            {
                                                                step01_box7.SetActive(false);
                                                                step01_keyboard_Shift.SetActive(false);

                                                                step01_sprite02.spriteName = "AE_222_02.png_Select";
                                                                step01_sprite03.spriteName = "AE_222_03.png_Select";
                                                                step01_sprite04.spriteName = "AE_222_04.png_Select";
                                                                step01_sprite05.spriteName = "AE_222_05.png_Select";

                                                                step01_box8.SetActive(true);
                                                                step01_box9.SetActive(true);
                                                                step01_spriteFolder.gameObject.SetActive(true);
                                                                step01_box9_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                                                                step01_groupPNG_dragable.SetActive(true);
                                                                step01_groupPNG_dragable.transform.localPosition = step01_PNG_resetpoint.transform.localPosition;
                                                                step01_groupPNG_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                                                                step01_groupPNG_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                                                                    CameraManager.SCREEN_MOVE_PAUSE = true;
                                                                    step01_groupPNG_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                                                                }));
                                                                step01_groupPNG_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
                                                                {
                                                                    CameraManager.SCREEN_MOVE_PAUSE = false;
                                                                    if (step01_groupPNG_dragable.transform.position.y < step01_spriteFolder.transform.position.y - 0.15f && step01_groupPNG_dragable.transform.position.y > step01_spriteFolder.transform.position.y - 0.3f)
                                                                    {
                                                                        step01_spriteFolder.gameObject.SetActive(false);
                                                                        step01_groupPNG_dragable.SetActive(false);
                                                                        step01_box8.SetActive(false);
                                                                        step01_box9.SetActive(false);

                                                                        step01_sprite01.spriteName = "AE_222_01.png_IN";
                                                                        step01_sprite02.spriteName = "AE_222_02.png_IN";
                                                                        step01_sprite03.spriteName = "AE_222_03.png_IN";
                                                                        step01_sprite04.spriteName = "AE_222_04.png_IN";
                                                                        step01_sprite05.spriteName = "AE_222_05.png_IN";

                                                                        MoveDown(step01_sprite01.gameObject, 1f);
                                                                        MoveDown(step01_sprite02.gameObject, 1f);
                                                                        MoveDown(step01_sprite03.gameObject, 1f);
                                                                        MoveDown(step01_sprite04.gameObject, 1f);
                                                                        MoveDown(step01_sprite05.gameObject, 1f);

                                                                        step01_spriteFolderInput2.gameObject.SetActive(true);
                                                                        step01_box10.SetActive(true);
                                                                        step01_box10.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                        step01_box10.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                                        {
                                                                            step01_box10.SetActive(false);
                                                                            step01_spriteFolderInputUntitle2.gameObject.SetActive(false);
                                                                            step01_labelFolderInput2.gameObject.SetActive(true);
                                                                            step01_labelFolderInput2.GetComponent<TypewriterEffect>().onFinished.Clear();
                                                                            step01_labelFolderInput2.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate
                                                                            {
                                                                                step01_spriteFolderInputSelected2.gameObject.SetActive(true);

                                                                                step01_delay2.gameObject.SetActive(true);
                                                                                step01_delay2.onFinished.Clear();
                                                                                step01_delay2.onFinished.Add(new EventDelegate(delegate
                                                                                {
                                                                                    StartCoroutine(EndDelay());
                                                                                }));
                                                                            }));
                                                                        }));
                                                                    }
                                                                    else
                                                                    {
                                                                        step01_groupPNG_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                                                                        step01_groupPNG_dragable.transform.localPosition = step01_PNG_resetpoint.transform.localPosition;
                                                                    }
                                                                }));
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }
                                            else
                                            {
                                                step01_groupMP4_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                                                step01_groupMP4_dragable.transform.localPosition = step01_groupMP4.transform.localPosition;
                                            }
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
    }

    public void MoveUp(GameObject go, float multiple)
    {
        go.GetComponent<TweenPosition>().from = go.transform.localPosition;
        go.GetComponent<TweenPosition>().to = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y + (32f * multiple), go.transform.localPosition.z);
        go.GetComponent<TweenPosition>().delay = 0f;
        go.GetComponent<TweenPosition>().duration = 0f;
        go.GetComponent<TweenPosition>().Play();
    }

    public void MoveDown(GameObject go, float multiple)
    {
        go.GetComponent<TweenPosition>().from = go.transform.localPosition;
        go.GetComponent<TweenPosition>().to = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y - (32f * multiple), go.transform.localPosition.z);
        go.GetComponent<TweenPosition>().delay = 0f;
        go.GetComponent<TweenPosition>().duration = 0f;
        go.GetComponent<TweenPosition>().Play();
    }

    public void OnStep02()
    {

    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_222_3");
    }
}
