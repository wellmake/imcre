﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_112 : MonoBehaviour
{
    float panel_Tool_Xpos;
    float panel_Timeline_Xpos;
    Camera cam;
    bool dragOn;
    Ray ray;
    RaycastHit hit;
    Vector3 newClipPosition;

    [Header("base sprites")]
    public UISprite monitor_Image;
    public UISprite clock_Blue;
    public UISprite clock_White;
    public GameObject v_Clip01;
    public UISprite edit_Line;
    public UISprite panel_Project_Inside;

    [Header("Objects")]
    public GameObject boxb_Razer;
    public GameObject razer_Selected;
    public GameObject guideline;
    public GameObject boxb_Guideline;
    public GameObject v_Clip02_L;
    public GameObject v_Clip02_R;
    public GameObject boxb_Select;
    public UISprite sprite_Selected;
    public GameObject boxb_Drag;
    public Transform boxr_DragEnd;

    void Start()
    {
        cam = Camera.main;
        SetSprites();
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnStageStart;
    }
    void SetSprites()
    {
        List<UISprite> lmSpriteList = new List<UISprite>();
        lmSpriteList.Add(monitor_Image);
        lmSpriteList.Add(clock_Blue);
        lmSpriteList.Add(clock_White);
        lmSpriteList.Add(v_Clip01.GetComponent<UISprite>());
        lmSpriteList.Add(edit_Line);
        lmSpriteList.Add(panel_Project_Inside);
        lmSpriteList.Add(razer_Selected.GetComponent<UISprite>());
        lmSpriteList.Add(v_Clip02_L.GetComponent<UISprite>());
        lmSpriteList.Add(v_Clip02_R.GetComponent<UISprite>());
        lmSpriteList.Add(sprite_Selected);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "DragStart")
            {
                dragOn = true;
            }
        }
        else if (dragOn && Input.GetMouseButton(0))
        {
            newClipPosition = Input.mousePosition;
            newClipPosition.z = boxr_DragEnd.position.z - cam.transform.position.z;
            newClipPosition = cam.ScreenToWorldPoint(newClipPosition);
            newClipPosition.y = boxr_DragEnd.position.y;
            newClipPosition.x = Mathf.Clamp(newClipPosition.x, boxb_Drag.transform.position.x, boxr_DragEnd.position.x);
            v_Clip02_R.transform.position = newClipPosition;
            if ((boxr_DragEnd.position.x - v_Clip02_R.transform.position.x) < 0.01f)
            {
                LM_Manager.ins.panel_Pause.SetActive(true);
                UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;                
            }
        }
        else if (dragOn && Input.GetMouseButtonUp(0))
        {
            dragOn = false;
        }
    }
    void OnStageStart(GameObject gameObject)
    {
        panel_Tool_Xpos = LM_Manager.ins.container.position.x + 
            (cam.ScreenToWorldPoint(new Vector3(0, 0, 10)).x - LM_Manager.ins.panel_Tool.transform.position.x);
        LM_Manager.ins.MoveContainerHorizontally(panel_Tool_Xpos);
        LM_Manager.ins.BoxTweenScale(boxb_Razer);
        UIEventListener.Get(boxb_Razer).onClick = OnBoxbRazerClick;
    }
    void OnBoxbRazerClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        razer_Selected.SetActive(true);
        panel_Timeline_Xpos = LM_Manager.ins.container.position.x +
            (cam.ScreenToWorldPoint(new Vector3(0, 0, 10)).x - edit_Line.transform.position.x);
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(panel_Timeline_Xpos);
        tp.AddOnFinished(() =>
        {
            guideline.SetActive(true);
            LM_Manager.ins.BoxTweenScale(boxb_Guideline);
            UIEventListener.Get(boxb_Guideline).onClick = OnBoxbGuidelineClick;
        });
    }
    void OnBoxbGuidelineClick(GameObject gameObject)
    {
        guideline.SetActive(false);
        v_Clip01.SetActive(false);
        v_Clip02_L.SetActive(true);
        v_Clip02_R.SetActive(true);
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
        {
            TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(panel_Tool_Xpos);
            tp.SetOnFinished(() =>
            {
                LM_Manager.ins.BoxTweenScale(boxb_Select);
                UIEventListener.Get(boxb_Select).onClick = OnBoxbSelectClick;
            });
        };
    }
    void OnBoxbSelectClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        razer_Selected.SetActive(false);
        sprite_Selected.gameObject.SetActive(true);
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(panel_Timeline_Xpos);
        tp.SetOnFinished(() =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_Drag);
            LM_Manager.ins.BoxTweenScale(boxr_DragEnd.gameObject);
        });
    }
}
