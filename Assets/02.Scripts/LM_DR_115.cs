﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_DR_115 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public UISprite step01_spriteNoJobs_text;
    public GameObject step01_box1;
    public UISprite step01_spriteYoutube_bg;
    public GameObject step01_box2;
    public UISprite step01_spriteFilenameBox1;
    public UISprite step01_spriteFilenameBox2;
    public GameObject step01_labelFilename;
    public GameObject step01_box3;
    public UISprite step01_spriteBrowse_btn;
    public UISprite step01_spritePopupSave;
    public GameObject step01_box4;
    public UISprite step01_spriteSave_btn;
    public UISprite step01_spriteSaveLocation;
    public GameObject step01_box5;
    public UISprite step01_spriteFormatOption;
    public GameObject step01_box6;
    public UISprite step01_spriteMP4;
    public GameObject step01_glitters1;
    public GameObject step01_box7;
    public UISprite step01_spriteAddQueue_btn;
    public UISprite step01_spriteRender1;
    public GameObject step01_box8;
    public UISlider step01_slider1;
    public UISprite step01_spriteRender2;
    public UISlider step01_slider2;
    public UILabel step01_labelTime1;
    public UISprite step01_spriteRemaining;
    public UILabel step01_labelPercent;
    public UILabel step01_labelTime2;
    public GameObject step01_glitters2;
    public UISprite step01_spriteRender3;
    public UISprite step01_spritePopupOutput;
    public GameObject step01_glitters3;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_DR_115_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));
    }

    bool timerFlag = false;
    int dragCount = 0;
    float timer = 0f;
    private void Update()
    {
        if (timerFlag)
        {
            
            timer += Time.deltaTime;

            step01_slider1.value = timer / 3f;
            step01_slider2.value = timer / 3f;

            if(timer > 3f)
            {
                step01_slider1.value = 1f;
                step01_labelTime1.text = "00:00:00";
                step01_labelPercent.text = "100%";
            } else
            {
                step01_labelTime1.text = "00:00:" + (int)(3f - timer);
                if(((int)(3f - timer)) < 10)
                    step01_labelTime1.text = "00:00:0" + (int)(3f - timer);
                step01_labelPercent.text = ((int)(step01_slider1.value * 100f)).ToString() + "%";
            }

            if (timer > 3f)
            {
                timerFlag = false;
                step01_slider2.value = 1f;

                LastStep();
            }
            else
            {
                step01_labelTime2.text = "00:00:" + (int)(3f - timer);
                if (((int)(3f - timer)) < 10)
                    step01_labelTime2.text = "00:00:0" + (int)(3f - timer);
            }
                
        }
    }
    /*
    public UISprite step01_spriteNoJobs_text;
    public GameObject step01_box1;
    public UISprite step01_spriteYoutube_bg;
    public GameObject step01_box2;
    public UISprite step01_spriteFilenameBox1;
    public UISprite step01_spriteFilenameBox2;
    public GameObject step01_labelFilename;
    public GameObject step01_box3;
    public UISprite step01_spriteBrowse_btn;
    public UISprite step01_spritePopupSave;
    public GameObject step01_box4;
    public UISprite step01_spriteSave_btn;
    public UISprite step01_spriteSaveLocation;
    public GameObject step01_box5;
    public UISprite step01_spriteFormatOption;
    public GameObject step01_box6;
    public UISprite step01_spriteMP4;
    public GameObject step01_glitters1;
    public GameObject step01_box7;
    public UISprite step01_spriteRender1;
    public GameObject step01_box8;
    public UISlider step01_slider1;
    public UISprite step01_spriteRender2;
    public UISlider step01_slider2;
    public UILabel step01_labelTIme1;
    public UISprite step01_spriteRemaining;
    public UILabel step01_labelPercent;
    public UILabel step01_labelTime2;
    public GameObject step01_glitters2;
    public UISprite step01_spriteRender3;
    public UISprite step01_spritePopupOutput;
    public GameObject step01_glitters3;
    */
    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteNoJobs_text);
        lmSpriteList.Add(step01_spriteYoutube_bg);
        lmSpriteList.Add(step01_spriteFilenameBox1);
        lmSpriteList.Add(step01_spriteFilenameBox2);
        lmSpriteList.Add(step01_spriteBrowse_btn);
        lmSpriteList.Add(step01_spritePopupSave);
        lmSpriteList.Add(step01_spriteSave_btn);
        lmSpriteList.Add(step01_spriteSaveLocation);
        lmSpriteList.Add(step01_spriteFormatOption);
        lmSpriteList.Add(step01_spriteMP4);
        lmSpriteList.Add(step01_spriteAddQueue_btn);
        lmSpriteList.Add(step01_spriteRender1);
        lmSpriteList.Add(step01_spriteRender2);
        lmSpriteList.Add(step01_spriteRemaining);
        lmSpriteList.Add(step01_spriteRender3);
        lmSpriteList.Add(step01_spritePopupOutput);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteNoJobs_text.spriteName = "DR115_0018_No-jobs-in-queue";
        step01_spriteYoutube_bg.spriteName = "DR115_0017_UTUBE-Basic";
        step01_spriteFilenameBox1.spriteName = "DR115_0016_Untitled";
        step01_spriteFilenameBox2.spriteName = "DR115_0015_Empted-Box";
        step01_spriteBrowse_btn.spriteName = "DR115_0014_Brows-Button";
        step01_spritePopupSave.spriteName = "DR115_0013_Save-Popup";
        step01_spriteSave_btn.spriteName = "DR115_0012_Save-Button";
        step01_spriteSaveLocation.spriteName = "DR115_0011_Saved-Place";
        step01_spriteFormatOption.spriteName = "DR115_0010_Format-Option";
        step01_spriteMP4.spriteName = "DR115_0008_mp4-2";
        step01_spriteAddQueue_btn.spriteName = "DR115_0007_Add-to-Render-Queue-Button";
        step01_spriteRender1.spriteName = "DR115_0006_Before-Render-";
        step01_spriteRender2.spriteName = "DR115_0002_Stop+Progress";
        step01_spriteRemaining.spriteName = "DR115_0003_remaining";
        step01_spriteRender3.spriteName = "DR115_0001_End-Rendering";
        step01_spritePopupOutput.spriteName = "DR115_0000_Output-Folder";

    }
    /*
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

        }));

    */
    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);

        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            step01_box1.SetActive(false);

            step01_spriteYoutube_bg.gameObject.SetActive(true);
            step01_box2.SetActive(true);
            step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                step01_box2.SetActive(false);

                step01_spriteFilenameBox1.gameObject.SetActive(true);
                step01_spriteFilenameBox1.GetComponent<TweenAlpha>().onFinished.Clear();
                step01_spriteFilenameBox1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                    step01_spriteFilenameBox1.gameObject.SetActive(false);

                    step01_spriteFilenameBox2.gameObject.SetActive(true);
                    step01_labelFilename.GetComponent<TypewriterEffect>().onFinished.Clear();
                    step01_labelFilename.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate {
                        step01_box3.SetActive(true);
                        step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step01_box3.SetActive(false);

                            step01_spriteBrowse_btn.gameObject.SetActive(true);
                            step01_spriteBrowse_btn.GetComponent<TweenAlpha>().onFinished.Clear();
                            step01_spriteBrowse_btn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {

                                CameraManager.ins.MoveCamera(step01_spritePopupSave.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                    step01_spritePopupSave.gameObject.SetActive(true);
                                    step01_spritePopupSave.GetComponent<TweenScale>().onFinished.Clear();
                                    step01_spritePopupSave.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                                        step01_box4.SetActive(true);
                                        step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                        step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                            step01_box4.SetActive(false);

                                            step01_spriteSave_btn.gameObject.SetActive(true);
                                            step01_spriteSave_btn.GetComponent<TweenAlpha>().onFinished.Clear();
                                            step01_spriteSave_btn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                step01_spritePopupSave.gameObject.SetActive(false);
                                                step01_spriteSave_btn.gameObject.SetActive(false);
                                                step01_spriteBrowse_btn.gameObject.SetActive(false);

                                                step01_spriteSaveLocation.gameObject.SetActive(true);

                                                CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                                    step01_box5.SetActive(true);
                                                    step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                                    step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                        step01_box5.SetActive(false);

                                                        step01_spriteFormatOption.gameObject.SetActive(true);
                                                        step01_spriteFormatOption.GetComponent<TweenAlpha>().onFinished.Clear();
                                                        step01_spriteFormatOption.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                            step01_box6.SetActive(true);
                                                            step01_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                                                            step01_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                step01_box6.SetActive(false);
                                                                step01_spriteFormatOption.gameObject.SetActive(false);

                                                                step01_spriteMP4.gameObject.SetActive(true);
                                                                step01_glitters1.SetActive(true);

                                                                LM_AE_Manager.ins.On_Middle_Guide("LEARNING_DR_115_1", "BOTTOM");

                                                                delay0.gameObject.SetActive(true);
                                                                delay0.GetComponent<TweenAlpha>().onFinished.Clear();
                                                                delay0.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                                    step01_box7.SetActive(true);
                                                                    step01_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                    step01_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                        LM_AE_Manager.ins.Off_Middle_Guide();
                                                                        step01_box7.SetActive(false);

                                                                        step01_spriteAddQueue_btn.gameObject.SetActive(true);
                                                                        step01_spriteAddQueue_btn.GetComponent<TweenAlpha>().onFinished.Clear();
                                                                        step01_spriteAddQueue_btn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                                            step01_spriteNoJobs_text.gameObject.SetActive(false);
                                                                            step01_spriteRender1.gameObject.SetActive(true);

                                                                            CameraManager.ins.MoveCamera(true, false, 1f).onFinished.Add(new EventDelegate(delegate {
                                                                                step01_spriteAddQueue_btn.gameObject.SetActive(false);

                                                                                step01_box8.SetActive(true);
                                                                                step01_box8.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                                step01_box8.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                                    step01_box8.SetActive(false);

                                                                                    step01_spriteRender2.gameObject.SetActive(true);

                                                                                    timerFlag = true;
                                                                                }));
                                                                            }));
                                                                        }));
                                                                    }));
                                                                }));
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
    }

    public void LastStep()
    {
        step01_spriteRender1.gameObject.SetActive(false);
        step01_spriteRender2.gameObject.SetActive(false);

        step01_spriteRender3.gameObject.SetActive(true);
        step01_glitters2.SetActive(true);

        delay1.gameObject.SetActive(true);
        delay1.GetComponent<TweenAlpha>().onFinished.Clear();
        delay1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
            step01_glitters2.SetActive(false);

            CameraManager.ins.MoveCamera(step01_spritePopupOutput.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                step01_spritePopupOutput.gameObject.SetActive(true);
                step01_spritePopupOutput.GetComponent<TweenScale>().onFinished.Clear();
                step01_spritePopupOutput.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                    step01_glitters3.SetActive(true);

                    delay2.gameObject.SetActive(true);
                    delay2.GetComponent<TweenAlpha>().onFinished.Clear();
                    delay2.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                        StartCoroutine(EndDelay());
                    }));
                }));
            }));
        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_DR_115_2");
    }
}
