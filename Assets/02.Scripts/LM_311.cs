﻿using System.Collections.Generic;
using UnityEngine;

public class LM_311 : MonoBehaviour
{
    Camera cam;
    Ray ray;
    RaycastHit hit;
    bool dragLeft;
    bool dragRight;
    bool dragOn;
    UISprite crossDissolveUISprite;
    float offsetX;
    public float speed = 10f;
    Vector3 mouseWorldPos;
    Vector3 clipOriginPos;

    public UISprite panel_Project_Inside;
    public UISprite video_01;
    public UISprite video_02;
    public UISprite panel_Effect;
    public GameObject boxb_VideoTransitions;
    public GameObject panel_Effect_2;
    public GameObject boxb_Dissolve;
    public GameObject panel_Effect_3;
    public GameObject boxb_CrossDissolve;
    public GameObject cross_Dissolve_Short;
    public GameObject cross_Dissolve_Click;
    public GameObject sprite_Touch;
    public GameObject boxb_DragLeft;
    public GameObject boxb_DragRight;

    void Start()
    {
        cam = Camera.main;
        crossDissolveUISprite = cross_Dissolve_Click.GetComponent<UISprite>();
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnStageStart;

        SetSprites();
        LM_Manager.ins.UpdatePanelPosition();
        panel_Project_Inside.UpdateAnchors();
    }
    void SetSprites()
    {
        List<UISprite> lmSpriteList = new List<UISprite>();
        lmSpriteList.Add(panel_Project_Inside);
        lmSpriteList.Add(video_01);
        lmSpriteList.Add(video_02);
        lmSpriteList.Add(panel_Effect);
        lmSpriteList.Add(panel_Effect_2.GetComponent<UISprite>());
        lmSpriteList.Add(panel_Effect_3.GetComponent<UISprite>());
        lmSpriteList.Add(cross_Dissolve_Short.GetComponent<UISprite>());
        lmSpriteList.Add(crossDissolveUISprite);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }
    void OnStageStart(GameObject gameObject)
    {
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x - panel_Effect.transform.position.x);
        tp.SetOnFinished(AfterStart);
    }
    void AfterStart()
    {
        LM_Manager.ins.BoxTweenScale(boxb_VideoTransitions);
        UIEventListener.Get(boxb_VideoTransitions).onClick = OnBoxb_VideoTransitionsClick;
    }
    void OnBoxb_VideoTransitionsClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        panel_Effect_2.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_Dissolve);
        UIEventListener.Get(boxb_Dissolve).onClick = OnBoxb_DissolveClick;
    }
    void OnBoxb_DissolveClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        panel_Effect_2.SetActive(false);
        panel_Effect_3.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_CrossDissolve);
        UIEventListener.Get(boxb_CrossDissolve).onClick = OnBoxb_CrossDissolveClick;
    }
    void OnBoxb_CrossDissolveClick(GameObject gameObject)
    {
        TweenPosition tp =
        LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x - video_02.transform.position.x);
        tp.SetOnFinished(() =>
        {
            cross_Dissolve_Short.SetActive(true);
            TweenPosition.Begin(cross_Dissolve_Short, 1f, video_02.transform.position, true).SetOnFinished(OnCrossDissolveShortMoveFinish);
        });
    }
    void OnCrossDissolveShortMoveFinish()
    {
        cross_Dissolve_Short.SetActive(false);
        cross_Dissolve_Click.SetActive(true);
        sprite_Touch.SetActive(true);
        UIEventListener.Get(cross_Dissolve_Click).onClick = OnCrossDissolveTouch;
    }
    void OnCrossDissolveTouch(GameObject gameObject)
    {
        sprite_Touch.SetActive(false);
        crossDissolveUISprite.spriteName = "cross_dissolve";
        crossDissolveUISprite.alpha = 1;
        gameObject.GetComponent<TweenAlpha>().enabled = false;
        boxb_DragLeft.SetActive(true);
        dragLeft = true;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            LM_Manager.ins.UpdatePanelPosition();
        }
        if (Input.GetMouseButtonDown(0))
        {
            ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "DragStart")
            {
                Debug.Log("drag start");
                dragOn = true;
                if (dragRight)
                {
                    offsetX = hit.point.x - crossDissolveUISprite.transform.position.x;
                }
            }
        }
        else if (dragOn && Input.GetMouseButton(0))
        {
            if (dragLeft)
            {
                mouseWorldPos = Input.mousePosition;
                mouseWorldPos.z = boxb_DragLeft.transform.position.z - cam.transform.position.z;
                offsetX = cam.ScreenToWorldPoint(mouseWorldPos).x - hit.point.x;
                //Debug.Log(offsetX);
                crossDissolveUISprite.width += Mathf.CeilToInt(offsetX * speed);
                //Debug.Log(Mathf.CeilToInt(offsetX * speed));
                crossDissolveUISprite.width = Mathf.Clamp(crossDissolveUISprite.width, 200, 293);
                if(crossDissolveUISprite.width <= 200)
                {
                    Debug.Log("minWidth");
                    dragLeft = false;
                    boxb_DragLeft.SetActive(false);

                    LM_Manager.ins.BoxTweenScale(boxb_DragRight);
                    dragRight = true;
                    dragOn = false;
                    crossDissolveUISprite.pivot = UIWidget.Pivot.Left;
                    clipOriginPos = crossDissolveUISprite.transform.position;
                    return;
                }
            }
            if (dragRight)
            {
                mouseWorldPos = Input.mousePosition;
                mouseWorldPos.z = crossDissolveUISprite.transform.position.z - cam.transform.position.z;
                mouseWorldPos = cam.ScreenToWorldPoint(mouseWorldPos);
                mouseWorldPos.y = crossDissolveUISprite.transform.position.y;
                mouseWorldPos.x -= offsetX;
                mouseWorldPos.x = Mathf.Clamp(mouseWorldPos.x, clipOriginPos.x, video_02.transform.position.x);
                crossDissolveUISprite.transform.position = mouseWorldPos;
                if ((video_02.transform.position.x - crossDissolveUISprite.transform.position.x) < 0.005f)
                {
                    Debug.Log("Complete");
                    LM_Manager.ins.panel_Pause.SetActive(true);
                    UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
                    dragRight = false;
                    return;
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            dragOn = false;
        }
    }
    //IEnumerator Co_SetSize(float offset)
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    crossDissolveUISprite.width += Mathf.CeilToInt(offsetX * speed);
    //}
}
