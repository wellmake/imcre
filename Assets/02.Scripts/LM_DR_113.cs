﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_DR_113 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_box1;
    public UISprite step01_spriteTitles_btn;
    public GameObject step01_cameraPoint;
    public UISprite step01_spriteTitlesMenu;
    public UISprite step01_spriteText;
    public UISprite step01_spriteText_dragable;
    public GameObject step01_box2;
    public GameObject step01_box3;
    public GameObject step01_box3_dragPointer;
    public GameObject step01_dragEnd1;
    public GameObject step01_groupDragResult;
    public UISprite step01_spriteTimelineItem;
    public UISprite step01_spriteVideoAppearence;
    public UISprite step01_spriteTextClip;
    public UISprite step01_spriteVideoTitle;
    public UISprite step01_spriteVideoBG;
    public UISprite step01_spriteVideoBG_right;
    public UISprite step01_spriteTextOption_top;
    public UISprite step01_spriteInvisibleText;
    public UISprite step01_spriteTitle_txt;
    public GameObject step01_glitters1;
    public GameObject step01_glitters2;
    public GameObject step01_panel_right;
    public UISlider step01_scroll;
    public UISprite step01_spriteTextOption_bg;
    public UISprite step01_spriteTextOption_title;
    public GameObject step01_box3_1;
    public UISprite step01_spriteTextOption_title_selected;
    public GameObject step01_labelTextOption_title;
    public GameObject step01_box4;
    public UISprite step01_spriteNameBox1;
    public GameObject step01_labelName1;
    public GameObject step01_box5;
    public GameObject step01_box6;
    public GameObject step01_box6_dragPointer;
    public GameObject step01_dragEnd2;
    public GameObject step01_box7;
    public GameObject step01_box10;
    public UISprite step01_spriteNameBox2;
    public GameObject step01_labelName2;
    public UISprite step01_spriteSizeStrock;
    public UISprite step01_spriteSampleVideo1;
    public UISprite step01_spritePopupColorBox;
    public GameObject step01_box8;
    public UISprite step01_spriteColorPoint;
    public UISprite step01_spriteSelectedColor1;
    public UISprite step01_spriteSelectedColor2;
    public UISprite step01_spriteSelectedColor3;
    public GameObject step01_box9;
    public UISprite step01_spriteSampleVideo2;
    public GameObject step01_glitters3;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_DR_113_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }
    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteTitles_btn);
        lmSpriteList.Add(step01_spriteTitlesMenu);
        lmSpriteList.Add(step01_spriteText);
        lmSpriteList.Add(step01_spriteText_dragable);
        lmSpriteList.Add(step01_spriteTimelineItem);
        lmSpriteList.Add(step01_spriteVideoAppearence);
        lmSpriteList.Add(step01_spriteTextClip);
        lmSpriteList.Add(step01_spriteVideoTitle);
        lmSpriteList.Add(step01_spriteVideoBG);
        lmSpriteList.Add(step01_spriteVideoBG_right);
        lmSpriteList.Add(step01_spriteTextOption_top);
        lmSpriteList.Add(step01_spriteInvisibleText);
        lmSpriteList.Add(step01_spriteTitle_txt);
        lmSpriteList.Add(step01_spriteTextOption_bg);
        lmSpriteList.Add(step01_spriteTextOption_title);
        lmSpriteList.Add(step01_spriteTextOption_title_selected);
        lmSpriteList.Add(step01_spriteNameBox1);
        lmSpriteList.Add(step01_spriteNameBox2);
        lmSpriteList.Add(step01_spriteSizeStrock);
        lmSpriteList.Add(step01_spriteSampleVideo1);
        lmSpriteList.Add(step01_spritePopupColorBox);
        lmSpriteList.Add(step01_spriteColorPoint);
        lmSpriteList.Add(step01_spriteSelectedColor1);
        lmSpriteList.Add(step01_spriteSelectedColor2);
        lmSpriteList.Add(step01_spriteSelectedColor3);
        lmSpriteList.Add(step01_spriteSampleVideo2);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteTitles_btn.spriteName = "113_0026_Tiltles";
        step01_spriteTitlesMenu.spriteName = "113_0024_Titles-Menu";
        step01_spriteText.spriteName = "113_0023_Text";
        step01_spriteText_dragable.spriteName = "113_0023_Text";
        step01_spriteTimelineItem.spriteName = "DR_113_0027_Video-box";
        step01_spriteVideoAppearence.spriteName = "113_0019_Video-appearence";
        step01_spriteTextClip.spriteName = "113_0021_Text-Clip";
        step01_spriteVideoTitle.spriteName = "113_0018_Video-1";
        step01_spriteVideoBG.spriteName = "113_0016_Video-3";
        step01_spriteVideoBG_right.spriteName = "113_0015_Video-4";
        step01_spriteTextOption_top.spriteName = "113_0013_Text-Option-Top";
        step01_spriteInvisibleText.spriteName = "DR_113_0027_Invisible-(text)";
        step01_spriteTitle_txt.spriteName = "113_0017_Video-2";
        step01_spriteTextOption_bg.spriteName = "113_0012_Text-Option-Basic";
        step01_spriteTextOption_title.spriteName = "113_0011_Tilte";
        step01_spriteTextOption_title_selected.spriteName = "113_0010_Title-Choosing";
        step01_spriteNameBox1.spriteName = "113_0009_Active-Box";
        step01_spriteNameBox2.spriteName = "113_0009_Active-Box";
        step01_spriteSizeStrock.spriteName = "113_0001_Strock-Size";
        step01_spriteSampleVideo1.spriteName = "DR_113_0027_Sample-Video-basic";
        step01_spritePopupColorBox.spriteName = "113_0008_Color-Box";
        step01_spriteColorPoint.spriteName = "113_0004_choosing-Color";
        step01_spriteSelectedColor1.spriteName = "113_0006_Color-Box-BLUE-2";
        step01_spriteSelectedColor2.spriteName = "113_0007_Color-Box-BLUE-1";
        step01_spriteSelectedColor3.spriteName = "113_0002_Blue";
        step01_spriteSampleVideo2.spriteName = "113_0000_Completed-TEXT";

    }
    /*
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

        }));

    */
    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);

        step01_panel_right.GetComponent<UIScrollView>().enabled = false;
        step01_scroll.GetComponent<BoxCollider>().enabled = false;
        step01_scroll.transform.Find("Thumb").GetComponent<BoxCollider>().enabled = false;

        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            step01_box1.SetActive(false);
            step01_spriteTitlesMenu.gameObject.SetActive(true);

            step01_spriteTitles_btn.gameObject.SetActive(true);
            step01_spriteTitles_btn.GetComponent<TweenAlpha>().onFinished.Clear();
            step01_spriteTitles_btn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                
                CameraManager.ins.MoveCamera(step01_cameraPoint.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                    step01_box2.SetActive(true);
                    step01_box3.SetActive(true);
                    step01_box3_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();
                    step01_spriteText.gameObject.SetActive(true);
                    step01_spriteText_dragable.gameObject.SetActive(true);
                    step01_spriteText_dragable.transform.localPosition = step01_spriteText.transform.localPosition;

                    step01_spriteText_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                        step01_spriteText_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                        CameraManager.SCREEN_MOVE_PAUSE = true;
                        step01_spriteText_dragable.spriteName = "113_0021_Text-Clip";
                        step01_spriteText_dragable.SetDimensions(step01_spriteTextClip.width, step01_spriteTextClip.height);
                    }));
                    step01_spriteText_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                        CameraManager.SCREEN_MOVE_PAUSE = false;
                        step01_spriteText_dragable.spriteName = "113_0023_Text";
                        step01_spriteText_dragable.SetDimensions(step01_spriteText.width, step01_spriteText.height);

                        if(step01_spriteText_dragable.transform.position.x <= step01_dragEnd1.transform.position.x + 0.1f && step01_spriteText_dragable.transform.position.x >= step01_dragEnd1.transform.position.x - 0.1f)
                        {
                            step01_box2.SetActive(false);
                            step01_box3.SetActive(false);

                            step01_spriteText_dragable.gameObject.SetActive(false);
                            step01_groupDragResult.SetActive(true);

                            delay0.gameObject.SetActive(true);
                            delay0.onFinished.Clear();
                            delay0.onFinished.Add(new EventDelegate(delegate {
                                CameraManager.ins.MoveCamera(true, false, 1.0f).onFinished.Add(new EventDelegate(delegate {
                                    step01_glitters1.SetActive(false);
                                    step01_glitters2.SetActive(false);

                                    LM_AE_Manager.ins.On_Middle_Guide("LEARNING_DR_113_1", "BOTTOM");
                                    step01_box3_1.SetActive(true);
                                    step01_box3_1.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box3_1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                        LM_AE_Manager.ins.Off_Middle_Guide();
                                        step01_box3_1.SetActive(false);

                                        step01_spriteTextOption_title.gameObject.SetActive(false);
                                        step01_spriteTextOption_title_selected.gameObject.SetActive(true);
                                        step01_spriteTextOption_title_selected.GetComponent<TweenAlpha>().onFinished.Clear();
                                        step01_spriteTextOption_title_selected.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                            step01_spriteTextOption_title_selected.gameObject.SetActive(false);
                                            step01_labelTextOption_title.gameObject.SetActive(true);

                                            step01_spriteTitle_txt.gameObject.SetActive(false);
                                            step01_spriteSampleVideo1.gameObject.SetActive(true);
                                            step01_labelTextOption_title.GetComponent<TypewriterEffect>().onFinished.Clear();
                                            step01_labelTextOption_title.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate {
                                                step01_box4.SetActive(true);
                                                step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                                step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                    step01_box4.SetActive(false);
                                                    step01_spriteNameBox1.gameObject.SetActive(true);
                                                    step01_labelName1.GetComponent<TypewriterEffect>().onFinished.Clear();
                                                    step01_labelName1.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate {
                                                        step01_spriteSampleVideo1.GetComponent<TweenScale>().enabled = true;

                                                        step01_box5.SetActive(true);
                                                        step01_box6.SetActive(true);
                                                        step01_box6_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

                                                        step01_panel_right.GetComponent<UIScrollView>().enabled = true;
                                                        step01_scroll.GetComponent<BoxCollider>().enabled = true;
                                                        step01_scroll.transform.Find("Thumb").GetComponent<BoxCollider>().enabled = true;

                                                        step01_scroll.onChange.Add(new EventDelegate(delegate {
                                                            if (step01_scroll.value > 0.98f)
                                                            {
                                                                step01_panel_right.GetComponent<UIScrollView>().enabled = false;
                                                                step01_scroll.GetComponent<BoxCollider>().enabled = false;
                                                                step01_scroll.transform.Find("Thumb").GetComponent<BoxCollider>().enabled = false;

                                                                step01_box5.SetActive(false);
                                                                step01_box6.SetActive(false);
                                                                step01_scroll.value = 1f;

                                                                step01_box7.SetActive(true);
                                                                step01_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                step01_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                    step01_box7.SetActive(false);

                                                                    CameraManager.ins.MoveCamera(step01_spritePopupColorBox.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                                                        step01_spritePopupColorBox.gameObject.SetActive(true);
                                                                        step01_spritePopupColorBox.GetComponent<TweenScale>().onFinished.Clear();
                                                                        step01_spritePopupColorBox.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                                                                            step01_box8.SetActive(true);
                                                                            step01_box8.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                            step01_box8.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                                step01_box8.SetActive(false);

                                                                                step01_spriteColorPoint.gameObject.SetActive(true);
                                                                                step01_spriteSelectedColor1.gameObject.SetActive(true);
                                                                                step01_spriteSelectedColor2.gameObject.SetActive(true);
                                                                                step01_spriteSelectedColor3.gameObject.SetActive(true);

                                                                                step01_box9.SetActive(true);
                                                                                step01_box9.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                                step01_box9.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                                    step01_box9.SetActive(false);
                                                                                    step01_spritePopupColorBox.gameObject.SetActive(false);
                                                                                    step01_spriteSampleVideo1.gameObject.SetActive(false);
                                                                                    step01_spriteSampleVideo2.gameObject.SetActive(true);

                                                                                    delay1.gameObject.SetActive(true);
                                                                                    delay1.onFinished.Clear();
                                                                                    delay1.onFinished.Add(new EventDelegate(delegate {
                                                                                        CameraManager.ins.MoveCamera(true, false, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                                                                            step01_box10.SetActive(true);
                                                                                            step01_box10.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                                            step01_box10.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                                                step01_box10.SetActive(false);

                                                                                                step01_spriteSizeStrock.gameObject.SetActive(true);
                                                                                                step01_spriteNameBox2.gameObject.SetActive(true);
                                                                                                step01_labelName2.GetComponent<TypewriterEffect>().onFinished.Clear();
                                                                                                step01_labelName2.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate {
                                                                                                    delay2.gameObject.SetActive(true);
                                                                                                    delay2.onFinished.Clear();
                                                                                                    delay2.onFinished.Add(new EventDelegate(delegate {
                                                                                                        CameraManager.ins.MoveCamera(step01_spriteSampleVideo2.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                                                                                            delay3.gameObject.SetActive(true);
                                                                                                            delay3.onFinished.Clear();
                                                                                                            delay3.onFinished.Add(new EventDelegate(delegate {
                                                                                                                StartCoroutine(EndDelay());
                                                                                                            }));
                                                                                                        }));
                                                                                                    }));
                                                                                                }));
                                                                                            }));
                                                                                        }));
                                                                                    }));
                                                                                }));
                                                                            }));
                                                                        }));
                                                                    }));
                                                                }));
                                                            }
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        } else
                        {
                            step01_spriteText_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                            step01_spriteText_dragable.transform.localPosition = step01_spriteText.transform.localPosition;
                        }
                    }));
                }));
            }));
        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_DR_113_2");
    }
}
