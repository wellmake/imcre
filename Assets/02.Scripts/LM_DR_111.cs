﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_DR_111 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_groupP1;
    public UISprite step01_spriteBG_Base;
    public UISprite step01_spriteBG_2;
    public UISprite step01_spriteBG_3;
    public UISprite step01_spriteBG_8;
    public UISprite step01_spriteBG_Title;

    public GameObject step01_box1;

    public GameObject step01_groupP2;
    public UISprite step01_spriteBG_1;
    public UISprite step01_spriteBG_4;
    public UISprite step01_spriteBG_2_1;
    public UISprite step01_spriteDB_btn;

    public GameObject step01_box2;
    public UISprite step01_spritePopupNewDB;
    public GameObject step01_box3;
    public UISprite step01_spriteNameBox1;
    public UILabel step01_labelName1;
    public GameObject step01_box4;
    public GameObject step01_box5;
    public UISprite step01_spritePopupLocation;
    public GameObject step01_box6;
    public UISprite step01_spriteSampleFolder;
    public GameObject step01_box7;
    public UISprite step01_spriteSelectFolder;
    public UISprite step01_spriteNameBox2;
    public UILabel step01_labelName2;
    public GameObject step01_box8;
    public GameObject step01_glitters;
    public UISprite step01_spriteBG_1_2;
    public GameObject step01_box9;
    public UISprite step01_spritePopupNewProject;
    public GameObject step01_box10;
    public UISprite step01_spriteNameBox3;
    public UILabel step01_labelName3;
    public GameObject step01_box11;
    public GameObject step01_cameraPoint;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_DR_111_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }
    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteBG_Base);
        lmSpriteList.Add(step01_spriteBG_2);
        lmSpriteList.Add(step01_spriteBG_3);
        lmSpriteList.Add(step01_spriteBG_8);
        lmSpriteList.Add(step01_spriteBG_Title);
        lmSpriteList.Add(step01_spriteBG_1);
        lmSpriteList.Add(step01_spriteBG_4);
        lmSpriteList.Add(step01_spriteBG_2_1);
        lmSpriteList.Add(step01_spriteDB_btn);
        lmSpriteList.Add(step01_spritePopupNewDB);
        lmSpriteList.Add(step01_spriteNameBox1);
        lmSpriteList.Add(step01_spritePopupLocation);
        lmSpriteList.Add(step01_spriteSampleFolder);
        lmSpriteList.Add(step01_spriteSelectFolder);
        lmSpriteList.Add(step01_spriteNameBox2);
        lmSpriteList.Add(step01_spriteBG_1_2);
        lmSpriteList.Add(step01_spritePopupNewProject);
        lmSpriteList.Add(step01_spriteNameBox3);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteBG_Base.spriteName = "111_BG-9";
        step01_spriteBG_2.spriteName = "111_BG-2";
        step01_spriteBG_3.spriteName = "111_BG-3";
        step01_spriteBG_8.spriteName = "111_BG-8";
        step01_spriteBG_Title.spriteName = "111_Davinci-Resolve-TEXT";
        step01_spriteBG_1.spriteName = "111_BG-1";
        step01_spriteBG_4.spriteName = "111_BG-4";
        step01_spriteBG_2_1.spriteName = "111_BG-2";
        step01_spriteDB_btn.spriteName = "111_New-Database-Button";
        step01_spritePopupNewDB.spriteName = "111_New-Database-Popup-1";
        step01_spriteNameBox1.spriteName = "111_Name-Box-1";
        step01_spritePopupLocation.spriteName = "111_Location-Popup";
        step01_spriteSampleFolder.spriteName = "111_Sample-Folder";
        step01_spriteSelectFolder.spriteName = "111_Folder-choosing-Button";
        step01_spriteNameBox2.spriteName = "111_Name-Box-2";
        step01_spriteBG_1_2.spriteName = "111_BG-1_2";
        step01_spritePopupNewProject.spriteName = "111_Creat-New-Project-Popup";
        step01_spriteNameBox3.spriteName = "111_Name-Box-2";

    }
    /*
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

        }));

    */
    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);

        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
        {
            step01_box1.SetActive(false);

            step01_groupP2.SetActive(true);

            step01_box2.SetActive(true);
            step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
            {
                step01_box2.SetActive(false);

                CameraManager.ins.MoveCamera(step01_spritePopupNewDB.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
                {

                    step01_spritePopupNewDB.gameObject.SetActive(true);
                    step01_spritePopupNewDB.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                    {
                        step01_box3.SetActive(true);
                        step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                        {
                            step01_box3.SetActive(false);

                            step01_spritePopupNewDB.spriteName = "111_New-Database-Popup-2";

                            step01_spriteNameBox1.gameObject.SetActive(true);
                            step01_box4.SetActive(true);
                            step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                            {
                                step01_box4.SetActive(false);

                                step01_labelName1.gameObject.SetActive(true);
                                step01_labelName1.GetComponent<TypewriterEffect>().onFinished.Clear();
                                step01_labelName1.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate
                                {
                                    step01_box5.SetActive(true);
                                    step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                    {
                                        step01_box5.SetActive(false);

                                        step01_spritePopupLocation.gameObject.SetActive(true);
                                        step01_spritePopupLocation.GetComponent<TweenScale>().onFinished.Clear();
                                        step01_spritePopupLocation.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                                        {
                                            step01_box6.SetActive(true);
                                            step01_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                                            step01_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                            {
                                                step01_box6.SetActive(false);

                                                step01_spriteSampleFolder.gameObject.SetActive(true);
                                                step01_box7.SetActive(true);
                                                step01_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                                                step01_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                {
                                                    step01_box7.SetActive(false);

                                                    step01_spriteSelectFolder.gameObject.SetActive(true);
                                                    step01_spriteSelectFolder.GetComponent<TweenAlpha>().onFinished.Clear();
                                                    step01_spriteSelectFolder.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                                                    {
                                                        step01_spritePopupLocation.gameObject.SetActive(false);
                                                        step01_spriteSampleFolder.gameObject.SetActive(false);
                                                        step01_spriteSelectFolder.gameObject.SetActive(false);

                                                        step01_spriteNameBox2.gameObject.SetActive(true);


                                                        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_DR_111_1", "BOTTOM");
                                                        step01_box8.SetActive(true);
                                                        step01_box8.GetComponent<UIEventTrigger>().onClick.Clear();
                                                        step01_box8.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                        {
                                                            LM_AE_Manager.ins.Off_Middle_Guide();
                                                            step01_box8.SetActive(false);

                                                            step01_spritePopupNewDB.gameObject.SetActive(false);
                                                            step01_spriteNameBox1.gameObject.SetActive(false);
                                                            step01_spriteNameBox2.gameObject.SetActive(false);

                                                            step01_glitters.SetActive(true);
                                                            step01_spriteBG_1_2.gameObject.SetActive(true);

                                                            delay0.gameObject.SetActive(true);
                                                            delay0.onFinished.Clear();
                                                            delay0.onFinished.Add(new EventDelegate(delegate
                                                            {
                                                                CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate
                                                                {
                                                                    delay3.gameObject.SetActive(true);
                                                                    delay3.onFinished.Clear();
                                                                    delay3.onFinished.Add(new EventDelegate(delegate
                                                                    {
                                                                        CameraManager.ins.MoveCamera(step01_cameraPoint.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate
                                                                        {
                                                                            step01_glitters.SetActive(false);
                                                                            step01_box9.SetActive(true);
                                                                            step01_box9.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                            step01_box9.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                                            {
                                                                                step01_box9.SetActive(false);

                                                                                CameraManager.ins.MoveCamera(step01_spritePopupNewProject.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate
                                                                                {
                                                                                    step01_spritePopupNewProject.gameObject.SetActive(true);
                                                                                    step01_spritePopupNewProject.GetComponent<TweenScale>().onFinished.Clear();
                                                                                    step01_spritePopupNewProject.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                                                                                    {
                                                                                        step01_box10.SetActive(true);
                                                                                        step01_box10.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                                        step01_box10.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                                                        {
                                                                                            step01_box10.SetActive(false);

                                                                                            step01_spriteNameBox3.gameObject.SetActive(true);
                                                                                            step01_labelName3.GetComponent<TypewriterEffect>().onFinished.Clear();
                                                                                            step01_labelName3.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate
                                                                                            {
                                                                                                step01_box11.SetActive(true);
                                                                                                step01_box11.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                                                step01_box11.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                                                                                                {
                                                                                                    step01_box11.SetActive(false);

                                                                                                    step01.SetActive(false);

                                                                                                    delay1.gameObject.SetActive(true);
                                                                                                    delay1.onFinished.Clear();
                                                                                                    delay1.onFinished.Add(new EventDelegate(delegate
                                                                                                    {
                                                                                                        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate
                                                                                                        {
                                                                                                            delay4.gameObject.SetActive(true);
                                                                                                            delay4.onFinished.Clear();
                                                                                                            delay4.onFinished.Add(new EventDelegate(delegate
                                                                                                            {
                                                                                                                CameraManager.ins.MoveCamera(true, false, 2f).onFinished.Add(new EventDelegate(delegate
                                                                                                                {
                                                                                                                    delay2.gameObject.SetActive(true);
                                                                                                                    delay2.onFinished.Clear();
                                                                                                                    delay2.onFinished.Add(new EventDelegate(delegate
                                                                                                                    {
                                                                                                                        StartCoroutine(EndDelay());
                                                                                                                    }));
                                                                                                                }));
                                                                                                            }));
                                                                                                            
                                                                                                        }));
                                                                                                    }));
                                                                                                }));
                                                                                            }));
                                                                                        }));
                                                                                    }));
                                                                                }));

                                                                            }));
                                                                        }));
                                                                    }));
                                                                }));
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));


    }


    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_DR_111_2");
    }
}
