﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LM_252 : MonoBehaviour
{
    Camera cam;
    float newX;
    float text_X;
    float properties_X;

    [Header("Step01")]
    public GameObject step01;
    public GameObject boxb_File;
    public GameObject menu_File;
    public GameObject boxb_New;
    public GameObject menu_New;
    public GameObject boxb_LegacyTitle;
    public GameObject window_New_Title;
    public GameObject boxb_NewTitle;
    public GameObject newTitle_Hello;
    public GameObject OK_Button;
    public GameObject boxb_OK_Button;

    [Header("Step02")]
    public GameObject step02;
    public GameObject boxb_T;
    public GameObject textline;
    public GameObject boxb_Textline;
    public GameObject touch_Textline;
    public UISprite hello_text;
    public GameObject boxb_Color;
    public GameObject window_ColorPicker;
    public GameObject boxb_ColorPicker;
    public GameObject boxb_ColorPicker_OK;
    public GameObject Color_Yellow;
    public GameObject boxb_Add;
    public GameObject properties_Stroke;
    public UISprite stroke_Red;
    public GameObject hello_stroke;
    public GameObject boxb_Shadow;
    public GameObject properties_Shadow;
    public UISprite shadow_White;
    public GameObject hellow_shadow;

    [Header("SetSprite var")]
    public UISprite legacyTitle;
    public UISprite properties;
    void Start()
    {
        cam = Camera.main;
        //step01.SetActive(true);
        //step02.SetActive(false);
        UIEventListener.Get(boxb_File).onClick = OnBoxbFileClick;
        menu_New.SetActive(false);

        // For Test
        //UIEventListener.Get(boxb_T).onClick = OnBoxb_T_Click;

        SetSprites();
    }
    void SetSprites()
    {
        List<UISprite> lmSpriteList = new List<UISprite>();
        lmSpriteList.Add(menu_File.GetComponent<UISprite>());
        lmSpriteList.Add(menu_New.GetComponent<UISprite>());
        lmSpriteList.Add(window_New_Title.GetComponent<UISprite>());
        lmSpriteList.Add(newTitle_Hello.GetComponent<UISprite>());
        lmSpriteList.Add(OK_Button.GetComponent<UISprite>());

        lmSpriteList.Add(textline.GetComponent<UISprite>());
        lmSpriteList.Add(hello_text);
        lmSpriteList.Add(window_ColorPicker.GetComponent<UISprite>());
        lmSpriteList.Add(Color_Yellow.GetComponent<UISprite>());
        lmSpriteList.Add(properties_Stroke.GetComponent<UISprite>());
        lmSpriteList.Add(stroke_Red);
        lmSpriteList.Add(hello_stroke.GetComponent<UISprite>());
        lmSpriteList.Add(properties_Shadow.GetComponent<UISprite>());
        lmSpriteList.Add(shadow_White);
        lmSpriteList.Add(hellow_shadow.GetComponent<UISprite>());

        lmSpriteList.Add(legacyTitle);
        lmSpriteList.Add(properties);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }
    void OnBoxbFileClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        menu_File.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_New);
        UIEventListener.Get(boxb_New).onClick = OnBoxbNewClick;
    }
    void OnBoxbNewClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        menu_New.SetActive(true);
        newX = menu_File.transform.position.x - menu_New.transform.position.x;
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(LM_Manager.ins.container.position.x + newX);
        tp.AddOnFinished(() =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_LegacyTitle);
            UIEventListener.Get(boxb_LegacyTitle).onClick = OnBoxbLegacyTitleClick;
        });
    }
    void OnBoxbLegacyTitleClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        menu_File.SetActive(false);
        menu_New.SetActive(false);
        window_New_Title.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_NewTitle);
        UIEventListener.Get(boxb_NewTitle).onClick = OnBoxbNewTitleClick;
    }
    void OnBoxbNewTitleClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        LM_Manager.ins.BoxTweenScale(newTitle_Hello);
        OK_Button.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_OK_Button);
        UIEventListener.Get(boxb_OK_Button).onClick = OnBoxbOKButtonClick;
    }
    void OnBoxbOKButtonClick(GameObject gameObject)
    {
        step01.SetActive(false);
        LM_Manager.ins.container.position = LM_Manager.ins.containerOriginPos;
        step02.SetActive(true);
        LM_Manager.ins.BoxTweenScale(boxb_T);
        UIEventListener.Get(boxb_T).onClick = OnBoxb_T_Click;
    }
    void OnBoxb_T_Click(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        textline.SetActive(true);
        touch_Textline.SetActive(true);
        boxb_Textline.GetComponent<UISprite>().alpha = 1f;
        text_X = LM_Manager.ins.container.position.x + (cam.ScreenToWorldPoint(new Vector3(0, 0, 10)).x - boxb_Textline.transform.position.x)*7/8;
        LM_Manager.ins.MoveContainerHorizontally(text_X);
        UIEventListener.Get(boxb_Textline).onClick = OnBoxb_TextlineClick;
    }
    void OnBoxb_TextlineClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        touch_Textline.SetActive(false);
        hello_text.gameObject.SetActive(true);
        hello_text.fillAmount = 0f;
        StartCoroutine(Co_FillHelloText());
    }
    IEnumerator Co_FillHelloText()
    {
        while (hello_text.fillAmount < 1f)
        {
            hello_text.fillAmount += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = (x) =>
        {
            LM_Manager.ins.BoxTweenScale(boxb_Color);
            UIEventListener.Get(boxb_Color).onClick = OnBoxb_ColorClick;
            properties_X = LM_Manager.ins.container.position.x + (hello_text.transform.position.x - properties.transform.position.x);
            LM_Manager.ins.MoveContainerHorizontally(properties_X);
        };
    }
    void OnBoxb_ColorClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        window_ColorPicker.SetActive(true);
        UIEventListener.Get(boxb_ColorPicker).onClick = OnBoxb_ColorPickerClick;
    }
    void OnBoxb_ColorPickerClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        LM_Manager.ins.BoxTweenScale(boxb_ColorPicker_OK);
        UIEventListener.Get(boxb_ColorPicker_OK).onClick = OnBoxb_ColorPicker_OKClick;
    }
    void OnBoxb_ColorPicker_OKClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        window_ColorPicker.SetActive(false);
        LM_Manager.ins.MoveContainerHorizontally(text_X);
        hello_text.color = Color.yellow;
        Color_Yellow.SetActive(true);
        StartCoroutine(Co_WaitMoveContainer(1.5f, properties_X, OnFinish_OKClick));
    }
    IEnumerator Co_WaitMoveContainer(float delay, float newX, EventDelegate.Callback del)
    {
        yield return new WaitForSeconds(delay);
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(newX);
        tp.SetOnFinished(del);
    }
    void OnFinish_OKClick()
    {
        LM_Manager.ins.BoxTweenScale(boxb_Add);
        UIEventListener.Get(boxb_Add).onClick = OnBoxbAddClick;
    }
    void OnBoxbAddClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        properties_Stroke.SetActive(true);
        hello_stroke.SetActive(true);
        LM_Manager.ins.MoveContainerHorizontally(text_X);
        StartCoroutine(Co_WaitMoveContainer(1.5f, properties_X, OnFinish_AddClick));
    }
    void OnFinish_AddClick()
    {
        LM_Manager.ins.BoxTweenScale(boxb_Shadow);
        UIEventListener.Get(boxb_Shadow).onClick = OnBoxbShadowClick;
    }
    void OnBoxbShadowClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        properties_Shadow.SetActive(true);
        hellow_shadow.SetActive(true);
        TweenPosition tp = LM_Manager.ins.MoveContainerHorizontally(text_X);
        tp.SetOnFinished(() =>
        {
            LM_Manager.ins.panel_Pause.SetActive(true);
            UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
        });
    }
}
