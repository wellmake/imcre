﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_241 : MonoBehaviour
{
    Camera cam;
    Transform panel_Timeline;
    Ray ray;
    RaycastHit hit;
    List<UISprite> lmSpriteList;

    [Header("Step01")]
    public GameObject v1_clip_long;
    public GameObject boxb_01;
    public GameObject line01;
    public GameObject line02;
    public GameObject boxb_02;
    public GameObject v1_clip_mid;
    public GameObject v1_clip_last;
    [Header("Step01 Sprites")]
    public UISprite v1_clip_video;
    public UISprite v1_clip_audio;

    [Header("Step02")]
    public GameObject boxb_mid;
    public GameObject Button_delete;
    public GameObject v1_clip_short;
    public GameObject mouse_R;
    public GameObject mouse_rightClick;
    public GameObject ripple_delete;

    [Header("Step03")]
    public GameObject step03;
    public Transform dragItem;
    public Transform dragStartPos;
    public Transform dragEndPos;
    bool isStep03;
    bool dragOn;
    Vector3 newDragItemPosition;
    UISprite dragItemSprite;
    [Header("Step03 Sprites")]
    public UISprite step03_v1_clip_audio;

    void Start()
    {
        cam = Camera.main;
        panel_Timeline = LM_Manager.ins.panel_Timeline.transform;
        dragItemSprite = dragItem.GetComponent<UISprite>();
        LM_Manager.ins.panel_Timeline.width = LM_Manager.ins.titlePanel.width;
        LM_Manager.ins.panel_Timeline.SetDimensions(LM_Manager.ins.titlePanel.width, LM_Manager.ins.panel_Timeline.height);
        LM_Manager.ins.panel_Timeline.ResetAnchors();
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnStart_MoveCamera;

        UIEventListener.Get(boxb_01).onClick = OnBoxb01Click;
        UIEventListener.Get(boxb_02).onClick = OnBoxb02Click;
        UIEventListener.Get(boxb_mid).onClick = OnBoxbMidClick;
        UIEventListener.Get(Button_delete).onClick = OnDeleteClick;
        UIEventListener.Get(mouse_rightClick).onClick = OnMouseRightClick;
        UIEventListener.Get(ripple_delete).onClick = OnRipple_deleteClick;

        SetSprites();
    }
    void SetSprites()
    {
        lmSpriteList = new List<UISprite>();
        lmSpriteList.Add(v1_clip_long.GetComponent<UISprite>());
        lmSpriteList.Add(v1_clip_mid.GetComponent<UISprite>());
        lmSpriteList.Add(v1_clip_video);
        lmSpriteList.Add(v1_clip_audio);

        lmSpriteList.Add(v1_clip_short.GetComponent<UISprite>());
        lmSpriteList.Add(ripple_delete.GetComponent<UISprite>());

        lmSpriteList.Add(dragItemSprite);
        lmSpriteList.Add(step03_v1_clip_audio);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_Manager.ins.lm_Atlas;
        }
    }
    private void OnStart_MoveCamera(GameObject go)
    {
        float newX = LM_Manager.ins.container.position.x + cam.ScreenToWorldPoint(new Vector3(0, 0, 10f)).x - panel_Timeline.position.x;
        TweenPosition tp =
        LM_Manager.ins.MoveContainerHorizontally(newX);
        LM_Manager.ins.UpdatePanelPosition();
        tp.AddOnFinished(OnStep01);
    }
    void OnStep01()
    {
        line01.SetActive(true);
        boxb_01.SetActive(true);
    }
    void OnBoxb01Click(GameObject gameObject)
    {
        line01.SetActive(false);
        line02.SetActive(true);
        v1_clip_mid.SetActive(true);
    }
    void OnBoxb02Click(GameObject gameObject)
    {
        line02.SetActive(false);
        v1_clip_last.SetActive(true);
        StartCoroutine(Wait_Pause(1f));
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnStep02;
    }
    void OnStep02(GameObject gameObject)
    {
        boxb_mid.SetActive(true);
    }
    void OnBoxbMidClick(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        Button_delete.SetActive(true);
    }
    void OnDeleteClick(GameObject gameObject)
    {
        Button_delete.SetActive(false);
        v1_clip_mid.SetActive(false);
        v1_clip_long.SetActive(false);
        v1_clip_short.SetActive(true);
        boxb_mid.SetActive(true);
        TweenScale.Begin(boxb_mid, 0.5f, Vector3.one).from = Vector3.zero;
        UIEventListener.Get(boxb_mid).onClick = OnBoxbMidClick_2;
    }
    void OnBoxbMidClick_2(GameObject gameObject)
    {
        LM_Manager.ins.DeActivateCurrentGuideBlue();
        mouse_R.SetActive(true);
    }
    void OnMouseRightClick(GameObject gameObject)
    {
        ripple_delete.SetActive(true);
    }
    void OnRipple_deleteClick(GameObject gameObject)
    {
        mouse_R.SetActive(false);
        LM_Manager.ins.panel_Pause.SetActive(true);
        UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = OnStep03;
    }
    void OnStep03(GameObject gameObject)
    {
        step03.SetActive(true);
        isStep03 = true;
    }
    void Update()
    {
        if (isStep03)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ray = cam.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100f) && hit.collider.tag == "DragStart")
                {
                    Debug.Log("Drag On");
                    dragOn = true;
                }
            }
            else if (dragOn && Input.GetMouseButton(0))
            {
                newDragItemPosition = Input.mousePosition;
                newDragItemPosition.z = dragItem.position.z - cam.transform.position.z;
                newDragItemPosition = cam.ScreenToWorldPoint(newDragItemPosition);
                newDragItemPosition.x = dragItem.position.x;
                newDragItemPosition.y = Mathf.Clamp(newDragItemPosition.y, dragStartPos.position.y, dragEndPos.position.y);

                dragItem.position = newDragItemPosition;
                if(Vector3.Distance(dragItem.position, dragEndPos.position) < 0.01f)
                {
                    LM_Manager.ins.panel_Pause.SetActive(true);
                    UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
                    dragOn = false;
                }
            }
            else if (dragOn && Input.GetMouseButtonUp(0))
            {
                //if (Physics.Raycast(ray, out hit, 100f) && hit.collider.tag == "DragStart")
                //{
                //    // The End
                //    Debug.Log("Drag End");
                //    LM_Manager.ins.panel_Pause.SetActive(true);
                //    UIEventListener.Get(LM_Manager.ins.guide_Touch).onClick = LM_Manager.ins.OnLevelComplete;
                //}
                dragOn = false;
                dragItemSprite.UpdateAnchors();
            }
        }
    }
    IEnumerator Wait_Pause(float delay)
    {
        yield return new WaitForSeconds(delay);
        LM_Manager.ins.panel_Pause.SetActive(true);
    }
}
