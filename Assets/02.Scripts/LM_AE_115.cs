﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_115 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02,
        step03,
        step04
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step00_Guide")]

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_keyboardGroup1;
    public GameObject step01_keyboard1_Ctrl;
    public GameObject step01_keyboard1_M;
    public UISprite step01_render1;
    public GameObject step01_box1;
    public UISprite step01_lossless;
    public UISprite step01_queueBtn;
    public UISprite step01_renderBtn;

    [Header("Step02")]
    public GameObject step02;
    public UISprite step02_outputModule;
    public GameObject step02_box2;
    public GameObject step02_box3;
    public UISprite step02_formatOption;
    public GameObject step02_box4;
    public UISprite step02_avi_select;
    public GameObject step02_box5;
    public UISprite step02_ok_select;

    [Header("Step03")]
    public GameObject step03;
    public GameObject step03_cameraPoint;
    public UISprite step03_customAVI;
    public GameObject step03_customAVI_glitters;
    public GameObject step03_box6;
    public UISprite step03_saveFolder;
    public GameObject step03_box7;
    public UISprite step03_renderBtn_on;
    public GameObject step03_box8;

    [Header("Step04")]
    public GameObject step04;
    public UISprite step04_rendering;
    public UISprite step04_progress;
    public UISprite step04_renderDone;
    public UISprite step04_stopBtn;
    public UISprite step04_pauseBtn;
    public GameObject step04_glitter;
    public UISprite step04_outputFolder;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_115_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);

        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_lossless.GetComponent<UISprite>().topAnchor.absolute = 343;
            step01_lossless.GetComponent<UISprite>().leftAnchor.absolute = 233;
            step01_lossless.GetComponent<UISprite>().rightAnchor.absolute = 314;

            step01_box1.GetComponent<UISprite>().topAnchor.absolute = 368;
            step01_box1.GetComponent<UISprite>().bottomAnchor.absolute = 322;
            step01_box1.GetComponent<UISprite>().leftAnchor.absolute = 217;
            step01_box1.GetComponent<UISprite>().rightAnchor.absolute = 327;

            step02_box3.GetComponent<UISprite>().topAnchor.absolute = 387;
            step02_box3.GetComponent<UISprite>().bottomAnchor.absolute = 330;
            step02_box3.GetComponent<UISprite>().leftAnchor.absolute = -212;
            step02_box3.GetComponent<UISprite>().rightAnchor.absolute = 110;

            step02_formatOption.GetComponent<UISprite>().topAnchor.absolute = 349;
            step02_formatOption.GetComponent<UISprite>().leftAnchor.absolute = -718;
            step02_formatOption.GetComponent<UISprite>().rightAnchor.absolute = -195;

            step02_box4.GetComponent<UISprite>().topAnchor.absolute = 328;
            step02_box4.GetComponent<UISprite>().bottomAnchor.absolute = 271;
            step02_box4.GetComponent<UISprite>().leftAnchor.absolute = -209;
            step02_box4.GetComponent<UISprite>().rightAnchor.absolute = 109;

            step02_avi_select.GetComponent<UISprite>().topAnchor.absolute = 325;
            step02_avi_select.GetComponent<UISprite>().bottomAnchor.absolute = 276;
            step02_avi_select.GetComponent<UISprite>().leftAnchor.absolute = -195;
            step02_avi_select.GetComponent<UISprite>().rightAnchor.absolute = 96;

            step02_box5.GetComponent<UISprite>().topAnchor.absolute = -459;
            step02_box5.GetComponent<UISprite>().bottomAnchor.absolute = -518;
            step02_box5.GetComponent<UISprite>().leftAnchor.absolute = 132;
            step02_box5.GetComponent<UISprite>().rightAnchor.absolute = 291;

            step02_ok_select.GetComponent<UISprite>().topAnchor.absolute = -468;
            step02_ok_select.GetComponent<UISprite>().bottomAnchor.absolute = -507;
            step02_ok_select.GetComponent<UISprite>().leftAnchor.absolute = 141;
            step02_ok_select.GetComponent<UISprite>().rightAnchor.absolute = 285;

            step03_customAVI.GetComponent<UISprite>().topAnchor.absolute = 361;
            step03_customAVI.GetComponent<UISprite>().bottomAnchor.absolute = 329;
            step03_customAVI.GetComponent<UISprite>().leftAnchor.absolute = 232;
            step03_customAVI.GetComponent<UISprite>().rightAnchor.absolute = 383;

            step03_box6.GetComponent<UISprite>().topAnchor.absolute = 371;
            step03_box6.GetComponent<UISprite>().bottomAnchor.absolute = 320;
            step03_box6.GetComponent<UISprite>().leftAnchor.absolute = 754;
            step03_box6.GetComponent<UISprite>().rightAnchor.absolute = -769;

            step03_box7.GetComponent<UISprite>().topAnchor.absolute = -237;
            step03_box7.GetComponent<UISprite>().bottomAnchor.absolute = -302;
            step03_box7.GetComponent<UISprite>().leftAnchor.absolute = 21;
            step03_box7.GetComponent<UISprite>().rightAnchor.absolute = 176;
            step03_customAVI.GetComponent<UISprite>().aspectRatio = (float)step03_customAVI.GetComponent<UISprite>().GetAtlasSprite().width / (float)step03_customAVI.GetComponent<UISprite>().GetAtlasSprite().height;
        }

    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_render1);
        lmSpriteList.Add(step01_lossless);
        lmSpriteList.Add(step01_queueBtn);
        lmSpriteList.Add(step01_renderBtn);
        lmSpriteList.Add(step02_outputModule);
        lmSpriteList.Add(step02_formatOption);
        lmSpriteList.Add(step02_avi_select);
        lmSpriteList.Add(step02_ok_select);
        lmSpriteList.Add(step03_customAVI);
        lmSpriteList.Add(step03_saveFolder);
        lmSpriteList.Add(step03_renderBtn_on);
        lmSpriteList.Add(step04_rendering);
        lmSpriteList.Add(step04_progress);
        lmSpriteList.Add(step04_renderDone);
        lmSpriteList.Add(step04_stopBtn);
        lmSpriteList.Add(step04_pauseBtn);
        lmSpriteList.Add(step04_outputFolder);


        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_render1.spriteName = "AE_115_0001s_0014_115_Render-Panner-Basic";
        step01_lossless.spriteName = "AE_115_0001s_0013_115_Lossless-(select)";
        step01_queueBtn.spriteName = "AE_115_0001s_0012_115_Queue-in-AEM";
        step01_renderBtn.spriteName = "AE_115_0001s_0011_115_Render-1";
        step02_outputModule.spriteName = "AE_115_0001s_0018_115_Output-Module-Settings";
        step02_formatOption.spriteName = "AE_115_0001s_0017_115_Format-Option";
        step02_avi_select.spriteName = "AE_115_0001s_0016_115_AVI-(select)";
        step02_ok_select.spriteName = "AE_115_0001s_0015_115_OK(select)";
        step03_customAVI.spriteName = "AE_115_0001s_0010_115_Custom-AVI";
        step03_saveFolder.spriteName = "AE_115_0001s_0008_115_Save-Folder";
        step03_renderBtn_on.spriteName = "AE_115_0001s_0006_115_Render";
        step04_rendering.spriteName = "AE_115_0001s_0002_115_Rendering-Ing-Menu";
        step04_progress.spriteName = "AE_115_0001s_0005_115_Render-Blue-Line";
        step04_renderDone.spriteName = "AE_115_0001s_0001_115_Render-Done-Menu";
        step04_stopBtn.spriteName = "AE_115_0001s_0003_115_Render-Stop-Butten";
        step04_pauseBtn.spriteName = "AE_115_0001s_0004_115_Pause";
        step04_outputFolder.spriteName = "AE_115_0001s_0000_115_Output-Folder";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        step01.SetActive(true);
        step01_keyboardGroup1.SetActive(true);
        step01_keyboard1_Ctrl.GetComponent<TweenScale>().enabled = true;
        step01_keyboard1_Ctrl.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_keyboard1_Ctrl.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
        {
            step01_keyboard1_Ctrl.GetComponent<TweenScale>().enabled = false;
            step01_keyboard1_Ctrl.transform.localScale = Vector3.one;

            step01_keyboard1_M.GetComponent<TweenScale>().enabled = true;
            step01_keyboard1_M.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_keyboard1_M.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
            {
                step01_keyboardGroup1.SetActive(false);

                step01_render1.gameObject.SetActive(true);
                step01_lossless.gameObject.SetActive(true);
                step01_renderBtn.gameObject.SetActive(true);
                step01_queueBtn.gameObject.SetActive(true);
                step01_box1.SetActive(true);
                step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
                step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                {
                    step01_box1.SetActive(false);
                    OnStep02();
                }));
            }));
        }));



    }

    public void OnStep02()
    {
        step02.SetActive(true);
        CameraManager.ins.MoveCamera(step02_outputModule.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
        {
            step02_outputModule.gameObject.SetActive(true);
            step02_outputModule.GetComponent<TweenAlpha>().onFinished.Clear();
            step02_outputModule.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
            {
                    
 
                    step02_box3.SetActive(true);
                    step02_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                    step02_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                    {
                        step02_formatOption.gameObject.SetActive(true);
                        
                        step02_box3.SetActive(false);
                        step02_box4.SetActive(true);
                        step02_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                        step02_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                        {
                            step02_avi_select.gameObject.SetActive(true);
                            step02_box4.SetActive(false);
                            step02_box5.SetActive(true);
                            step02_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                            step02_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
                            {
                                step02_box5.SetActive(false);
                                step02_ok_select.gameObject.SetActive(true);
                                step02_ok_select.GetComponent<TweenAlpha>().onFinished.Clear();
                                step02_ok_select.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate
                                {
                                    step02.SetActive(false);

                                    step03_customAVI.gameObject.SetActive(true);
                                    step03_customAVI_glitters.gameObject.SetActive(true);

                                    CameraManager.ins.MoveCamera(step03_cameraPoint.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
                                    {
                                        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_115_1", "MID");
                                        OnStep03();
                                    }));
                                }));
                            }));
                        }));
                    }));
          
            }));
        }));

    }
    public void OnStep03()
    {
        step03.SetActive(true);
        step03_box6.SetActive(true);
        step03_box6.GetComponent<UIEventTrigger>().onClick.Clear();
        step03_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            step03_box6.SetActive(false);
            step03_customAVI_glitters.gameObject.SetActive(false);
            LM_AE_Manager.ins.Off_Middle_Guide();
            CameraManager.ins.MoveCamera(step03_saveFolder.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate
            {
                step03_saveFolder.gameObject.SetActive(true);
                step03_saveFolder.GetComponent<TweenScale>().onFinished.Clear();
                step03_saveFolder.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate
                {
                    step03_box7.SetActive(true);
                    step03_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                    step03_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                        step03_box7.SetActive(false);
                        step03_saveFolder.gameObject.SetActive(false);
                        CameraManager.ins.MoveCamera(false, false, 0.5f).onFinished.Add(new EventDelegate(delegate
                        {
                            step03_box8.SetActive(true);
                            step03_renderBtn_on.gameObject.SetActive(true);
                            step03_box8.GetComponent<UIEventTrigger>().onClick.Clear();
                            step03_box8.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                step03_box8.SetActive(false);
                                step03_renderBtn_on.gameObject.SetActive(false);
                                step01_queueBtn.gameObject.SetActive(false);
                                step01_renderBtn.gameObject.SetActive(false);

                                step04.SetActive(true);
                                step04_pauseBtn.gameObject.SetActive(true);
                                step04_stopBtn.gameObject.SetActive(true);
                                step04_stopBtn.GetComponent<TweenAlpha>().onFinished.Clear();
                                step04_stopBtn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                    OnStep04();
	                            }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
    }

    public void OnStep04()
    {
        step04_rendering.gameObject.SetActive(true);
        CameraManager.ins.MoveCamera(false, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
            step04_progress.gameObject.SetActive(true);
            CameraManager.ins.MoveCamera(false, false, 3f).onFinished.Add(new EventDelegate(delegate {
                step04_pauseBtn.gameObject.SetActive(false);
                step04_stopBtn.gameObject.SetActive(false);
                step04_glitter.SetActive(true);

                step04_glitter.GetComponent<TweenAlpha>().onFinished.Clear();
                step04_glitter.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                    CameraManager.ins.MoveCamera(step04_outputFolder.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                        step04_outputFolder.gameObject.SetActive(true);
                        step04_outputFolder.GetComponent<TweenScale>().onFinished.Clear();
                        step04_outputFolder.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                            StartCoroutine(EndDelay());
                        }));
                    }));
                }));
            }));
        }));

    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_115_2");
    }
}
