﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_214 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01_CameraPoint;
    public GameObject step01;
    public GameObject step01_box1;
    public UISprite step01_spriteEye1;
    public UISprite step01_spriteVideo;
    public GameObject step01_box2;
    public UISprite step01_spriteEye2;
    public UISprite step01_spriteLayer;
    public GameObject step01_box3;
    public UISprite step01_spriteEye3;
    public UISprite step01_spriteTextLayer;
    public GameObject step01_box4;
    public UISprite step01_spriteDot1;
    public GameObject step01_box5;
    public UISprite step01_spriteDot2;
    public GameObject step01_box6;
    public UISprite step01_spriteDot3;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_214_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }
    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteEye1);
        lmSpriteList.Add(step01_spriteVideo);
        lmSpriteList.Add(step01_spriteEye2);
        lmSpriteList.Add(step01_spriteLayer);
        lmSpriteList.Add(step01_spriteEye3);
        lmSpriteList.Add(step01_spriteTextLayer);
        lmSpriteList.Add(step01_spriteDot1);
        lmSpriteList.Add(step01_spriteDot2);
        lmSpriteList.Add(step01_spriteDot3);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteEye1.spriteName = "AE_214_video";
        step01_spriteVideo.spriteName = "AE_214_mp4";
        step01_spriteEye2.spriteName = "AE_214_video";
        step01_spriteLayer.spriteName = "AE_214_shape layer";
        step01_spriteEye3.spriteName = "AE_214_video";
        step01_spriteTextLayer.spriteName = "AE_214_text layer";
        step01_spriteDot1.spriteName = "AE_214_solo";
        step01_spriteDot2.spriteName = "AE_214_solo";
        step01_spriteDot3.spriteName = "AE_214_solo";

    }
    /*
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

        }));

    */
    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);

        CameraManager.ins.MoveCamera(false, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_214_1", "MID");
            
            step01_box1.SetActive(true);
            step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                LM_AE_Manager.ins.Off_Middle_Guide();
                step01_box1.SetActive(false);

                step01_spriteEye1.gameObject.SetActive(true);
                step01_spriteVideo.gameObject.SetActive(true);

                CameraManager.ins.MoveCamera(step01_spriteVideo.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                    delay0.gameObject.SetActive(true);
                    delay0.onFinished.Clear();
                    delay0.onFinished.Add(new EventDelegate(delegate {
                        CameraManager.ins.MoveCamera(false, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                            step01_box2.SetActive(true);
                            step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                step01_box2.SetActive(false);

                                step01_spriteEye2.gameObject.SetActive(true);
                                step01_spriteLayer.gameObject.SetActive(true);

                                CameraManager.ins.MoveCamera(step01_spriteVideo.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                                    delay1.gameObject.SetActive(true);
                                    delay1.onFinished.Clear();
                                    delay1.onFinished.Add(new EventDelegate(delegate {
                                        CameraManager.ins.MoveCamera(false, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                            step01_box3.SetActive(true);
                                            step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                                            step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                step01_box3.SetActive(false);

                                                step01_spriteEye3.gameObject.SetActive(true);
                                                step01_spriteTextLayer.gameObject.SetActive(true);

                                                CameraManager.ins.MoveCamera(step01_spriteVideo.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                                                    delay2.gameObject.SetActive(true);
                                                    delay2.onFinished.Clear();
                                                    delay2.onFinished.Add(new EventDelegate(delegate {
                                                        CameraManager.ins.MoveCamera(false, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                                            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_214_2", "MID");

                                                            step01_box4.SetActive(true);
                                                            step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                                            step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                LM_AE_Manager.ins.Off_Middle_Guide();
                                                                step01_box4.SetActive(false);

                                                                step01_spriteDot1.gameObject.SetActive(true);

                                                                step01_spriteLayer.gameObject.SetActive(false);
                                                                step01_spriteTextLayer.gameObject.SetActive(false);

                                                                CameraManager.ins.MoveCamera(step01_spriteVideo.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                                                                    delay3.gameObject.SetActive(true);
                                                                    delay3.onFinished.Clear();
                                                                    delay3.onFinished.Add(new EventDelegate(delegate {
                                                                        CameraManager.ins.MoveCamera(false, true, 0.5f).onFinished.Add(new EventDelegate(delegate {

                                                                            step01_box5.SetActive(true);
                                                                            step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                            step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                                step01_box5.SetActive(false);

                                                                                step01_spriteDot2.gameObject.SetActive(true);
                                                                                step01_spriteLayer.gameObject.SetActive(true);

                                                                                CameraManager.ins.MoveCamera(step01_spriteVideo.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                                                                                    delay4.gameObject.SetActive(true);
                                                                                    delay4.onFinished.Clear();
                                                                                    delay4.onFinished.Add(new EventDelegate(delegate {
                                                                                        CameraManager.ins.MoveCamera(false, true, 0.5f).onFinished.Add(new EventDelegate(delegate {

                                                                                            step01_box6.SetActive(true);
                                                                                            step01_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                                            step01_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                                                step01_box6.SetActive(false);

                                                                                                step01_spriteDot3.gameObject.SetActive(true);
                                                                                                step01_spriteTextLayer.gameObject.SetActive(true);

                                                                                                CameraManager.ins.MoveCamera(step01_spriteVideo.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                                                                                                    StartCoroutine(EndDelay());
                                                                                                }));
                                                                                            }));
                                                                                        }));
                                                                                    }));
                                                                                }));
                                                                            }));
                                                                        }));
                                                                    }));
                                                                }));
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));

        }));


    }


    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_214_3");
    }
}
