﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_226 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_mouse1;
    public GameObject step01_box1;
    public UISprite step01_spriteC_jpg1;
    public UISprite step01_spriteMenu1;
    public GameObject step01_box2;
    public UISprite step01_spriteReloadFootage;
    public UISprite step01_spriteC_icon;
    public UISprite step01_spriteD_img;
    public GameObject step01_mouse2;
    public GameObject step01_box3;
    public UISprite step01_spriteC_jpg2;
    public UISprite step01_spriteD_jpg;
    public UISprite step01_spriteMenu2;
    public GameObject step01_box4;
    public UISprite step01_spriteReplaceFootage;
    public UISprite step01_spriteMenu2_1;
    public GameObject step01_box5;
    public UISprite step01_spriteFile;
    public UISprite step01_spritePopup;
    public GameObject step01_box6;
    public UISprite step01_spriteJpg_selected;
    public GameObject step01_box7;
    public UISprite step01_spriteD_icon;
    public UISprite step01_spriteC_img;

    public UISprite step01_C_new;
    public UISprite step01_D_new;

    public TweenAlpha step01_delay0;
    public TweenAlpha step01_delay1;
    public TweenAlpha step01_delay2;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_226_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_box5.GetComponent<UISprite>().topAnchor.absolute = 22;
            step01_box5.GetComponent<UISprite>().bottomAnchor.absolute = -44;
            step01_box5.GetComponent<UISprite>().leftAnchor.absolute = 786;
            step01_box5.GetComponent<UISprite>().rightAnchor.absolute = -319;

            step01_spriteMenu2_1.GetComponent<UISprite>().topAnchor.absolute = 20;
            step01_spriteMenu2_1.GetComponent<UISprite>().bottomAnchor.absolute = -186;
            step01_spriteMenu2_1.GetComponent<UISprite>().leftAnchor.absolute = 786;
            step01_spriteMenu2_1.GetComponent<UISprite>().rightAnchor.absolute = -322;

            step01_spriteFile.GetComponent<UISprite>().topAnchor.absolute = 11;
            step01_spriteFile.GetComponent<UISprite>().bottomAnchor.absolute = -34;
            step01_spriteFile.GetComponent<UISprite>().leftAnchor.absolute = 793;
            step01_spriteFile.GetComponent<UISprite>().rightAnchor.absolute = -329;

            step01_spriteJpg_selected.GetComponent<UISprite>().topAnchor.absolute = 192;
            step01_spriteJpg_selected.GetComponent<UISprite>().bottomAnchor.absolute = 18;
            step01_spriteJpg_selected.GetComponent<UISprite>().leftAnchor.absolute = 270;
            step01_spriteJpg_selected.GetComponent<UISprite>().rightAnchor.absolute = 441;

            step01_spriteReplaceFootage.GetComponent<UISprite>().leftAnchor.absolute = 229;
            step01_spriteReplaceFootage.GetComponent<UISprite>().rightAnchor.absolute = 775;

            step01_spriteMenu2_1.GetComponent<UISprite>().aspectRatio = (float)step01_spriteMenu2_1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteMenu2_1.GetComponent<UISprite>().GetAtlasSprite().height;
            step01_spriteFile.GetComponent<UISprite>().aspectRatio = (float)step01_spriteFile.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteFile.GetComponent<UISprite>().GetAtlasSprite().height;

        }
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteC_jpg1);
        lmSpriteList.Add(step01_spriteMenu1);
        lmSpriteList.Add(step01_spriteReloadFootage);
        lmSpriteList.Add(step01_spriteC_icon);
        lmSpriteList.Add(step01_spriteD_img);
        lmSpriteList.Add(step01_spriteC_jpg2);
        lmSpriteList.Add(step01_spriteD_jpg);
        lmSpriteList.Add(step01_spriteMenu2);
        lmSpriteList.Add(step01_spriteReplaceFootage);
        lmSpriteList.Add(step01_spriteMenu2_1);
        lmSpriteList.Add(step01_spriteFile);
        lmSpriteList.Add(step01_spritePopup);
        lmSpriteList.Add(step01_spriteJpg_selected);
        lmSpriteList.Add(step01_spriteD_icon);
        lmSpriteList.Add(step01_spriteC_img);


        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteC_jpg1.spriteName = "226_C.jpg";
        step01_spriteMenu1.spriteName = "226_Menu-1";
        step01_spriteReloadFootage.spriteName = "226_Reload-Footage";
        step01_spriteC_icon.spriteName = "226_C.jpg-Layer-Icon";
        step01_spriteD_img.spriteName = "226_D-Image";
        step01_spriteC_jpg2.spriteName = "226_C.jpg_2";
        step01_spriteD_jpg.spriteName = "226_D.jpg";
        step01_spriteMenu2.spriteName = "226_Menu-1";
        step01_spriteReplaceFootage.spriteName = "226_Replace-Footage";
        step01_spriteMenu2_1.spriteName = "226_Menu-2";
        step01_spriteFile.spriteName = "226_File";
        step01_spritePopup.spriteName = "226_Pop-up";
        step01_spriteJpg_selected.spriteName = "226_C.jpg-choosing-1";
        step01_spriteD_icon.spriteName = "226_D.jpg-Layer-Icon";
        step01_spriteC_img.spriteName = "226_C-Image";

    }

    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);

        step01.SetActive(true);

        step01_mouse1.SetActive(true);
        step01_box1.SetActive(true);
        step01_mouse1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_mouse1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            step01_mouse1.SetActive(false);
            step01_box1.SetActive(false);

            step01_spriteC_jpg1.gameObject.SetActive(true);
            step01_spriteMenu1.gameObject.SetActive(true);
            step01_box2.SetActive(true);
            step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                step01_box2.SetActive(false);

                step01_spriteD_img.gameObject.SetActive(true);
                step01_spriteReloadFootage.gameObject.SetActive(true);

                step01_C_new.gameObject.SetActive(true);
                step01_spriteReloadFootage.GetComponent<TweenAlpha>().onFinished.Clear();
                step01_spriteReloadFootage.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {

                    step01_spriteMenu1.gameObject.SetActive(false);
                    step01_spriteReloadFootage.gameObject.SetActive(false);

                    step01_spriteC_icon.gameObject.SetActive(true);


                    CameraManager.ins.MoveCamera(step01_spriteD_img.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {

                        step01_delay0.gameObject.SetActive(true);
                        step01_delay0.onFinished.Clear();
                        step01_delay0.onFinished.Add(new EventDelegate(delegate {
                            CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_226_1", "MID");

                                step01_box3.SetActive(true);
                                step01_mouse2.SetActive(true);
                                step01_mouse2.GetComponent<UIEventTrigger>().onClick.Clear();
                                step01_mouse2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                    LM_AE_Manager.ins.Off_Middle_Guide();

                                    step01_spriteC_jpg1.gameObject.SetActive(false);
                                    step01_spriteC_icon.gameObject.SetActive(false);
                                    step01_box3.SetActive(false);
                                    step01_mouse2.SetActive(false);

                                    step01_spriteC_jpg2.gameObject.SetActive(true);
                                    step01_spriteD_jpg.gameObject.SetActive(true);

                                    step01_spriteMenu2.gameObject.SetActive(true);
                                    step01_box4.SetActive(true);
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                        step01_box4.SetActive(false);

                                        step01_spriteReplaceFootage.gameObject.SetActive(true);
                                        step01_spriteReplaceFootage.GetComponent<TweenAlpha>().onFinished.Clear();
                                        step01_spriteReplaceFootage.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                            
                                            step01_spriteMenu2_1.gameObject.SetActive(true);
                                            CameraManager.ins.MoveCamera(step01_spriteMenu2_1.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                                
                                                step01_box5.SetActive(true);
                                                step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                                step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                    step01_box5.SetActive(false);

                                                    step01_spriteFile.gameObject.SetActive(true);
                                                    step01_spriteFile.GetComponent<TweenAlpha>().onFinished.Clear();
                                                    step01_spriteFile.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                        step01_spriteMenu2.gameObject.SetActive(false);
                                                        step01_spriteReplaceFootage.gameObject.SetActive(false);
                                                        step01_spriteMenu2_1.gameObject.SetActive(false);
                                                        step01_spriteFile.gameObject.SetActive(false);

                                                        CameraManager.ins.MoveCamera(step01_spritePopup.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                                            step01_spritePopup.gameObject.SetActive(true);
                                                            step01_spritePopup.GetComponent<TweenScale>().onFinished.Clear();
                                                            step01_spritePopup.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                                                                step01_box6.SetActive(true);
                                                                step01_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                step01_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                    step01_box6.SetActive(false);

                                                                    step01_spriteJpg_selected.gameObject.SetActive(true);
                                                                    step01_box7.SetActive(true);
                                                                    step01_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                                                                    step01_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                        step01_box7.SetActive(false);
                                                                        step01_spritePopup.gameObject.SetActive(false);
                                                                        step01_spriteD_jpg.spriteName = "226_Reloaded-D.jpg";
                                                                        step01_spriteJpg_selected.gameObject.SetActive(false);

                                                                        step01_D_new.gameObject.SetActive(true);
                                                                        step01_spriteC_img.gameObject.SetActive(true);
                                                                        step01_spriteD_icon.gameObject.SetActive(true);

                                                                        step01_delay1.gameObject.SetActive(true);
                                                                        step01_delay1.GetComponent<TweenAlpha>().onFinished.Clear();
                                                                        step01_delay1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                                            CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                                                                StartCoroutine(EndDelay());
                                                                            }));
                                                                        }));
                                                                    }));
                                                                }));
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));

    }


    public void OnStep02()
    {

    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_226_2");
    }
}
