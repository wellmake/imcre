﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_DR_112 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_groupKeyboard;
    public GameObject step01_keyboard_Ctrl;
    public GameObject step01_keyboard_I;
    public UISprite step01_spritePopupImport;
    public GameObject step01_box1;
    public UISprite step01_spriteA;
    public GameObject step01_box2;
    public UISprite step01_spriteB;
    public GameObject step01_box3;
    public UISprite step01_spriteC;
    public GameObject step01_box4;
    public UISprite step01_spriteOpen_btn;
    public UISprite step01_spriteNoClips;

    [Header("Step01")]
    public GameObject step02;
    public UISprite step02_spriteA_mp4;
    public UISprite step02_spriteA_mp4_selected;
    public UISprite step02_spriteBC_mp4;
    public GameObject step02_box5;
    public GameObject step02_Mouse;
    public UISprite step02_spriteMenu;
    public GameObject step02_box6;
    public UISprite step02_spriteCreateNewTimeline_btn;
    public UISprite step02_spritePopupNewTimeline;
    public GameObject step02_box7;
    public UISprite step02_spriteNameBox1;
    public UILabel step02_labelName1;
    public UISprite step02_spriteNameBox2;
    public GameObject step02_box8;
    public UISprite step02_spriteCreate_btn;
    public GameObject step02_groupResult;
    public UISprite step02_spriteTimeline;
    public UISprite step02_spriteEdited;
    public UISprite step02_spriteTime;
    public UISprite step02_spriteFowardBtn;
    public UISprite step02_spriteVideo;
    public UISprite step02_spriteTools;
    public UISprite step02_spriteVideoClip;
    public UISprite step02_spriteIndicator;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_DR_112_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }
    }
    
    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spritePopupImport);
        lmSpriteList.Add(step01_spriteA);
        lmSpriteList.Add(step01_spriteB);
        lmSpriteList.Add(step01_spriteC);
        lmSpriteList.Add(step01_spriteOpen_btn);
        lmSpriteList.Add(step01_spriteNoClips);
        lmSpriteList.Add(step02_spriteA_mp4);
        lmSpriteList.Add(step02_spriteA_mp4_selected);
        lmSpriteList.Add(step02_spriteBC_mp4);
        lmSpriteList.Add(step02_spriteMenu);
        lmSpriteList.Add(step02_spriteCreateNewTimeline_btn);
        lmSpriteList.Add(step02_spritePopupNewTimeline);
        lmSpriteList.Add(step02_spriteNameBox1);
        lmSpriteList.Add(step02_spriteNameBox2);
        lmSpriteList.Add(step02_spriteCreate_btn);
        lmSpriteList.Add(step02_spriteTimeline);
        lmSpriteList.Add(step02_spriteEdited);
        lmSpriteList.Add(step02_spriteTime);
        lmSpriteList.Add(step02_spriteFowardBtn);
        lmSpriteList.Add(step02_spriteVideo);
        lmSpriteList.Add(step02_spriteTools);
        lmSpriteList.Add(step02_spriteVideoClip);
        lmSpriteList.Add(step02_spriteIndicator);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spritePopupImport.spriteName = "112_Import-Popup";
        step01_spriteA.spriteName = "112_Import-Popup_2";
        step01_spriteB.spriteName = "112_Import-Popup_3";
        step01_spriteC.spriteName = "112_Import-Popup_4";
        step01_spriteOpen_btn.spriteName = "112_Open-Button";
        step01_spriteNoClips.spriteName = "112_No-clips-in-media-pool";
        step02_spriteA_mp4.spriteName = "112_A-File-1";
        step02_spriteA_mp4_selected.spriteName = "112_A-File-2";
        step02_spriteBC_mp4.spriteName = "112_C,B.mp4";
        step02_spriteMenu.spriteName = "112_Menu";
        step02_spriteCreateNewTimeline_btn.spriteName = "112_Create-New-Timeline-Using-Selected-Clips";
        step02_spritePopupNewTimeline.spriteName = "112_New-Timeline-Popup";
        step02_spriteNameBox1.spriteName = "112_Start-Timcode-Box";
        step02_spriteNameBox2.spriteName = "112_Timeline-Name-Box";
        step02_spriteCreate_btn.spriteName = "112_Create-Button";
        step02_spriteTimeline.spriteName = "112_Timeline-1";
        step02_spriteEdited.spriteName = "112_Edited";
        step02_spriteTime.spriteName = "112_00_00_00_00";
        step02_spriteFowardBtn.spriteName = "112_Forward-Button";
        step02_spriteVideo.spriteName = "112_Video";
        step02_spriteTools.spriteName = "112_Tools";
        step02_spriteVideoClip.spriteName = "112_Video-Clip";
        step02_spriteIndicator.spriteName = "112_Indicator";

    }
    /*
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

        }));

    */
    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);

        step01_groupKeyboard.SetActive(true);
        step01_keyboard_Ctrl.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_keyboard_Ctrl.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
        {
            step01_keyboard_Ctrl.GetComponent<TweenScale>().enabled = false;
            step01_keyboard_Ctrl.transform.localScale = new Vector3(0.9f, 0.9f, 1f);

            step01_keyboard_I.GetComponent<TweenScale>().enabled = true;

            step01_keyboard_I.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_keyboard_I.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate
            {
                step01_keyboard_I.GetComponent<TweenScale>().enabled = false;
                step01_keyboard_I.transform.localScale = new Vector3(0.9f, 0.9f, 1f);

                CameraManager.ins.MoveCamera(step01_spritePopupImport.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                    step01_groupKeyboard.SetActive(false);

                    step01_spritePopupImport.gameObject.SetActive(true);
                    step01_spritePopupImport.GetComponent<TweenScale>().onFinished.Clear();
                    step01_spritePopupImport.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {
                        step01_box1.SetActive(true);
                        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step01_box1.SetActive(false);

                            step01_spriteA.gameObject.SetActive(true);

                            step01_box2.SetActive(true);
                            step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                step01_box2.SetActive(false);

                                step01_spriteB.gameObject.SetActive(true);

                                step01_box3.SetActive(true);
                                step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                                step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                    step01_box3.SetActive(false);

                                    step01_spriteC.gameObject.SetActive(true);

                                    step01_box4.SetActive(true);
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                    step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                        step01_box4.SetActive(false);

                                        step01_spriteOpen_btn.gameObject.SetActive(true);
                                        step01_spriteOpen_btn.GetComponent<TweenAlpha>().onFinished.Clear();
                                        step01_spriteOpen_btn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                            step01.SetActive(false);

                                            OnStep02();
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
    }

    /*
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

        }));

    */
    public void OnStep02()
    {
        step02.SetActive(true);

        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {

            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_DR_112_1", "MID");

            step02_box5.SetActive(true);
            step02_Mouse.SetActive(true);
            step02_Mouse.GetComponent<UIEventTrigger>().onClick.Clear();
            step02_Mouse.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                LM_AE_Manager.ins.Off_Middle_Guide();

                step02_box5.SetActive(false);
                step02_Mouse.SetActive(false);

                step02_spriteA_mp4_selected.gameObject.SetActive(true);
                step02_spriteMenu.gameObject.SetActive(true);
                step02_spriteMenu.GetComponent<TweenAlpha>().onFinished.Clear();
                step02_spriteMenu.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {

                    CameraManager.ins.MoveCamera(step02_spriteMenu.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                        step02_box6.SetActive(true);
                        step02_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                        step02_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step02_box6.SetActive(false);

                            step02_spriteCreateNewTimeline_btn.gameObject.SetActive(true);
                            step02_spriteCreateNewTimeline_btn.GetComponent<TweenAlpha>().onFinished.Clear();
                            step02_spriteCreateNewTimeline_btn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                
                                step02_spriteMenu.gameObject.SetActive(false);
                                step02_spriteCreateNewTimeline_btn.gameObject.SetActive(false);

                                step02_spritePopupNewTimeline.gameObject.SetActive(true);
                                step02_spritePopupNewTimeline.GetComponent<TweenScale>().onFinished.Clear();
                                step02_spritePopupNewTimeline.GetComponent<TweenScale>().onFinished.Add(new EventDelegate(delegate {

                                    CameraManager.ins.MoveCamera(step02_spritePopupNewTimeline.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                        step02_box7.SetActive(true);
                                        step02_box7.GetComponent<UIEventTrigger>().onClick.Clear();
                                        step02_box7.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                            step02_box7.SetActive(false);

                                            step02_spriteNameBox1.gameObject.SetActive(true);
                                            step02_spriteNameBox2.gameObject.SetActive(true);
                                            step02_labelName1.GetComponent<TypewriterEffect>().onFinished.Clear();
                                            step02_labelName1.GetComponent<TypewriterEffect>().onFinished.Add(new EventDelegate(delegate {
                                                step02_box8.SetActive(true);
                                                step02_box8.GetComponent<UIEventTrigger>().onClick.Clear();
                                                step02_box8.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                    step02_box8.SetActive(false);

                                                    step02_spriteCreate_btn.gameObject.SetActive(true);
                                                    step02_spriteCreate_btn.GetComponent<TweenAlpha>().onFinished.Clear();
                                                    step02_spriteCreate_btn.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                        step02_spritePopupNewTimeline.gameObject.SetActive(false);
                                                        step02_spriteNameBox1.gameObject.SetActive(false);
                                                        step02_spriteNameBox2.gameObject.SetActive(false);
                                                        step02_spriteCreate_btn.gameObject.SetActive(false);
                                                        step02_spriteA_mp4_selected.gameObject.SetActive(false);

                                                        step02_groupResult.SetActive(true);

                                                        delay0.gameObject.SetActive(true);
                                                        delay0.GetComponent<TweenAlpha>().onFinished.Clear();
                                                        delay0.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                            StartCoroutine(EndDelay());
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_DR_112_2");
    }
}
