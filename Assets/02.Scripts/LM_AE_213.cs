﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_AE_213 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_box1;
    public UISprite step01_spriteWindow;
    public UISprite step01_spriteMenu1;
    public GameObject step01_box2;
    public UISprite step01_spriteMenu1_effectCtrl;

    [Header("Step02")]
    public GameObject step02;
    public GameObject step02_glitters;
    public UISprite step02_spriteEffectCtrl_panel;
    public UISprite step02_spriteEffectCtrl_panel_dragable;
    public GameObject step02_box3;
    public GameObject step02_box4;
    public GameObject step02_box4_dragPointer;
    public GameObject step02_dragEnd;
    public GameObject step02_groupMovingPanel;
    public UISprite step02_spriteMoving1;
    public UISprite step02_spriteMoving2;
    public UISprite step02_spriteMoving3;

    [Header("Step03")]
    public GameObject step03;
    public UISprite step03_spriteEffectCtrl_panel_moved;
    public GameObject step03_glitters;
    public GameObject step03_box5;
    public UISprite step03_spritePanelMenu;
    public GameObject step03_box6;
    public UISprite step03_spriteClosePanel;

    [Header("Step04")]
    public GameObject step04;
    public GameObject step04_slider1;
    public UISprite step04_spriteSliderThumb1;
    public UISprite step04_spriteExpand_Left1;
    public UISprite step04_spriteExpand_Left2;
    public UISprite step04_spriteExpand_Left3;
    public UISprite step04_spriteExpand_Top1;
    public UISprite step04_spriteExpand_Top2;
    public GameObject step04_expaneLimit;
    public UISprite step04_spriteExpand_Top3;
    public UISprite step04_spriteExpand_Top4;
    public GameObject step04_box7;
    public GameObject step04_box8;
    public GameObject step04_box8_dragPointer;
    public GameObject step04_slider2;
    public UISprite step04_spriteSliderThumb2;
    public UISprite step04_spriteExpand_Bottom1;
    public UISprite step04_spriteExpand_Bottom2;
    public UISprite step04_spriteExpand_Bottom3;
    public UISprite step04_spriteExpand_Bottom4;
    public GameObject step04_box9;
    public GameObject step04_box10;
    public GameObject step04_box10_dragPointer;
    public GameObject step04_box11;
    public UISprite step04_spriteWindow;
    public UISprite step04_spriteMenu1;
    public GameObject step04_box12;
    public UISprite step04_spriteEffectCtrl;
    public UISprite step04_spriteMenu2;
    public GameObject step04_box13;
    public UISprite step04_spriteDefault;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_AE_213_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));

        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            step01_box1.GetComponent<UISprite>().leftAnchor.absolute = 745;
            step01_box1.GetComponent<UISprite>().rightAnchor.absolute = 838;

            step01_spriteWindow.GetComponent<UISprite>().leftAnchor.absolute = 753;
            step01_spriteWindow.GetComponent<UISprite>().rightAnchor.absolute = 831;

            step01_spriteMenu1.GetComponent<UISprite>().leftAnchor.absolute = -2122;
            step01_spriteMenu1.GetComponent<UISprite>().rightAnchor.absolute = -582;

            step01_box2.GetComponent<UISprite>().topAnchor.absolute = -515;
            step01_box2.GetComponent<UISprite>().bottomAnchor.absolute = -576;
            step01_box2.GetComponent<UISprite>().leftAnchor.absolute = 742;
            step01_box2.GetComponent<UISprite>().rightAnchor.absolute = -270;

            step01_spriteMenu1_effectCtrl.GetComponent<UISprite>().topAnchor.absolute = -794;
            step01_spriteMenu1_effectCtrl.GetComponent<UISprite>().bottomAnchor.absolute = -297;
            step01_spriteMenu1_effectCtrl.GetComponent<UISprite>().leftAnchor.absolute = 755;
            step01_spriteMenu1_effectCtrl.GetComponent<UISprite>().rightAnchor.absolute = -285;

            step03_box5.GetComponent<UISprite>().topAnchor.absolute = 242;
            step03_box5.GetComponent<UISprite>().bottomAnchor.absolute = 185;
            step03_box5.GetComponent<UISprite>().leftAnchor.absolute = 239;
            step03_box5.GetComponent<UISprite>().rightAnchor.absolute = 303;

            step03_spriteClosePanel.GetComponent<UISprite>().topAnchor.absolute = 236;
            step03_spriteClosePanel.GetComponent<UISprite>().bottomAnchor.absolute = 202;
            step03_spriteClosePanel.GetComponent<UISprite>().leftAnchor.absolute = 312;
            step03_spriteClosePanel.GetComponent<UISprite>().rightAnchor.absolute = 668;

            step04_box11.GetComponent<UISprite>().leftAnchor.absolute = 745;
            step04_box11.GetComponent<UISprite>().rightAnchor.absolute = 836;

            step04_spriteMenu1.GetComponent<UISprite>().leftAnchor.absolute = -2122;
            step04_spriteMenu1.GetComponent<UISprite>().rightAnchor.absolute = -582;

            step04_spriteWindow.GetComponent<UISprite>().leftAnchor.absolute = 753;
            step04_spriteWindow.GetComponent<UISprite>().rightAnchor.absolute = 831;

            step04_spriteEffectCtrl.GetComponent<UISprite>().topAnchor.absolute = -90;
            step04_spriteEffectCtrl.GetComponent<UISprite>().bottomAnchor.absolute = -128;
            step04_spriteEffectCtrl.GetComponent<UISprite>().leftAnchor.absolute = 755;
            step04_spriteEffectCtrl.GetComponent<UISprite>().rightAnchor.absolute = -288;

            step04_box12.GetComponent<UISprite>().leftAnchor.absolute = 750;
            step04_box12.GetComponent<UISprite>().rightAnchor.absolute = -282;

            step04_spriteMenu2.GetComponent<UISprite>().topAnchor.absolute = -87;
            step04_spriteMenu2.GetComponent<UISprite>().bottomAnchor.absolute = 89;
            step04_spriteMenu2.GetComponent<UISprite>().leftAnchor.absolute = -281;
            step04_spriteMenu2.GetComponent<UISprite>().rightAnchor.absolute = 245;

            step04_box13.GetComponent<UISprite>().topAnchor.absolute = 316;
            step04_box13.GetComponent<UISprite>().bottomAnchor.absolute = 262;
            step04_box13.GetComponent<UISprite>().leftAnchor.absolute = -281;
            step04_box13.GetComponent<UISprite>().rightAnchor.absolute = 242;

            step04_spriteDefault.GetComponent<UISprite>().topAnchor.absolute = 358;
            step04_spriteDefault.GetComponent<UISprite>().bottomAnchor.absolute = 217;
            step04_spriteDefault.GetComponent<UISprite>().leftAnchor.absolute = -276;
            step04_spriteDefault.GetComponent<UISprite>().rightAnchor.absolute = 240;

            step01_spriteWindow.GetComponent<UISprite>().aspectRatio = (float)step01_spriteWindow.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteWindow.GetComponent<UISprite>().GetAtlasSprite().height;
            step01_spriteMenu1.GetComponent<UISprite>().aspectRatio = (float)step01_spriteMenu1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteMenu1.GetComponent<UISprite>().GetAtlasSprite().height;
            step01_spriteMenu1_effectCtrl.GetComponent<UISprite>().aspectRatio = (float)step01_spriteMenu1_effectCtrl.GetComponent<UISprite>().GetAtlasSprite().width / (float)step01_spriteMenu1_effectCtrl.GetComponent<UISprite>().GetAtlasSprite().height;
            step03_spritePanelMenu.GetComponent<UISprite>().aspectRatio = (float)step03_spritePanelMenu.GetComponent<UISprite>().GetAtlasSprite().width / (float)step03_spritePanelMenu.GetComponent<UISprite>().GetAtlasSprite().height;

            step04_spriteWindow.GetComponent<UISprite>().aspectRatio = (float)step04_spriteWindow.GetComponent<UISprite>().GetAtlasSprite().width / (float)step04_spriteWindow.GetComponent<UISprite>().GetAtlasSprite().height;
            step04_spriteMenu1.GetComponent<UISprite>().aspectRatio = (float)step04_spriteMenu1.GetComponent<UISprite>().GetAtlasSprite().width / (float)step04_spriteMenu1.GetComponent<UISprite>().GetAtlasSprite().height;

            step04_spriteEffectCtrl.GetComponent<UISprite>().aspectRatio = (float)step04_spriteEffectCtrl.GetComponent<UISprite>().GetAtlasSprite().width / (float)step04_spriteEffectCtrl.GetComponent<UISprite>().GetAtlasSprite().height;
            step04_spriteMenu2.GetComponent<UISprite>().aspectRatio = (float)step04_spriteMenu2.GetComponent<UISprite>().GetAtlasSprite().width / (float)step04_spriteMenu2.GetComponent<UISprite>().GetAtlasSprite().height;
            step04_spriteDefault.GetComponent<UISprite>().aspectRatio = (float)step04_spriteDefault.GetComponent<UISprite>().GetAtlasSprite().width / (float)step04_spriteDefault.GetComponent<UISprite>().GetAtlasSprite().height;
        }
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }

        if(step04_spriteExpand_Top3.transform.localPosition.y >= step04_expaneLimit.transform.localPosition.y)
        {
            step04_spriteExpand_Top3.updateAnchors = UIRect.AnchorUpdate.OnEnable;
        } else
        {
            step04_spriteExpand_Top3.updateAnchors = UIRect.AnchorUpdate.OnUpdate;
        }
    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteWindow);
        lmSpriteList.Add(step01_spriteMenu1);
        lmSpriteList.Add(step01_spriteMenu1_effectCtrl);

        lmSpriteList.Add(step02_spriteEffectCtrl_panel);
        lmSpriteList.Add(step02_spriteEffectCtrl_panel_dragable);
        lmSpriteList.Add(step02_spriteMoving1);
        lmSpriteList.Add(step02_spriteMoving2);
        lmSpriteList.Add(step02_spriteMoving3);

        lmSpriteList.Add(step03_spriteEffectCtrl_panel_moved);
        lmSpriteList.Add(step03_spritePanelMenu);
        lmSpriteList.Add(step03_spriteClosePanel);

        lmSpriteList.Add(step04_spriteSliderThumb1);
        lmSpriteList.Add(step04_spriteExpand_Left1);
        lmSpriteList.Add(step04_spriteExpand_Left2);
        lmSpriteList.Add(step04_spriteExpand_Left3);
        lmSpriteList.Add(step04_spriteExpand_Top1);
        lmSpriteList.Add(step04_spriteExpand_Top2);
        lmSpriteList.Add(step04_spriteExpand_Top3);
        lmSpriteList.Add(step04_spriteExpand_Top4);
        lmSpriteList.Add(step04_spriteSliderThumb2);
        lmSpriteList.Add(step04_spriteExpand_Bottom1);
        lmSpriteList.Add(step04_spriteExpand_Bottom2);
        lmSpriteList.Add(step04_spriteExpand_Bottom3);
        lmSpriteList.Add(step04_spriteExpand_Bottom4);
        lmSpriteList.Add(step04_spriteWindow);
        lmSpriteList.Add(step04_spriteMenu1);
        lmSpriteList.Add(step04_spriteEffectCtrl);
        lmSpriteList.Add(step04_spriteMenu2);
        lmSpriteList.Add(step04_spriteDefault);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteWindow.spriteName = "213_Window";
        step01_spriteMenu1.spriteName = "213_Menu-1";
        step01_spriteMenu1_effectCtrl.spriteName = "213_Menu_Effet-Controls-";
        step02_spriteEffectCtrl_panel.spriteName = "213_Effect-Contol-Pannel--Top";
        step02_spriteEffectCtrl_panel_dragable.spriteName = "213_Effect-Contol-Pannel--Top";
        step02_spriteMoving1.spriteName = "213_ffect-Control-Panel-moving-1";
        step02_spriteMoving2.spriteName = "213_Effect-Control-Panel-moving-2";
        step02_spriteMoving3.spriteName = "213_Effect-Control-Panel-moving-3";
        step03_spriteEffectCtrl_panel_moved.spriteName = "213_Effect-Control-Panel--After-moving";
        step03_spritePanelMenu.spriteName = "213_Panel-Menu";
        step03_spriteClosePanel.spriteName = "213_Close-Panel";
        step04_spriteSliderThumb1.spriteName = "213_Project-Panel-Expending-3";
        step04_spriteExpand_Left1.spriteName = "213_Project-Panel-Expending-1";
        step04_spriteExpand_Left2.spriteName = "213_Project-Panel-Expending-2";
        step04_spriteExpand_Left3.spriteName = "213_Project-Panel-Expending-3";
        step04_spriteExpand_Top1.spriteName = "213_Composition-Panel-empted-Space";
        step04_spriteExpand_Top2.spriteName = "213_Composition-Panel-1";
        step04_spriteExpand_Top3.spriteName = "213_Composition-Panel-2";
        step04_spriteExpand_Top4.spriteName = "213_Composition-Panel-3";
        step04_spriteSliderThumb2.spriteName = "213_Timeline-Panel-Expending-3";
        step04_spriteExpand_Bottom1.spriteName = "213_Timeline-Panel-Expending-1";
        step04_spriteExpand_Bottom2.spriteName = "213_Timeline-Panel-Expending-2";
        step04_spriteExpand_Bottom3.spriteName = "213_Timeline-Panel-Expending-3";
        step04_spriteExpand_Bottom4.spriteName = "213_Propject-Panel-Bottom";
        step04_spriteWindow.spriteName = "213_Window";
        step04_spriteMenu1.spriteName = "213_Menu-1";
        step04_spriteEffectCtrl.spriteName = "213_Workspace";
        step04_spriteMenu2.spriteName = "213_Menu-2";
        step04_spriteDefault.spriteName = "213_Reset-_Default_-to-Saved-Layout";

    }
    
    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);

        CameraManager.ins.MoveCamera(step01_spriteMenu1.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
            step01_box1.SetActive(true);
            step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
            step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                step01_box1.SetActive(false);

                step01_spriteWindow.gameObject.SetActive(true);
                step01_spriteWindow.GetComponent<TweenAlpha>().onFinished.Clear();
                step01_spriteWindow.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                    step01_spriteMenu1.gameObject.SetActive(true);
                    step01_spriteMenu1.GetComponent<TweenAlpha>().onFinished.Clear();
                    step01_spriteMenu1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                        step01_box2.SetActive(true);
                        step01_box2.GetComponent<UIEventTrigger>().onClick.Clear();
                        step01_box2.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                            step01_box2.SetActive(false);
                            step01_spriteMenu1_effectCtrl.gameObject.SetActive(true);
                            step01_spriteMenu1_effectCtrl.GetComponent<TweenAlpha>().onFinished.Clear();
                            step01_spriteMenu1_effectCtrl.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                step01.SetActive(false);
                                OnStep02();
                            }));
                        }));
                    }));
                }));
            }));
        }));

    }


    public void OnStep02()
    {
        step02.SetActive(true);

        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_213_1", "MID");
            delay0.gameObject.SetActive(true);
            delay0.onFinished.Clear();
            delay0.onFinished.Add(new EventDelegate(delegate {
                LM_AE_Manager.ins.Off_Middle_Guide();
                step02_glitters.SetActive(false);

                step02_box3.SetActive(true);
                step02_box4.SetActive(true);
                step02_box4_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

                step02_spriteEffectCtrl_panel_dragable.gameObject.SetActive(true);
                step02_spriteEffectCtrl_panel_dragable.transform.localPosition = step02_spriteEffectCtrl_panel.transform.localPosition;

                step02_spriteEffectCtrl_panel_dragable.GetComponent<UIEventTrigger>().onDragStart.Clear();
                step02_spriteEffectCtrl_panel_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                    CameraManager.SCREEN_MOVE_PAUSE = true;
                    step02_spriteEffectCtrl_panel_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                    step02_spriteEffectCtrl_panel_dragable.depth = 6;
                    step02_groupMovingPanel.SetActive(true);
                }));
                step02_spriteEffectCtrl_panel_dragable.GetComponent<UIEventTrigger>().onDrag.Clear();
                step02_spriteEffectCtrl_panel_dragable.GetComponent<UIEventTrigger>().onDrag.Add(new EventDelegate(delegate {
                    step02_spriteEffectCtrl_panel_dragable.depth = 6;
                }));
                step02_spriteEffectCtrl_panel_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
                step02_spriteEffectCtrl_panel_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                    CameraManager.SCREEN_MOVE_PAUSE = false;

                    step02_spriteEffectCtrl_panel_dragable.depth = 4;
                    step02_groupMovingPanel.SetActive(false);

                    if (step02_spriteEffectCtrl_panel_dragable.transform.localPosition.y <= step02_dragEnd.transform.localPosition.y + 300f && step02_spriteEffectCtrl_panel_dragable.transform.localPosition.y >= step02_dragEnd.transform.localPosition.y - 300f)
                    {
                        step02_box3.SetActive(false);
                        step02_box4.SetActive(false);
                        step02_spriteEffectCtrl_panel_dragable.gameObject.SetActive(false);

                        step02.SetActive(false);
                        OnStep03();
                    } else
                    {
                        step02_spriteEffectCtrl_panel_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                        step02_spriteEffectCtrl_panel_dragable.transform.localPosition = step02_spriteEffectCtrl_panel.transform.localPosition;
                    }
                }));
            }));
        }));

    }
    
    public void OnStep03()
    {
        step03.SetActive(true);

        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_213_2", "BOTTOM");

        delay1.gameObject.SetActive(true);
        delay1.onFinished.Clear();
        delay1.onFinished.Add(new EventDelegate(delegate {
            LM_AE_Manager.ins.Off_Middle_Guide();

            step03_box5.SetActive(true);
            step03_box5.GetComponent<UIEventTrigger>().onClick.Clear();
            step03_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                step03_box5.SetActive(false);
                step03_spritePanelMenu.gameObject.SetActive(true);
                step03_spritePanelMenu.GetComponent<TweenAlpha>().onFinished.Clear();
                step03_spritePanelMenu.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                    step03_box6.SetActive(true);
                    step03_box6.GetComponent<UIEventTrigger>().onClick.Clear();
                    step03_box6.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                        step03_box6.SetActive(false);
                        step03_spriteClosePanel.gameObject.SetActive(true);
                        step03_spriteClosePanel.GetComponent<TweenAlpha>().onFinished.Clear();
                        step03_spriteClosePanel.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                            step03.SetActive(false);

                            OnStep04();
                        }));
                    }));
                }));
            }));

        }));
    }

    
    public void OnStep04()
    {
        LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_213_3", "BOTTOM");

        step04.SetActive(true);

        delay2.gameObject.SetActive(true);
        delay2.onFinished.Clear();
        delay2.onFinished.Add(new EventDelegate(delegate {
            LM_AE_Manager.ins.Off_Middle_Guide();

            step04_box7.SetActive(true);
            step04_box8.SetActive(true);
            step04_box8_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

            step04_slider1.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate
            {
                CameraManager.SCREEN_MOVE_PAUSE = true;
                step04_slider1.transform.Find("Thumb").GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
            }));
            step04_slider1.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
            {
                CameraManager.SCREEN_MOVE_PAUSE = false;
            }));

            step04_slider1.GetComponent<UISlider>().onChange.Clear();
            step04_slider1.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate {
                if(step04_slider1.GetComponent<UISlider>().value == 1f)
                {
                    step04_spriteSliderThumb1.GetComponent<BoxCollider>().enabled = false;
                    step04_slider1.GetComponent<UISlider>().value = 1f;
                    step04_box7.SetActive(false);
                    step04_box8.SetActive(false);

                    step04_box9.SetActive(true);
                    step04_box10.SetActive(true);
                    step04_box10_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

                    step04_slider2.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate
                    {
                        CameraManager.SCREEN_MOVE_PAUSE = true;
                        step04_slider2.transform.Find("Thumb").GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                    }));
                    step04_slider2.transform.Find("Thumb").GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate
                    {
                        CameraManager.SCREEN_MOVE_PAUSE = false;
                    }));

                    step04_slider2.GetComponent<UISlider>().onChange.Clear();
                    step04_slider2.GetComponent<UISlider>().onChange.Add(new EventDelegate(delegate {
                        if (step04_slider2.GetComponent<UISlider>().value == 1f)
                        {
                            step04_spriteSliderThumb2.GetComponent<BoxCollider>().enabled = false;
                            step04_slider2.GetComponent<UISlider>().value = 1f;
                            step04_box9.SetActive(false);
                            step04_box10.SetActive(false);

                            CameraManager.ins.MoveCamera(step04_spriteMenu1.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                LM_AE_Manager.ins.On_Middle_Guide("LEARNING_AE_213_4", "MID");
                                step04_box11.SetActive(true);
                                step04_box11.GetComponent<UIEventTrigger>().onClick.Clear();
                                step04_box11.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                    LM_AE_Manager.ins.Off_Middle_Guide();
                                    step04_box11.SetActive(false);

                                    step04_spriteWindow.gameObject.SetActive(true);
                                    step04_spriteWindow.GetComponent<TweenAlpha>().onFinished.Clear();
                                    step04_spriteWindow.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                        step04_spriteMenu1.gameObject.SetActive(true);
                                        step04_spriteMenu1.GetComponent<TweenAlpha>().onFinished.Clear();
                                        step04_spriteMenu1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                            step04_box12.SetActive(true);
                                            step04_box12.GetComponent<UIEventTrigger>().onClick.Clear();
                                            step04_box12.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                step04_box12.SetActive(false);
                                                step04_spriteEffectCtrl.gameObject.SetActive(true);
                                                step04_spriteEffectCtrl.GetComponent<TweenAlpha>().onFinished.Clear();
                                                step04_spriteEffectCtrl.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                    step04_spriteMenu2.gameObject.SetActive(true);
                                                    step04_spriteMenu2.GetComponent<TweenAlpha>().onFinished.Clear();
                                                    step04_spriteMenu2.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                        CameraManager.ins.MoveCamera(step04_spriteMenu2.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
                                                            step04_box13.SetActive(true);
                                                            step04_box13.GetComponent<UIEventTrigger>().onClick.Clear();
                                                            step04_box13.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                                step04_box13.SetActive(false);
                                                                step04_spriteDefault.gameObject.SetActive(true);
                                                                step04_spriteDefault.GetComponent<TweenAlpha>().onFinished.Clear();
                                                                step04_spriteDefault.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                                    step04.SetActive(false);
                                                                    delay3.gameObject.SetActive(true);
                                                                    delay3.onFinished.Clear();
                                                                    delay3.onFinished.Add(new EventDelegate(delegate {
                                                                        CameraManager.ins.MoveCamera(true, true, 0.5f).onFinished.Add(new EventDelegate(delegate {
                                                                            delay4.gameObject.SetActive(true);
                                                                            delay4.onFinished.Clear();
                                                                            delay4.onFinished.Add(new EventDelegate(delegate {
                                                                                StartCoroutine(EndDelay());
                                                                            }));
                                                                        }));
                                                                    }));
                                                                }));
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }
                    }));
                }
            }));

        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_AE_213_5");
    }
}
