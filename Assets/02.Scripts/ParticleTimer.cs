﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public class ParticleTimer : MonoBehaviour {

    private ParticleSystem m_particle;

   void Awake()
    {
        m_particle = this.transform.GetComponent<ParticleSystem>();
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (!m_particle.isPlaying)
            CallerManager.Caller.GetInst.Restore(this.gameObject);
              


    }



    public void SetParticle(ParticleSystem pcs)
    {
        m_particle = pcs;
    }
    void OnEnable()
    {
        m_particle = this.transform.GetComponent<ParticleSystem>();
        m_particle.Play();
    }

}
