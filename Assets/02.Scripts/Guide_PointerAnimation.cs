﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide_PointerAnimation : MonoBehaviour
{
    private Animation Anim;
    public GameObject ClickAnimation;

    public bool b_AnimStart = false;
    private bool b_Click = false;

    public bool isDragAnim;
    // Start is called before the first frame update
    void Start()
    {
        Anim = this.GetComponent<Animation>();

        if(this.gameObject.GetComponent<TweenScale>() != null)
            this.gameObject.GetComponent<TweenScale>().enabled = true;
    }

    // Update is called once per frame

    float duration;
    void Update()
    {
        if (b_AnimStart && isDragAnim == false)
        {

            if (!Anim.isPlaying)
                Anim.Play();

            int frame = (int)(Anim["GuidePointer_ClickAnim"].time * 60f);
            if (frame <= 2 && b_Click)
                b_Click = false;
            else if (frame >= 22 && !b_Click)
            {
                if(ClickAnimation != null)
                    ClickAnimation.gameObject.SetActive(true);
                b_Click = true;
            }
        }
        
    }


    
}
