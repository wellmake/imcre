﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LM_DR_114 : MonoBehaviour
{
    public enum Step
    {
        step01,
        step02
    }
    public Transform container;
    public Transform RootUI;

    [Header("Step01")]
    public GameObject step01;
    public GameObject step01_cameraPoint1;
    public UISprite step01_spriteIndicator;
    public UISprite step01_spriteSoundSource;
    public UISprite step01_spriteSoundSource_dragable;
    public GameObject step01_box1;
    public GameObject step01_box2;
    public GameObject step01_box2_dragPointer;
    public GameObject step01_dragEnd;
    public UISprite step01_spriteClip1;
    public GameObject step01_glitters1;
    public GameObject step01_keyboard_B;
    public UISprite step01_spriteKnife;
    public GameObject step01_box3;
    public UISprite step01_spriteClip1_5;
    public UISprite step01_spriteClip2;
    public GameObject step01_glitters2;
    public GameObject step01_box4;
    public UISprite step01_spriteClip3;
    public GameObject step01_keyboard_delete;
    public UISprite step01_spriteClip3_5;
    public GameObject step01_box5;
    public UISprite step01_spriteM;
    public UISprite step01_spriteClip4;
    public GameObject step01_cameraPoint2;
    public GameObject step01_keyboard_space;
    public GameObject step01_video;

    public TweenAlpha delay0;
    public TweenAlpha delay1;
    public TweenAlpha delay2;
    public TweenAlpha delay3;
    public TweenAlpha delay4;

    List<UISprite> lmSpriteList;


    public GameObject Guide_Touch;
    public GameObject start_guide;
    public GameObject Skip;

    public GameObject MiddleGuide;
    public GameObject EndGuide;
    private void Start()
    {
        this.Guide_Touch = LM_AE_Manager.ins.Guide_Touch_;
        this.start_guide = LM_AE_Manager.ins.start_guide_;
        this.Skip = LM_AE_Manager.ins.Skip_;
        this.MiddleGuide = LM_AE_Manager.ins.MiddleGuide_;

        CameraManager.ins.InitCamera();

        lmSpriteList = new List<UISprite>();

        // Set the Atlas along the current language
        if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.en)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213") as NGUIAtlas;
            // guideBlue_Window.SetActive(true);
        }
        else if (LM_AE_Manager.ins.curLanguage == LM_AE_Manager.Language.kr)
        {
            //Controller.ins.lm_Atlas = Resources.Load("213_kr") as NGUIAtlas;
            //  guideBlue_Window_kr.SetActive(true);
        }
        SetSprites();

        LM_AE_Manager.ins.Set_Start_Guide("LEARNING_DR_114_0", OnStep01);
        LM_AE_Manager.ins.Set_Title("LM_" + this.gameObject.name);


        Skip.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
            OnStep01(null);
        }));
    }

    bool dragFlag = false;
    int dragCount = 0;
    private void Update()
    {
        if (dragFlag)
        {

        }
    }

    void SetSprites()
    {
        lmSpriteList.Clear();
        lmSpriteList.Add(step01_spriteIndicator);
        lmSpriteList.Add(step01_spriteSoundSource);
        lmSpriteList.Add(step01_spriteSoundSource_dragable);
        lmSpriteList.Add(step01_spriteClip1);
        lmSpriteList.Add(step01_spriteKnife);
        lmSpriteList.Add(step01_spriteClip1_5);
        lmSpriteList.Add(step01_spriteClip2);
        lmSpriteList.Add(step01_spriteClip3);
        lmSpriteList.Add(step01_spriteClip3_5);
        lmSpriteList.Add(step01_spriteM);
        lmSpriteList.Add(step01_spriteClip4);

        foreach (var item in lmSpriteList)
        {
            item.atlas = LM_AE_Manager.ins.lm_Atlas;
        }

        step01_spriteIndicator.spriteName = "114_0014_Indicator";
        step01_spriteSoundSource.spriteName = "114_0013_Sound-Source-";
        step01_spriteSoundSource_dragable.spriteName = "114_0012_Drag-Sound-Source";
        step01_spriteClip1.spriteName = "114_0011_Sound-Cllip-1";
        step01_spriteKnife.spriteName = "114_0009_Knife-Tool";
        step01_spriteClip1_5.spriteName = "114_0006_Cutting-Line";
        step01_spriteClip2.spriteName = "114_0007_Clip-2";
        step01_spriteClip3.spriteName = "114_0004_Choosing-Clip--";
        step01_spriteClip3_5.spriteName = "114_0008_Clip-1";
        step01_spriteM.spriteName = "114_0002_M";
        step01_spriteClip4.spriteName = "114_0001_unactived-Clip";

    }
    /*
        step01_box1.SetActive(true);
        step01_box1.GetComponent<UIEventTrigger>().onClick.Clear();
        step01_box1.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {

        }));

    */
    public void OnStep01(GameObject gameobject)
    {
        start_guide.SetActive(false);
        step01.SetActive(true);

        CameraManager.ins.MoveCamera(step01_cameraPoint1.transform.position, 0.5f, true).onFinished.Add(new EventDelegate(delegate {
            step01_spriteSoundSource.gameObject.SetActive(true);
            step01_spriteSoundSource_dragable.gameObject.SetActive(true);
            step01_spriteSoundSource_dragable.transform.localPosition = step01_spriteSoundSource.transform.localPosition;

            step01_box1.SetActive(true);
            step01_box2.SetActive(true);
            step01_box2_dragPointer.GetComponent<PrefabDragAnim>().StartDrag();

            step01_spriteSoundSource_dragable.GetComponent<UIEventTrigger>().onDragStart.Add(new EventDelegate(delegate {
                step01_spriteSoundSource_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnEnable;
                CameraManager.SCREEN_MOVE_PAUSE = true;
            }));
            step01_spriteSoundSource_dragable.GetComponent<UIEventTrigger>().onDragEnd.Clear();
            step01_spriteSoundSource_dragable.GetComponent<UIEventTrigger>().onDragEnd.Add(new EventDelegate(delegate {
                CameraManager.SCREEN_MOVE_PAUSE = false;
                if (step01_spriteSoundSource_dragable.transform.position.y <= step01_dragEnd.transform.position.y + 0.1f && step01_spriteSoundSource_dragable.transform.position.y >= step01_dragEnd.transform.position.y - 0.1f)
                {
                    step01_spriteSoundSource_dragable.gameObject.SetActive(false);
                    step01_box1.SetActive(false);
                    step01_box2.SetActive(false);

                    step01_spriteClip1.gameObject.SetActive(true);
                    step01_glitters1.SetActive(true);

                    CameraManager.ins.MoveCamera(step01_cameraPoint2.transform.position, 0.5f, false).onFinished.Add(new EventDelegate(delegate {
                        delay0.gameObject.SetActive(true);
                        delay0.GetComponent<TweenAlpha>().onFinished.Clear();
                        delay0.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                            step01_glitters1.SetActive(false);

                            LM_AE_Manager.ins.On_Middle_Guide("LEARNING_DR_114_1", "TOP");

                            step01_keyboard_B.SetActive(true);
                            step01_keyboard_B.GetComponent<UIEventTrigger>().onClick.Clear();
                            step01_keyboard_B.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                step01_keyboard_B.GetComponent<TweenScale>().enabled = false;
                                step01_keyboard_B.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);

                                LM_AE_Manager.ins.Off_Middle_Guide();
                                step01_keyboard_B.SetActive(false);

                                step01_spriteKnife.gameObject.SetActive(true);
                                step01_box3.SetActive(true);
                                step01_box3.GetComponent<UIEventTrigger>().onClick.Clear();
                                step01_box3.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                    step01_box3.SetActive(false);

                                    step01_spriteClip1_5.gameObject.SetActive(true);
                                    step01_spriteClip2.gameObject.SetActive(true);
                                    step01_glitters2.SetActive(true);

                                    delay1.gameObject.SetActive(true);
                                    delay1.GetComponent<TweenAlpha>().onFinished.Clear();
                                    delay1.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                        step01_box4.SetActive(true);
                                        step01_box4.GetComponent<UIEventTrigger>().onClick.Clear();
                                        step01_box4.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                            step01_box4.SetActive(false);
                                            step01_glitters2.SetActive(false);

                                            step01_spriteClip3.gameObject.SetActive(true);
                                            step01_keyboard_delete.SetActive(true);
                                            step01_keyboard_delete.GetComponent<UIEventTrigger>().onClick.Clear();
                                            step01_keyboard_delete.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                step01_keyboard_delete.GetComponent<TweenScale>().enabled = false;
                                                step01_keyboard_delete.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);

                                                step01_keyboard_delete.SetActive(false);
                                                step01_spriteClip1.gameObject.SetActive(false);
                                                step01_spriteClip1_5.gameObject.SetActive(false);
                                                step01_spriteClip2.gameObject.SetActive(false);
                                                step01_spriteClip3.gameObject.SetActive(false);

                                                step01_spriteClip3_5.gameObject.SetActive(true);

                                                step01_box5.SetActive(true);
                                                step01_box5.GetComponent<UIEventTrigger>().onClick.Clear();
                                                step01_box5.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                    step01_box5.SetActive(false);

                                                    step01_spriteM.gameObject.SetActive(true);
                                                    step01_spriteClip4.gameObject.SetActive(true);

                                                    step01_keyboard_space.SetActive(true);
                                                    step01_keyboard_space.GetComponent<UIEventTrigger>().onClick.Clear();
                                                    step01_keyboard_space.GetComponent<UIEventTrigger>().onClick.Add(new EventDelegate(delegate {
                                                        step01_keyboard_space.GetComponent<TweenScale>().enabled = false;
                                                        step01_keyboard_space.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);

                                                        step01_keyboard_space.SetActive(false);

                                                        step01_video.SetActive(true);

                                                        step01_spriteIndicator.GetComponent<TweenPosition>().from = step01_spriteIndicator.transform.localPosition;
                                                        step01_spriteIndicator.GetComponent<TweenPosition>().to = new Vector3(step01_cameraPoint2.transform.localPosition.x, step01_spriteIndicator.transform.localPosition.y, 0f);
                                                        step01_spriteIndicator.GetComponent<TweenPosition>().duration = 5f;
                                                        step01_spriteIndicator.GetComponent<TweenPosition>().delay = 0.5f;
                                                        step01_spriteIndicator.GetComponent<TweenPosition>().PlayForward();
                                                        step01_spriteIndicator.GetComponent<TweenPosition>().onFinished.Clear();
                                                        step01_spriteIndicator.GetComponent<TweenPosition>().onFinished.Add(new EventDelegate(delegate {
                                                            delay2.gameObject.SetActive(true);
                                                            delay2.GetComponent<TweenAlpha>().onFinished.Clear();
                                                            delay2.GetComponent<TweenAlpha>().onFinished.Add(new EventDelegate(delegate {
                                                                //step01_video.SetActive(false);
                                                                StartCoroutine(EndDelay());
                                                            }));
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        }));
                    }));

                } else
                {
                    step01_spriteSoundSource_dragable.GetComponent<UISprite>().updateAnchors = UIRect.AnchorUpdate.OnUpdate;
                    step01_spriteSoundSource_dragable.transform.localPosition = step01_spriteSoundSource.transform.localPosition;
                }
            }));
        }));
    }

    IEnumerator EndDelay()
    {
        yield return new WaitForSeconds(1);
        LM_AE_Manager.ins.On_End_Guide("LEARNING_DR_114_2");
    }
}
